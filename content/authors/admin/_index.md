---
title: "Laurent Jacques"
author: ["Laurent Jacques"]
draft: false
# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: FNRS Senior Research Associate and Professor

# Organizations/Affiliations to show in About widget
organizations:
- name: UCLouvain, Belgium 
  url: "https://laurentjacques.gitlab.io"

# Short bio (displayed in user profile at end of posts)
#bio: Laurent Jacques is Professor and FNRS Senior Research Associate in the Image and Signal Processing Group (ISPGroup) in the ICTEAM institute at UCLouvain. His research focuses on Sparse Representations of signals (1-D, 2-D, sphere), Compressive Sensing, 1-bit and Quantized Compressive Sensing, Computational Imaging, Inverse Problems in Optics and in Computer Vision. 

# Interests to show in About widget
interests:
- Inverse Problems in Optics, Astronomy, and Biomedical Sciences
- Compressive Sensing and Computational Imaging
- Compressive Learning and Data Sciences
- Image and Signal Processing

# Education to show in About widget
education:
  courses:
  - course: PhD in Mathematical Physics
    institution: UCLouvain, Belgium
    year: 2004
  - course: MSc in Mathematical Physics
    institution: UCLouvain, Belgium
    year: 1998
  - course: BSc in Physics
    institution: UCLouvain, Belgium
    year: 1996
# Education to show in About widget
appointments:
- IEEE  Transactions on Signal Processing - Associate Editor (2022-now)
- EURASIP Journal on Advances in Signal Processing - Associate Editor (2020-2022)
- EURASIP Signal and Data Analytics for Machine Learning - Technical Area Committee member
# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:laurent.jacques@uclouvain.be'
- icon: mastodon
  icon_pack: fab
  link: https://mathstodon.xyz/@lowrankjack
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/jacquesdurden
- icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
  icon_pack: fas
  link: https://scholar.google.com/citations?hl=en&user=zo8lijwAAAAJ&view_op=list_works&pagesize=100
- icon: github
  icon_pack: fab
  link: https://github.com/jacquesdurden/
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/laurent-jacques-6641579/
- icon: orcid
  icon_pack: fab
  link: https://orcid.org/0000-0002-6261-0328
# TODO: add https://nofake.science/tribune

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
# email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

I am a Professor and FNRS Senior Research Associate at the Image and Signal Processing Group ([ISPGroup](https://sites.uclouvain.be/ispgroup/)), in the Mathematical engineering ([INMA](https://uclouvain.be/fr/node/2107)) department of the [ICTEAM institute](https://uclouvain.be/fr/node/1991) at [UCLouvain](https://uclouvain.be/en/index.html), Belgium. My main research activities focus on compressive sensing (theory and applications), inverse problems solving in optics, astronomy and biomedical sciences, compressive learning and data sciences, sparse signal representations and other low-complexity signal priors.
