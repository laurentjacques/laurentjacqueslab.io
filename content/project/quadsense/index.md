---
title: "QuadSense"
author: ["Laurent Jacques"]
date: 2023-12-22
tags: ["rank-one projection", "project", "FNRS", "PDR", "ptychography", "interferometry", "quadsense", "project", "fnrs"]
draft: false
title: "QuadSense"
subtitle: "Computational Sensing for Interferometric and Ptychographic Imaging"
summary: "Computational Sensing for Interferometric and Ptychographic Imaging"
authors: []
tags: []
categories: []
date: 2024-05-15
include_toc: true

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

## NEW (updated on 20/11/24): Postdoc opening {#new--updated-on-20-11-24--postdoc-opening}

The research group of Prof. Laurent Jacques in the mathematical engineering department ([INMA](https://uclouvain.be/fr/node/2107)) and the [Image and Signal Processing Group](https://ispgroup.gitlab.io/) (ISPGroup) at [UCLouvain](https://uclouvain.be/en), Belgium, opens one position for a postdoctoral researcher to work on the **QuadSense** project (see project description below), a new project funded by the [Belgian Fund for Scientific Research - FNRS](https://www.frs-fnrs.be/en).

> &rarr; **Application deadline**: None (but the position could be closed earlier if a good candidate is found)<br />
> &rarr; **Starting date**: as soon as the selected candidate is available.


### Project keywords {#project-keywords}

Inverse problems, quadratic projections, rank-one projections, interferometric models, phase retrieval, compressive sensing, implicit neural representation, neural fields, radio astronomy, ptychography.


### Applicant's profile {#applicant-s-profile}

-   PhD in applied mathematics, electrical engineering, physics, optics, or data science.
-   Knowledge in at least one of the following topics is a plus: compressive sensing, inverse problem solving, convex optimization, machine learning, deep learning techniques, implicit neural representation and PINN, random projections, radio-astronomy, interferometry, ...
-   Interest in both theoretical and numerical developments.
-   Good programming skills in Python (knowing specific toolboxes such as [PyTorch](https://pytorch.org/), [Jax](https://jax.readthedocs.io/en/latest/index.html) or [DeepInv](https://deepinv.github.io/deepinv/) is a plus, as well as GPU/parallel programming) ;
-   Good communications skills, both written and oral, in English.
-   Knowing French **is not** required (the research group is international)
-   **Mobility criterion**: The position is open to all nationalities, including Belgian, but the condition is to have spent less than 12 months over the last three years in Belgium.


### We offer {#we-offer}

-   A research position in a dynamic environment, working on leading-edge theories and applications with international contacts (see below);
-   The salary is attractive (net salary &gt; 2200 k€/month, exact value depends on candidate's status)
-   Health insurance system is cheap in Belgium (similar to other countries in Europe);
-   A 12-month position funded by the Belgian NSF (FNRS), with possible 12-month extension according to the progression.
-   The funding is a scholarship; Visa will be needed for a non-EU researcher.


### Application procedure {#application-procedure}

Applications must be sent by email as soon as possible to [Prof. Laurent Jacques](mailto:laurent.jacques@uclouvain.be) and must include these 3 elements:

-   a detailed resume (in pdf)
-   a motivation letter (in pdf)
-   the names and complete addresses of two reference persons.

The position can be filled as soon as the selected candidate is available.


## The QuadSense Project {#the-quadsense-project}

> _Important remark_: The postdoctoral researcher hired will work on the QuadSense project described below. This project provides only a general scientific framework for the research to be pursued by this researcher. Therefore, the actual research to be carried out for this postdoctoral position will be determined both by discussions with the project members and by the scientific interest of the hired researcher.

Many fields of science and technology have to deal with an ever-growing accumulation of data, in various forms such as signals, images, videos or volumes of biomedical data. Consequently, numerous methods and algorithms, including principal component analysis, clustering and random projection techniques, have been developed to condense these objects into compact representations while preserving essential information during the compression process. Alternatively, in certain scenarios, the physics of specific applications delivers indirect observations of the target object (such as biological cells in a confocal microscope, human brains in an MRI or black holes in the Event Horizon Telescope) from which high quality object images must be recovered.

{{< figure src="/ox-hugo/2023-12-22-T16-03-33.png" caption="<span class=\"figure-number\">Figure 1: </span>Working principle of a lensless endoscope (legend: SLM: spatial light modulator; BS: beam-splitter; MCF: multicore optical fiber; PMT: photomultiplier). (a) Conventional raster-scan lensless endoscopy (RS-LE). In RS-LE, the light beam outgoing from the multicore optical fiber (MCF) at the distal end is focused thanks to a wavefront shaper (SLM) at the proximal end. Translating the focused illumination line-by-line and collecting the reflected/re-emitted light for each location in an external single-pixel detector enables the imaging of the (fluorescent) biological sample at a specified depth [8]. (b) 3-D interferometric lensless imaging (3ILI). In 3ILI, we aim to reconstruct a 3-D biological sample by leveraging the axial resolution of random speckles in the near field." width="60%" >}}

Quadratic projection (and sensing) methods define a subclass of these data transformations. We encounter them in the problem of phase retrieval (PR), algorithmic methods for dimensionality reduction (e.g., for covariance matrix estimation from a large data stream using rank-one projection (ROP) methods [1,2]), sketched principal component analysis [3], or data sketching using an optical processing unit [4]. They also define a key model to explain PR where only the spectrum intensity of a given object is observed, e.g., in X-ray crystallography or electron ptychography [5-7].

{{< figure src="/ox-hugo/2023-12-22-T15-58-20.png" caption="<span class=\"figure-number\">Figure 2: </span>(a) Working principle of electron ptychography (EP). Following a 4-D STEM scheme, EP aims to image the atomic structure of organic and inorganic samples. It relies on recording two-dimensional (2-D) diffraction patterns, in intensity, for every position of a convergent electron beam. (b) By solving the related phase retrieval problem (e.g., using greedy or non-convex optimization methods), the four-dimensional observational dataset allows the reconstruction of a high-resolution object. (c) In QuadSense, we will study the possibility of direct inference in the observational space. We will leverage the similarity between the EP sensing model and the quadratic sensing model of a ROP to show how one can infer the correlation between the target image \\(f\\) and a probing pattern \\(u\\) by comparing the EP sensing of both objects. (d) Finally, we will study additional compressive schemes for EP by subsampling both the probe positions and the diffraction patterns, thus combining EP with the matrix completion problem in the spectrum-space domain." width="60%" >}}

{{< figure src="/ox-hugo/2023-12-22-T16-29-41.png" caption="<span class=\"figure-number\">Figure 3: </span>(a) In radio-interferometric imaging (RII), an array of radio telescopes observes an object of interest in a part of the sky. The van Cittert-Zernike theorem (VCZT) shows that the time correlation of two antenna signals amounts to the evaluation of the spatial Fourier transform of the image on a frequency vector (or visibility) resulting from the projection of the baseline vector of the antenna pair onto the object plane. (b) In QuadSense, inspired by the similarities with the sensing model of interferometric lensless imaging, we will study new compression schemes for the antenna signals. In particular, by the VCZT, by linearly combining the signals of a group of antennas adjusted by separate gains, the time autocorrelation of the resulting signal is the ROP of the interferometric matrix of the image." width="60%" >}}

****QuadSense aims at expanding the applicability of quadratic sensing models, in the context of interferometric (see Fig. 1 and Fig. 3) and ptychographic imaging (see Fig. 2), to address the specific challenges faced by these applications, such as compressing their large observational data, or enabling 3-D imaging.**** The common feature of these applications is their connection with a quadratic projection, which can be recast as a set of **rank-one projections** (see figure captions). We therefore expect a high level of interac- tion between the different development stages of QuadSense, such as the formulation of suitable inference techniques and image reconstruction methods.

QuadSense will also be supported by a strong network of collaborators, including

-   [A. Moshtaghpour](https://www.rfi.ac.uk/people/dr-amirafshar-moshtaghpour/) (Rosalind Franklin Institute, UK)
-   [J. Tachella](https://tachella.github.io/) (ENS Lyon, France)
-   [S. Sivankutty](https://pro.univ-lille.fr/siddharth-sivankutty) (U. Lille, France)
-   [H. Rigneault](https://www.fresnel.fr/spip/spip.php?article1139) (Institut Fresnel, France)
-   [Y. Wiaux](https://researchportal.hw.ac.uk/en/persons/yves-wiaux) (Heriot-Watt University, UK)

QuadSense research is supported by [the Fund for Scientific Research (F.R.S.–FNRS)](https://www.frs-fnrs.be/en/).

{{< figure src="/ox-hugo/2023-12-22-T16-48-39.png" width="10%" >}}


## References {#references}

-   [1] Y. Chen, Y. Chi, A. J. Goldsmith, Exact and Stable Covariance Estimation From Quadratic Sampling via Convex Programming, IEEE Transactions on Information Theory, vol. 61, no. 7, 2015.
-   [2] T. Cai, and A. Zhang., ROP: Matrix Recovery via Rank One Projections, The Annals of Statistics, vol. 43, no. 1, pp. 102-38, 2015.
-   [3] Gribonval, R., Chatalic, A., Keriven, N., Schellekens, V., Jacques, L., &amp; Schniter, P. (2021). Sketching data sets for large-scale learning: Keeping only what you need. IEEE Signal Processing Magazine, 38(5), 12-36.
-   [4] A. Saade, F. Caltagirone, I. Carron, L. Daudet, A. Drémeau, S. Gigan, and F. Krzakala, Random projections through multiple optical scattering: Approximating kernels at the speed of light. In IEEE ICASSP 2016 (pp. 6215-6219).
-   [5] Fienup, J. R. (1982). Phase retrieval algorithms: a comparison. Applied optics, 21(15), 2758-2769.
-   [6] E.J. Candès, T. Strohmer, and V. Voroninski, PhaseLift: Exact and Stable Signal Recovery from Magnitude Measurements via Convex Programming.,Comm. Pure Appl. Math., 66: 1241-1274, 2013.
-   [7] Zhou, L., et al (2020). Low-dose phase retrieval of biological specimens using cryo-electron ptychography. Nature communications, 11(1), 2773.
-   [8] See related publications here: <https://laurentjacques.gitlab.io/tag/lensless-imaging/>
