---
title: 'First news'
date: Sun, 18 May 2008 21:58:52 +0000
draft: false
tags: ['General']
---


#### 
[Laurent (yet another one)](http://www.laurent-duval.eu "laurent.duval@gmail.com") - <time datetime="2008-05-19 16:51:37">May 1, 2008</time>

Long live your blog!
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-05-19 20:56:04">May 1, 2008</time>

Thank you Laurent D. Your are very quick to find scientific colleagues on the blogosphere ;-) I added "La vertu d'un La" in the right links.
<hr />
#### 
[Igor Carron](http://nuit-blanche.blogspot.com "igorcarron@gmail.com") - <time datetime="2008-05-22 20:01:31">May 4, 2008</time>

I did not realize you were into computational photography. When is the first technical entry ? :-) Good luck. Igor.
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-05-23 07:50:50">May 5, 2008</time>

Hello Igor, Nice to see that, as Laurent did, you have detected by yourself that I started this blog ;-) As you saw, it is just a beginning. I do not know currently at which frequency I will post. Probably at very low rate. Computational photography is up to now just a "focus of interest". I'm working with Pierre Vandergheynst at EPFL on new camera designs with researchers in micro-electronics using multiple sensors and/or CS but this is just the beginning. For the first technical post, I'm working on it. It will be possibly a review of a recent CS paper I liked and used. Best, Laurent
<hr />
