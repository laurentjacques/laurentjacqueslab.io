---
title: 'Testing a Quasi-Isometric Quantized Embedding'
date: Tue, 19 Nov 2013 10:31:25 +0000
draft: false
tags: ['Compressed Sensing', 'Johnson Lindenstrauss', 'Numerical Methods']
---

It took me a certain time to do it. Here is at least a first attempt to test numerically the validity of some of the results I obtained in "_A Quantized Johnson Lindenstrauss Lemma: The Finding of Buffon's Needle_" [(arXiv)](http://arxiv.org/abs/1309.1507) I have decided to avoid using the too conventional matlab environment. Rather, I took this exercise as an opportunity to learn the "ipython notebooks" and the wonderful tools provided by the [SciPy](http://scipy.org) python ecosystem. In short, for those of you who don't know it, the ipython notebooks allow you to generate actual scientific HTML reports with (latex rendered) explanations and graphics. The result cannot be properly presented on this blog (hosted on WordPress), so, I decided to share the report through [IPython Notebook Viewer website](http://nbviewer.ipython.org/). Here it is:

["Testing a Quasi-Isometric Embedding"](http://nbviewer.ipython.org/url/perso.uclouvain.be/laurent.jacques/pythonnb/TestingQuasiIsometricEmbedding.ipynb)

_(update 21/11/2013)_ ... and a variant of it estimating a "curve of failure" (rather than playing with standard deviation analysis):

["Testing a Quasi-Isometric Embedding with Percentile Analysis"](http://nbviewer.ipython.org/url/perso.uclouvain.be/laurent.jacques/pythonnb/TestingQuasiIsometricEmbeddingPercentile.ipynb)

Moreover, on these two links, you have also the possibility to download the corresponding script for running it on your own IPython notebook system. If you have any comments or corrections, don't hesitate to add them below in the "comment" section. Enjoy!