---
title: '1000th visit and some Compressed Sensing "humour"'
date: Sat, 22 Nov 2008 22:47:30 +0000
draft: false
tags: ['General']
---


#### 
[1000th visit and some Compressed Sensing “humour”](http://comp-eng.binus.ac.id/2012/04/20/1000th-visit-and-some-compressed-sensing-humour/ "") - <time datetime="2012-04-20 12:11:59">Apr 5, 2012</time>

\[...\] http://yetaspblog.wordpress.com/2008/11/23/1000th-visit-and-some-compressed-sensing-humour/ \[...\]
<hr />
#### 
[pedrock](http:// "pedrock.opiniatre@gmail.com") - <time datetime="2008-11-25 17:44:22">Nov 2, 2008</time>

Merci pour le Blogroll ;) Pedrock
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-11-25 18:09:51">Nov 2, 2008</time>

De rien Pedro. Marrant que tu ai aussi migré sur Wordpress ! @+, Laurent
<hr />
#### 
[Kezhi LI]( "k.li08@imperial.ac.uk") - <time datetime="2008-11-27 13:50:04">Nov 4, 2008</time>

Good idea~ Maybe we can operate the reconstruction on the shrinked lion data in a cage, so we "catch" the lion :-)
<hr />
#### 
[Jort Gemmeke](http://www.amadana.nl "jgemmeke@amadana.nl") - <time datetime="2008-11-27 14:33:47">Nov 4, 2008</time>

A lion may be sparsely represented in a basis of other lions. In that case, we can reconstruct the Lion using only very few measurements of the Lion. So no need for the whole lion, just some hairs or so. And since finding the sparsest representation of hairs of the Lion in the basis of lions is quite slow, you have plenty of time to build the cage around it while your waiting for the solver to finish...
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-11-27 15:02:03">Nov 4, 2008</time>

Excellent. Thanks. Be careful, if you remove too many hairs, e.g. more than M/log(N/K), the lion becomes itself too sparse ;-)
<hr />
#### 
[Igor Carron](http://nuit-blanche.blogspot.com "igorcarron@gmail.com") - <time datetime="2008-11-27 16:49:30">Nov 4, 2008</time>

along the lines Of Jort's solution: acquire CS measurements of the Lion, once it is caught it takes a long time to reconstruct. If you are fast enough, acquire an image of a cage. Since CS is linear, add the CS measurements of the Lion and that of the cage before the Lion is reconstructed and then reconstruct the new measurements of Lion + Cage. Howw does that sound ? Igor.
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-11-27 17:22:17">Nov 4, 2008</time>

Perfect. Very handy indeed. Laurent
<hr />
#### 
[Laurent Duval](http://www.laurent-duval.eu "laurent.duval@gmail.com") - <time datetime="2008-11-28 23:29:15">Nov 5, 2008</time>

I would project the whole stuff onto a tight (lionlet) frame, then use a (local trigonometric) folding operator to bend the frame into a cage. If the lion is indeed sparse, the frame needs to be very very tight. Not CS enough? To deterministic? Bad bad naughty me... So, let's build a lion out of sand: http://flickr.com/photos/rosemovie/2710246346/ Laurent
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-11-29 01:56:39">Nov 6, 2008</time>

Cool way indeed. The only problem I could see is that there is still a lot of research in the field of the best Lionlet frame, as in any other \*let field, some trying for instance to make that more directional. But after all, a frame promoting only the vertical direction of the cage bars is sufficient of course ! Thank you Laurent. Laurent
<hr />
#### 
[Edward](http://www.ams.org "egdunne@gmail.com") - <time datetime="2008-12-10 06:06:17">Dec 3, 2008</time>

The game of lion-hunting using mathematical methods dates back at least to the 1930s. Ralph Boas, who was famous for his mathematics, learned the game when he was a grad student at Princeton in the late 1930s. He became a great player of the game, and published some fine articles about mathematical lion-hunting. Others followed Boas's lead, even to the extent of copying Boas's use of pseudonyms rather than his own name. A collection of Boas's non-technical writings is titled "Lion Hunting and other Mathematical Pursuits". It was published by the Mathematical Association of America (http://www.maa.org) in 1995.
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2008-12-10 10:56:37">Dec 3, 2008</time>

Thank you very much for these historical explanations Edward. I like to read the origins of such things (as for Matching Pursuit in one of my previous posts) Laurent
<hr />
#### 
[Academic Career Links](http://aclinks.wordpress.com "an.occasional.mathematician@gmail.com") - <time datetime="2009-04-04 00:12:26">Apr 6, 2009</time>

A nice post, thanks!
<hr />
