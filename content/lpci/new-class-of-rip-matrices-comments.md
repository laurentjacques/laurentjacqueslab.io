---
title: 'New class of RIP matrices ?'
date: Tue, 18 May 2010 08:03:34 +0000
draft: false
tags: ['General']
---


#### 
[Igor Carron](http://nuit-blanche.blogspot.com "igorcarron@gmail.com") - <time datetime="2010-05-26 14:36:33">May 3, 2010</time>

Laurent, Do you think that g could be behaving like the quantization function in the 1-bit compressive sensing approach of Petros ? http://www.merl.com/reports/docs/TR2010-014.pdf Cheers, Igor.
<hr />
