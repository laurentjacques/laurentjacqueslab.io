---
title: 'About'
date: Sat, 10 May 2008 16:08:16 +0000
draft: false
---

Dear blog visitor. My name is [Laurent Jacques](http://perso.uclouvain.be/laurent.jacques/ "LJ's homepage") (Laurent is my firstname ;-) ).  I'm Professor in _University of Louvain_ (UCLouvain, Louvain-la-Neuve, Belgium) and my main research interests focus on:

*   Compressed Sensing: theory, quantization, robustness and sensors.
*   Inverse problems solving by promoting union of low-dimensional subspace models (e.g., sparse or low-rank models)
*   Applied mathematics for Optics (Deflectometry), Astronomy (Radio-Interferometry) and biomedical applications (X-Ray Imaging).
*   Theoretical questions linked to new design of sensors (_e.g._, cameras, optical sensors, tomography, hyper-spectral sensors).
*   Representation of data living on "non-conventional" spaces (_e.g._, sphere, manifolds, or graphs).

This blog collects some of my recent readings or ideas in these topics.  Don't hesitate to comment any of my posts!