---
title: 'Some comments on Noiselets'
date: Fri, 20 Aug 2010 22:05:00 +0000
draft: false
tags: ['Compressed Sensing']
---


#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2010-08-27 13:49:59">Aug 5, 2010</time>

It could be doable indeed. I feel more confortable with SS than with randperm nevertheless. But I saw some concentration results on random permutations recently on arxiv that could help.
<hr />
#### 
[Alex]( "akhushnir@gmail.com") - <time datetime="2011-03-26 08:20:03">Mar 6, 2011</time>

Hello. I have two questions. Maybe you can help me to understand. 1) I am not familiar with wavelets and compressed sensing. Could you please give the definition and properties of noiselets in the simple (maybe intuitive) way. 2) You say that there are two wavelet system: Haar and Doubechy which good for noiselets. In which swnce? And is there is another wavelet systems which could works? Thank you very much.
<hr />
#### 
[Gabriel Peyré]( "") - <time datetime="2010-08-24 11:05:18">Aug 2, 2010</time>

Regarding theory I agree that one should use noiselets. It should be doable to do the math for randomization (or spreadspectrum) and then partial hadamard.
<hr />
#### 
[jackdurden]( "laurent.jacques@epfl.ch") - <time datetime="2010-08-23 14:24:07">Aug 1, 2010</time>

Good to know. This scheme is possibly very efficient too. And another variant would be to use a spreadspectrum (i.e. pointwise multiplication by Bernoulli vector before hadamard/noiselet/fourier) instead of the first randperm. However, the question is how to prove that Hadamard functions + randperms provides a flat spectrum for function sparse in the Haar basis. Perhaps this can be proved, but I never read such a proof up to now. For noiselets, the proof is in Coifman et al. Noiselets paper.
<hr />
#### 
[Gabriel Peyré]( "") - <time datetime="2010-08-22 23:50:46">Aug 0, 2010</time>

In my CS experiments, I use a doubly scrambled Hadamard transform, which means, y = x(randperm(n)); y = hadamard(y)); % N\*log(N) operations, you can use dct or any flat ortho basis y = y(randperm(n)); y = y(1:p); % select only p random coefficients What is the advantage of Noiselets over this scheme ? I suspect they work similarely on data sparse in wavelets.
<hr />
#### 
[Compressive Sensing: The Big Picture | spider&#039;s space](http://spiderspace.wordpress.com/2012/05/15/compressive-sensing-the-big-picture/ "") - <time datetime="2012-05-15 10:41:57">May 2, 2012</time>

\[...\] An implementation can be found here. Some useful comments on Noiselets by Laurent \[...\]
<hr />
#### 
[Andrei]( "andrei.tunea@gmail.com") - <time datetime="2012-08-14 18:48:24">Aug 2, 2012</time>

Hi there! I am a newbie to noiselets, but will try to use them in my master thesis. My question is about real values noiselets (Q1). I have the following data vector: \[4 2 5 5\] The fnt1d (from Laurent Jacques) of this vector is: \[3.5000 - 0.5000i ; 4.5000 + 1.5000i ; 4.5000 - 1.5000i ; 3.5000 + 0.5000i\] Now according to the answer to Q1, if I want the real fnt of \[4 2 5 5\], I take the first two imaginary entries from the fnt1d, and concatenate the real and imaginary coefficients to a vector. Something like: \[3.5 4.5 -0.5 1.5\] So I wanted to see if realnoiselet from Justin Romberg yields the same values. But if I use that function I get \[12 8 6 6\]. Which (as I figured out) are the sums and differences of the elements 1 and 3 respectively 2 and 4 of the vector \[3.5 4.5 -0.5 1.5\]. Rearranged and multiplied with 2. Why is that? Is \[12 8 6 6\] the right answer? Does that mean the answer to Q1 is not right? Thank you very much!
<hr />
#### 
[KP]( "kamlesh.pawar@monash.edu") - <time datetime="2013-08-25 06:45:06">Aug 0, 2013</time>

I tried to implement the noiselet matrix using eq.(2) and found that the noiselet matrix od size (4x4) has value 1, -1 and 0. The noiselet matirx that I got for 4x4 was 0.5\*\[0-i, 1+0i, 1-0i, 0+i; 1+0i, 0+i, 0-i, 1-0i; 1-0i, 0-i, 0+i, 1+0i; 0+i, 1-0i, 1+0i, 0-i\] ; This noiselet matrix contradicts with the answer 2 here in this post ? To verify my implementation I also downloaded the code from here http://nuit-blanche.blogspot.com.au/2008/04/monday-morning-algorithm-15-building.html that also provide same result. Can anyone let me know what would be the noiselet matrix of size 4x4 ?
<hr />
#### 
[jackdurden]( "jacques.durden@gmail.com") - <time datetime="2013-08-26 10:16:03">Aug 1, 2013</time>

Thank you for the error detection KP. Except for the global amplitude that may differ from different conventions, your values are correct. I have updated my answer to Q2 above.
<hr />
#### 
[转载：Compressive Sensing: The Big Picture &#8211; FIXBBS](http://www.fixbbs.com/p/10466342.html "") - <time datetime="2021-02-06 13:38:45">Feb 6, 2021</time>

\[…\] An implementation can be found here. Some useful comments on Noiselets by Laurent \[…\]
<hr />
