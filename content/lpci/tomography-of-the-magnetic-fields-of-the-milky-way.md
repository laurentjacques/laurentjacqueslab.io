---
title: 'Tomography of the magnetic fields of the Milky Way?'
date: Wed, 07 Dec 2011 20:44:25 +0000
draft: false
tags: ['General', 'inverse problem', 'tomography']
---

I have just found this "new" (well 150 years old actually) tomographical method... for measuring the **magnetic field of our own galaxy**

"[New all-sky map shows the magnetic fields of the Milky Way with the highest precision](http://www.mpa-garching.mpg.de/mpa/institute/news_archives/news1112_fara/news1112_fara-en.html)"  
by Niels Oppermann et al. (arxiv work available [here](http://arxiv.org/abs/1111.6186))

Selected excerpt:

_"... One way to measure cosmic magnetic fields, which has been known for over 150 years, makes use of an effect known as Faraday rotation. When polarized light passes through a magnetized medium, the plane of polarization rotates. The amount of rotation depends, among other things, on the strength and direction of the magnetic field. Therefore, observing such rotation allows one to investigate the properties of the intervening magnetic fields."_

Mmmm... very interesting, at least for my personal knowledge of the wonderful tomographical problem zoo (amongst gravitational lensing, interferometry, MRI, deflectometry).

P.S. Wow... 16 months without any post here. I'm really bad.