---
title: 'When Buffon''s needle problem meets the Johnson-Lindenstrauss Lemma'
date: Fri, 06 Sep 2013 14:27:11 +0000
draft: false
tags: ['Compressed Sensing', 'General', 'Johnson Lindenstrauss']
---


#### 
[Quasi-isometric embeddings of vector sets with quantized sub-Gaussian projections | Le Petit Chercheur Illustré](https://yetaspblog.wordpress.com/2015/04/28/quasi-isometric-embeddings-of-vector-sets-with-quantized-sub-gaussian-projections/ "") - <time datetime="2015-04-28 10:47:20">Apr 2, 2015</time>

\[…\] explained in my previous post on quantized embedding and the funny connection with Buffon’s needle problem, I have recently noticed that for finite \[…\]
<hr />
