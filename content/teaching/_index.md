---
title: "Teaching"
author: ["Laurent Jacques"]
draft: false
title: Teaching
date: "2018-06-28T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `assets/media/` folder).
header:
  caption: ""
  image: ""
---

## Courses {#courses}

-   [LELEC2885](https://www.uclouvain.be/en-cours-LELEC2885): "Image processing and computer vision" (MSc, from 2010-11 till now)
-   [LELEC2900](https://www.uclouvain.be/en-cours-LELEC2900): "Signal Processing" (MSc, from 2018-19 till now)
-   [LINMA2120](https://www.uclouvain.be/en-cours-LINMA2120): "Applied mathematics seminar" (MSc, from 2019-20 till now)
-   [LINMA2360](https://uclouvain.be/cours-linma2360): "Project in mathematical engineering" (MSc, from 2021-22 till now)
-   [LEPL1109](https://www.uclouvain.be/en-cours-LEPL1109): "Statistiques et science des données" (BSc, from 2020-21 till now)
-   [LELEC2102](https://uclouvain.be/en-cours-lelec2102): "Project in Electrical Engineering: Integration of wireless embedded sensing systems" (MSc, from 2021 till now)
-   [LELEC2103](https://uclouvain.be/en-cours-lelec2103): "Project in Electrical Engineering: Optimization of wireless embedded sensing systems" (MSc, from 2021 till now)
-   [LEPL1105](https://www.uclouvain.be/en-cours-LEPL1105): "Analyse II" (BSc, from 2022-23 till now)
-   [LGBIO1111](https://uclouvain.be/en-cours-2024-lgbio1111): "Cell biology and physiology" (BSc, from 2024-25 till now)
-   [Admission Exams in Engineering (Geometry)](https://uclouvain.be/fr/facultes/epl/matieres-de-l-examen.html) (from 2017 to 2021)
