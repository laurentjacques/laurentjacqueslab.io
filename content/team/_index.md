---
title: "Team"
author: ["Laurent Jacques"]
draft: false
title: Team members
date: ''
reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `assets/media/` folder).
header:
  caption: ""
  image: ""
---

{{< figure src="/ox-hugo/team.jpg" width="50%" >}}


## PhD-students and postdocs: {#phd-students-and-postdocs}

-   Nicolas Mil-Homens Cavaco (PhD-student, FNRS, 2024-)
-   Rémi Delogne (PhD-student, TA, UCLouvain, 2020-)
-   [Jérome Eertmans](https://eertmans.be/) (PhD-student, TA, UCLouvain, co-advised with C. Oestges, 2021-)
-   Anatole Moureaux (PhD-student, co-advised with F. Abreu Araujo, 2022-)
-   Clément Thomas (PhD-student, co-advised with [M. Georges, CSL/ULiège](https://www.uliege.be/cms/c_9054334/fr/repertoire?uid=U027964), 2023-)


## Former PhD-students, researchers and postdocs: {#former-phd-students-researchers-and-postdocs}

-   Dr Olivier Leblanc (FNRS, 2020-2024)
-   Dr Gilles Monnoyer de Galland de Carnières (2019-2023, FNRS, co-advised with L. Vandendorpe)
-   Dr [Thomas Feuillen](http://thomas-feuillen.com/) (2015-2021, TA UCLouvain, co-advised with L. Vandendorpe)
-   Dr Alexander Stollenwerk (Postdoc, PDR FNRS, 2020-2021)
-   Dr Stéphanie Guérit (2014-2021, TA UCLouvain, co-advised with J. Lee)
-   Dr [Vincent Schellekens](https://perso.uclouvain.be/vincent.schellekens/) (2017-2021, FNRS)
-   Dr Benoît Pairet (2014-2020, FRIA/FNRS)
-   Dr [Pierre-Yves Gousenbourger](https://perso.uclouvain.be/pygousenbourger/) (2014-2020, TA UCLouvain, co-advised with P.-A. Absil)
-   Dr Wissam Benjilali (2015-2019, CEA-Leti, main advisors: G. Sicard, W. Guicquero)
-   Dr [Amirafshar Moshtaghpour](https://www.rfi.ac.uk/about/people/dr-amirafshar-moshtaghpour/) (2015-2019, FRIA/FNRS)
-   Dr Chunlei Xu (Postdoc, AlterSense Project, MIS-FNRS, 2016-2018)
-   Dr Kévin Degraux (2013-2018, FNRS)
-   Dr Valerio Cambareri (Postdoc, AlterSense Project, MIS-FNRS, 2015-2017)
-   Dr [Augustin Cosse](http://www.augustincosse.com/) (2010-2016, co-advised with L. Demanet)
-   Dr Adriana Gonzalez Gonzalez (2010-2016, TA UCLouvain)
-   Dr [Prasad Sudhakar](http://psudhaka.github.io/) (2011-2014, Postdoc, DETROIT project)
-   Alexandre Saint (2011-2012, engineer, DETROIT project)
-   Muhammad Arsalan (2014, PhD-student)
