---
title: "Quantized Iterative Hard Thresholding: Bridging 1-bit and High-Resolution Quantized Compressed Sensing"
author: ["Laurent Jacques"]
tags: [1, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Kévin Degraux
- Christophe De Vleeschouwer
tags:
- '"1-bit compressed sensing"'
- '"quantization"'
- '"iterative hard thresholding"'
- '"binary embedding"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:36+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:36.296637Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of International Conference on Sampling Theory and Applications
  2013, Bremen, Germany*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134466
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1305.1786
---

**Abstract**: In this work, we show that reconstructing a sparse signal from quantized compressive measurement can be achieved in an unified formalism whatever the (scalar) quantization resolution, i.e., from 1-bit to high resolution assumption. This is achieved by generalizing the iterative hard thresholding (IHT) algorithm and its binary variant (BIHT) introduced in previous works to enforce the consistency of the reconstructed signal with respect to the quantization model. The performance of this algorithm, simply called quantized IHT (QIHT), is evaluated in comparison with other approaches (e.g., IHT, basis pursuit denoise) for several quantization scenarios.
