---
title: "Fast Method to Fit a  C1 Piecewise-Bézier Function to Manifold-Valued Data Points: How Suboptimal is the Curve Obtained on the Sphere S2?"
author: ["Laurent Jacques"]
tags: [1, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Pierre-Yves Gousenbourger
- Laurent Jacques
- Pierre-Antoine Absil
tags:
- '"INMA:PAI-DYSCO"'
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:31+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:31.563013Z'
publication_types:
- '1'
abstract: 
publication: '*Geometric Science of Information. GSI 2017. Lecture Notes in Computer
  Science*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/189233
doi: https://dx.doi.org/10.1007/978-3-319-68445-1_69
---

**Abstract**: We propose an analysis of the quality of the fitting method proposed in Gousenbourger et al., 2017 (ESANN2017 proceedings). This method fits smooth paths to manifold-valued data points using C1 piecewise-Bézier functions. This method is based on the principle of minimizing an objective function composed of a data-attachment term and a regularization term chosen as the mean squared acceleration of the path.  However, the method strikes a tradeoff between speed and accuracy by following a strategy that is guaranteed to yield the optimal curve only when the manifold is linear. In this paper, we focus on the sphere S2. We compare the quality of the path returned by the algorithms from Gousenbourger et al., 2017 (ESANN2017 proceedings) with the path obtained by minimizing, over the same search space of C1 piecewise-Bézier curves, a finite-difference approximation of the objective function by means of a derivative-free manifold-based optimization method.
