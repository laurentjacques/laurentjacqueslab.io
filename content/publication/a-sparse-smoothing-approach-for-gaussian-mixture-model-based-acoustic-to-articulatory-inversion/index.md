---
title: "A sparse smoothing approach for Gaussian mixture model based acoustic-to-articulatory inversion"
author: ["Laurent Jacques"]
tags: [1, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Prasad Sudhakara Murthy
- Laurent Jacques
- Prasanta Kumar Ghosh
tags:
- '"acoustic-to-articulatory inversion"'
- '"smoothing"'
- '"Gaussian mixture model"'
- '"sparsity"'
- '"chambolle-pock"'
- '"L1 minimization"'
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:35.139642Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of IEEE International Conference on Acoustics, Speech and
  Signal Processing (ICASSP), 2014*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/145733
doi: https://dx.doi.org/10.1109/ICASSP.2014.6854157
---

**Abstract**: It is well-known that the performance of the Gaussian mixture model (GMM) based acoustic-to-articulatory inversion (AAI) improves by either incorporating smoothness constraint directly in the inversion criterion or smoothing (low-pass filtering) estimated articulator tra- jectories in a post-processing step, where smoothing is performed independently of the inversion. As the low-pass filtering is inde- pendent of inversion, the smoothed articulator trajectory samples no longer remain optimal as per the inversion criterion. In this work, we propose a sparse smoothing technique which constrains the smoothed articulator trajectory to be different from the estimated trajectory only at a sparse subset of samples while simultaneously achieving the required degree of smoothness. Inversion experi- ments on the articulatory database show that the sparse smoothing achieves an AAI performance similar to that using low-pass filtering but in sparse smoothing ∼15% (on average) of the samples in the smoothed articulator trajectory remain identical to those in the esti- mated articulator trajectory thereby preserve their AAI optimality as opposed to 0% in low-pass filtering.
