---
title: "Mitigating memory requirements for random trees/ferns"
author: ["Laurent Jacques"]
tags: [1, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- Christophe De Vleeschouwer
- Anthony Legrand
- Laurent Jacques
- Martial Hebert
tags:
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:34+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:34.437015Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of the IEEE International Conference on Image Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/167990
doi: https://dx.doi.org/10.1109/ICIP.2015.7350793
---

**Abstract**: Randomized sets of binary tests have appeared to be quite effective in solving a variety of image processing and vision problems. The exponential growth of their memory usage with the size of the sets however hampers their implementation on the memory-constrained hardware generally available on low-power embedded systems. Our paper addresses this limitation by formulating the conventional semi-naive Bayesian ensemble decision rule in terms of posterior class probabilities, instead of class conditional distributions of binary tests realizations. Subsequent clustering of the posterior class distributions computed at training allows for sharp reduction of large binary tests sets memory footprint, while preserving their high accuracy.  Our validation considers a smart metering applicative scenario, and demonstrates that up to 80% of the memory usage can be saved, at constant accuracy.
