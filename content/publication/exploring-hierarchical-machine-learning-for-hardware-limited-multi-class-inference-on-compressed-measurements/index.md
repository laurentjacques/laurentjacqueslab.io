---
title: "Exploring Hierarchical Machine Learning for Hardware-Limited Multi-Class Inference on Compressed Measurements"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Wissam Benjilali
- William Guicquero
- Laurent Jacques
- Gilles Sicard
tags:
- '"Hierarchical learning"'
- '"compressive sensing"'
- '"clustering"'
- '"SVM"'
- '"embedded multi-class inference"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:28.039811Z'
publication_types:
- '1'
abstract: 
publication: '*2019 IEEE International Symposium on Circuits and Systems (ISCAS)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/216657
doi: https://dx.doi.org/10.1109/iscas.2019.8702423
---

**Abstract**: This paper explores hierarchical clustering methods to learn a hierarchical multi-class classifier on compressed measurements in the context of highly constrained hardware (e.g., always-on ultra low power vision systems). In contrast to the popular multi-class classification approaches based on multiple binary classifiers (i.e., one-vs.-all and one-vs.one [1]), a hierarchical classifier requires only O(log 2 C) binary classifiers in a decision tree. In this work, we investigate three clustering methods used to construct balanced clusters at each node thus reducing the depth of the decision tree in order to lower hardware requirements to its minimum. A binary Support Vector Machine (SVM) [2] classifier is then learned on Compressive Sensing measurements [3] at each node of the hierarchical tree. Our results, based on two object recognition databases (AT&T and COIL-100 databases), show the competitiveness of hierarchical classification in terms of hardware requirements (lower memory and computational complexity) as well as its classification accuracy.
