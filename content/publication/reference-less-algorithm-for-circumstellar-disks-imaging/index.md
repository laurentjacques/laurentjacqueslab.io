---
title: "Reference-less algorithm for circumstellar disks imaging"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Benoît Pairet
- Faustine Cantalloube
- Laurent Jacques
tags:
- '"image processing"'
- '"exoplanet detection"'
- '"astronomy"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:30+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:30.766079Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of iTWIST'18*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211765
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1812.01333
---

**Abstract**: Circumstellar disks play a key role in the understanding of stellar systems.  Direct imaging of such extended structures is a challenging task. Current post-processing techniques, first tailored for exoplanets imaging, tend to produce deformed images of the circumstellar disks, hindering our capability to study their shape and photometry in details. We address here the reasons of this shortcoming and propose an algorithm that produces more faithful images of disks taken with ground-based telescopes.  We also show that our algorithm is a good candidate for exoplanets imaging. We then explain how our approach can be extended in the form of a regularized inverse problem.
