---
title: "Grid Hopping: Accelerating Direct Estimation Algorithms for Multistatic FMCW Radar"
author: ["Laurent Jacques"]
tags: [2, 2024]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Gilles Monnoyer
- Thomas Feuillen
- Maxime Drouguet
- Laurent Jacques
- Luc Vandendorpe
tags: # One keywords per item "-" with format - '"keyword"'
- '""'
#categories: []
date: '2024-10-28'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named Symbol’s value as variable is void: featured.jpg/png to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. Symbol’s value as variable is void: projects references Symbol’s value as variable is void: content/project/deep-learning/index.md.
#   Otherwise, set Symbol’s value as variable is void: projects.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
abstract: ''
publication: '*IEEE Transactions on Signal Processing*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.1109/TSP.2024.3465842'

# custom links
links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/292969 #add dial handle or comment this out
---

**Abstract**: In radars, sonars, or for sound source localization, sensor networks enable the estimation of parameters that cannot be unambiguously recovered by a single sensor. The estimation algorithms designed for this context are commonly divided into two categories: the two-step methods, separately estimating intermediate parameters in each sensor before combining them; and the single-step methods jointly processing all the received signals. This paper provides a general framework, coined Grid Hopping (GH), unifying existing techniques to accelerate the single-step methods, known to provide robust results with a higher computational time. GH exploits interpolation to approximate evaluations of correlation functions from the coarser grid used in two-step methods onto the finer grid required for single-step methods, hence “hopping” from one grid to the other. The contribution of this paper is two-fold. We first formulate GH, showing its particularization to existing acceleration techniques used in multiple applications. Second, we derive a novel theoretical bound characterizing the performance loss caused by GH in simplified scenarios. We finally provide Monte-Carlo simulations demonstrating how GH preserves the advantages of both the single-step and two-step approaches and compare its performance when used with multiple interpolation techniques.
