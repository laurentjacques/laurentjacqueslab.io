---
title: "Compressive Acquisition of Sparse Deflectometric Maps"
author: ["Laurent Jacques"]
tags: [1, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Prasad Sudhakara Murthy
- Laurent Jacques
- Adriana Gonzalez Gonzalez
- Xavier Dubois
- Philippe Antoine
- Luc Joannes
tags:
- '"CISM:CECI"'
- '"schlieren"'
- '"inverse problem"'
- '"deflectometry"'
- '"convex optimisation"'
- '"compressed sensing"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:35.754927Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of the  10th International Conference on Sampling Theory
  and Applications (SampTA)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134467
---

**Abstract**: Schlieren deflectometry aims at measuring deflections of light rays from transparent objects, which is subsequently used to characterize the objects. With each location on a smooth object surface a sparse deflection map (or spectrum) is associated. In this paper, we demonstrate the compressive acquisition and reconstruction of such maps, and the usage of deflection information for object characterization, using a schlieren deflectometer. To this end, we exploit the sparseness of deflection maps and we use the framework of spread spectrum compressed sensing. Further, at a second level, we demonstrate how to use the deflection information optimally to reconstruct the distribution of refractive index inside an object, by exploiting the sparsity of refractive index maps in gradient domain.
