---
title: "Piecewise-Bezier C1 smoothing on manifolds with application to wind field estimation"
author: ["Laurent Jacques"]
tags: [1, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Pierre-Yves Gousenbourger
- Estelle Massart
- Antoni Musolas
- Pierre-Antoine Absil
- Julien Hendrickx
- Laurent Jacques
- Youssef Marzouk
tags:
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:31+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:31.750440Z'
publication_types:
- '1'
abstract: 
publication: '*25th European Symposium on Artificial Neural Networks, Computational
  Intelligence and Machine Learning (ESANN 2017)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/186413
---

**Abstract**: We propose an algorithm for fitting C1 piecewise-Bezier curves to (possibly corrupted) data points on manifolds. The curve is chosen as a compromise between proximity to data points and regularity. We apply our algorithm as an example to fit a curve to a set of low-rank covariance matrices, a task arising in wind field modeling. We show that our algorithm has denoising abilities for this application.
