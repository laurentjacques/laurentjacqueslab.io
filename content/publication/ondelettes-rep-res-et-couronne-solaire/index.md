---
title: "Ondelettes, repères et couronne solaire"
author: ["Laurent Jacques"]
tags: [7, 2004]
draft: false
authors:
- Laurent Jacques
tags:
- '"Frame"'
- '"Stereographic"'
- '"Sphere"'
- '"Directional"'
- '"Wavelet"'
- '"Ondelettes"'
- '"Solaire"'
- '"Bright point"'
- '"Couronne"'
- '"Repère"'
- '"Multiselectivity"'
- '"Multisélectivité"'
- '"Solar"'
- '"Corona"'
- '"Cosmic"'
- '"Image"'
- '"Signal"'
- '"Regularity"'
- '"Holder"'
- '"Lipschitz"'
- '"Directionnelle"'
- '"Sphère"'
- '"Stéréographique"'
categories: []
date: '2004-01-01'
lastmod: 2021-08-06T23:54:43+02:00
featured: false
draft: false

publishDate: '2021-08-06T21:54:43.530264Z'
publication_types:
- '7'
abstract: 
publication: ''
# custom links
links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/5365 #add dial handle or comment this out
---

**Abstract**: Dans cette thèse, nous explorons premièrement la notion de directionnalité lors de la conception de repères d'ondelettes du plan. Cette propriété, qui semble essentielle pour la vision biologique, donne lieu à une meilleure représentation des contours d'objets dans les décompositions d'images utilisant ces repères.  Elle génère en outre une redondance supplémentaire qui exploitée à bon escient, permet par exemple de réduire les effets d'un bruit additif (gaussien). Nous montrons également comment cette directionnalité, généralement perçue comme un paramètre figé, peut être adaptée localement aux éléments d'une image. Nous définissons ainsi le concept d'analyse d'images multisélective. Dans ce cadre, des règles de récurrence héritées d'une analyse multirésolution circulaire associent des ondelettes d'une certaine sélectivité angulaire pour générer des ondelettes de plus faible directionnalité jusqu'à l'obtention d'une ondelette totalement isotrope.  Dans le cas d'un repère d'ondelettes linéaire, ces différents niveaux de sélectivité ont la possibilité de s'ajuster localement au contenu d'une image. Nous constatons par ailleurs que cette adaptabilité fournit de meilleures reconstructions que les méthodes à sélectivité fixe lors d'approximations non linéaires d'images.  Cette thèse traite ensuite du problème de l'analyse de données représentées sur la sphère. Il a été établi précédemment [AV99] que la transformée continue en ondelettes (CWT) s'étend à cet espace par l'emploi d'une dilatation stéréographique respectant la compacité de S2. Dans certain cas, il est utile de réduire la redondance de cette transformée, ne fut ce que pour faciliter le traitement des données dans l'espace multi-échelle généré. Nous étudions par conséquent comment créer des repères sphériques semi-continus, où seule l'échelle est échantillonnée, et totalement discrétisés. Nous tirons parti dans ce dernier cas de grilles sphériques équi-angulaires et des règles de quadrature associées pour obtenir des conditions suffisantes à la reconstruction des fonctions analysées. Les capacités d'analyse et de synthèse de repères d'ondelettes DOG sont également testées sur des exemples de données sphériques. Une dernière partie de ce document est dédiée à l'étude d'un objet physique étonnant : la couronne solaire. Cette couche extérieure du soleil est observée depuis 1996 par l'expérience EIT à bord du satellite SoHO dans différentes longueurs d'onde de l'ultraviolet lointain. La compréhension physique des multiples phénomènes apparaissant dans la couronne solaire passe par le traitement automatique des images EIT. Dans cette tâche, nous nous limitons à deux problèmes particuliers.  Nous utilisons premièrement la CWT et sa capacité à analyser la régularité locale d'une image pour gommer les traces laissées par les rayons cosmiques, majoritairement non solaires, sur les enregistrements EIT. Deuxièmement, la couronne solaire contient des éléments de faible taille (<60arcsec) nommés points brillants (ou BPs pur Bright points). Ceux-ci trouvent leur origine dans l'échauffement local du plasma coronal sous l'action du champs magnétique solaire. En abordant une approche similaire à celle développée en [Bij99], nous étudions comment sélectionner et caractériser ces BPS en décomposant une image en ses objets constitutifs. Ces derniers sont issus de tubes de maxima dans la description multi-échelle de l'image, c'est-à-dire d'une généralisation discrète des lignes de maxima de la CWT.
