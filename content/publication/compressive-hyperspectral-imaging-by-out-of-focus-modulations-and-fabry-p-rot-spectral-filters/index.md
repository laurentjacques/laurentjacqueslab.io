---
title: "Compressive Hyperspectral Imaging by Out-of-Focus Modulations and Fabry-Pérot Spectral Filters"
author: ["Laurent Jacques"]
tags: [1, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Kévin Degraux
- Valerio Cambareri
- Bert Geelen
- Laurent Jacques
- Gauthier Lafruit
- Gianluca Setti
tags: []
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:35.281789Z'
publication_types:
- '1'
abstract: 
publication: '"*Proceedings of the second "international Traveling Workshop on Interactions between Sparse models and Technology" (iTWIST''14)*"'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/157076
---

**Abstract**: We describe a compressive hyperspectral imaging scheme that randomly convolves each spectral band of the data cube. This independent sensing of each wavelength relies on a tiling of Fabry-Pérot filters incorporated in the CMOS pixel grid. The compressive observations are induced by an out-of-focus spa- tial light modulation joined to focusing optics. While our design extends a recent monochromatic imaging scheme to the hyper- spectral domain, we show that our model reaches good reconstruc- tion performances when compared to more ideal sensing methods.
