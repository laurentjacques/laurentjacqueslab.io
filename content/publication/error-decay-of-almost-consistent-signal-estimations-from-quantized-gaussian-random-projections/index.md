---
title: "Error Decay of (almost) Consistent Signal Estimations from Quantized Gaussian Random Projections"
author: ["Laurent Jacques"]
tags: [2, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
tags:
- '"AlterSense"'
- '"Gaussian random measurement"'
- '"scalar quantization scheme"'
- '"consistent signal estimation error decay"'
- '"quantized Gaussian random projection"'
- '"error bounds"'
- '"resolution uniform scalar quantization"'
- '"Gaussian random frame"'
- '"consistent signal reconstruction decay"'
- '"quantized compressed sensing"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:21.896377Z'
publication_types:
- '2'
publication: '*IEEE Transactions on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178296
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1406.0022
doi: https://dx.doi.org/10.1109/TIT.2016.2573313
---

**Abstract**: This paper provides new error bounds on "consistent" reconstruction methods for signals observed from quantized random projections. Those signal estimation techniques guarantee a perfect matching between the available quantized data and a new observation of the estimated signal under the same sensing model. Focusing on dithered uniform scalar quantization of resolution \\(\delta>0\\), we prove first that, given a Gaussian random frame of \\(\mathbb R^N\\) with \\(M\\) vectors, the worst-case \\(\ell\_2\\)-error of consistent signal reconstruction decays with high probability as \\(O(\frac{K}{M} \log \frac{M}{N} \sqrt{K^{3}})\\) uniformly for all signals of the unit ball \\(\mathbb B^N \subset \mathbb R^N\\). Up to a log factor, this matches a known lower bound in \\(\Omega(N/M)\\) and former empirical validations in \\(O(N/M)\\). Equivalently, if \\(M\\) exceeds a minimal number of frame coefficients growing like \\(O(\frac{K}{M} \log\frac{MN}{\sqrt K^{3}})\\), any vectors in \\(\mathbb B^N\\) with \\(M\\) identical quantized projections are at most \\(\epsilon\_0\\) apart with high probability. Second, in the context of Quantized Compressed Sensing with \\(M\\) Gaussian random measurements and under the same scalar quantization scheme, consistent reconstructions of \\(K\\)-sparse signals of \\(\mathbb R^N\\) have a worst-case error that decreases with high probability as uniformly for all such signals.  Finally, we show that the proximity of vectors whose quantized random projections are only approximately consistent can still be bounded with high probability. A certain level of corruption is thus allowed in the quantization process, up to the appearance of a systematic bias in the reconstruction error of (almost) consistent signal estimates."'
