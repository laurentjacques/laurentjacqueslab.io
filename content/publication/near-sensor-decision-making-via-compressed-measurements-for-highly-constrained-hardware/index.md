---
title: "Near Sensor Decision Making via Compressed Measurements for Highly Constrained Hardware"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Wissam Benjilali
- William Guicquero
- Laurent Jacques
- Gilles Sicard
tags:
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:28.315957Z'
publication_types:
- '1'
abstract: 
publication: '*2019 53rd Asilomar Conference on Signals, Systems, and Computers*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242109
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1901.06527
doi: https://dx.doi.org/10.1109/ieeeconf44664.2019.9048849
---

**Abstract**: This work presents and compare three realistic scenarios to perform near sensor decision making based on Dimensionality Reduction (DR) techniques of high dimensional signals in the context of highly constrained hardware. The studied DR techniques are learned according to two alternative strategies: one whose parameters are learned in a compressed signal representation, as being achieved by random projections in a compressive sensing device, the others being performed in the original signal domain. For both strategies, the inference is yet indifferently performed in the compressed domain with dedicated algorithm depending on the selected learning technique.  Our results, based on two common datasets, show that performing the inference in the compressed domain represents a competitive approach compared to the classical classification strategy (inference in the original signal domain) regarding memory and computational requirements. We also exhibit the fact that it is especially well suited for embedded applications in the context of hardware implementations with limited resources even with specific hardware design and limitations.
