---
title: "Blind Deconvolution of PET Images using Anatomical Priors"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Stéphanie Guérit
- Adriana González
- Anne Bol
- John Aldo Lee
- Laurent Jacques
tags:
- '"PET imaging"'
- '"blind deconvolution"'
- '"inverse problem"'
- '"total variation"'
- '"anatomical prior"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.715655Z'
publication_types:
- '1'
abstract: 
publication: "*iTWIST'16*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/175260
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1609.04167
---

**Abstract**: Images from positron emission tomography (PET) provide metabolic information about the human body. They present, however, a spatial resolution that is limited by physical and instrumental factors often modeled by a blurring function. Since this function is typically unknown, blind deconvolution (BD) techniques are needed in order to produce a useful restored PET image. In this work, we propose a general BD technique that restores a low resolution blurry image using information from data acquired with a high resolution modality (e.g., CT-based delineation of regions with uniform activity in PET images). The proposed BD method is validated on synthetic and actual phantoms.
