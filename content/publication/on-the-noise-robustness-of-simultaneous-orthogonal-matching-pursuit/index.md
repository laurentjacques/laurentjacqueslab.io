---
title: "On the Noise Robustness of Simultaneous Orthogonal Matching Pursuit"
author: ["Laurent Jacques"]
tags: [2, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Francois Determe
- Jérôme Louveaux
- Laurent Jacques
- Francois Horlin
tags:
- '"AERC"'
- '"Simultaneous orthogonal matching pursuit"'
- '"compressed sensing"'
- '"restricted isometry property"'
- '"asymptotic recovery"'
- '"Gaussian"'
- '"noise"'
- '"MMV"'
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:21.319192Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Signal Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/182869
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1602.03400
doi: https://dx.doi.org/10.1109/TSP.2016.2626244
---

**Abstract**: In this paper, the joint support recovery of several sparse signals whose supports present similarities is examined. Each sparse signal is acquired using the same noisy linear measurement process, which returns fewer observations than the dimension of the sparse signals. The measurement noise is assumed additive, Gaussian, and admits different variances for each sparse signal that is measured.  Using the theory of compressed sensing, the performance of simultaneous orthogonal matching pursuit (SOMP) is analysed for the envisioned signal model. The cornerstone of this paper is a novel analysis method upper bounding the probability that SOMP recovers at least one incorrect entry of the joint support during a prescribed number of iterations. Furthermore, the probability of SOMP failing is investigated whenever the number of sparse signals being recovered simultaneously increases and tends to infinity. In particular, convincing observations and theoretical results suggest that SOMP committing no mistake in the noiseless case does not guarantee the absence of error in the noisy case whenever the number of acquired sparse signals scales to infinity. Finally, simulation results confirm the validity of the theoretical results.
