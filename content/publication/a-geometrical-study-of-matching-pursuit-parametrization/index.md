---
title: "A geometrical study of matching pursuit parametrization"
author: ["Laurent Jacques"]
tags: [2, 2008]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Christophe De Vleeschouwer
tags:
- '"matching pursuit"'
- '"manifold analysis"'
- '"parametric dictionary"'
- '"signal decomposition"'
- '"differential Riemannian geometry"'
categories: []
date: '2008-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:24.938193Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Signal Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/36518
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/0801.3372
doi: https://dx.doi.org/10.1109/TSP.2008.917379
url_code: MPGeom.zip
---

**Abstract**: This paper studies the effect of discretizing the parametrization of a dictionary used for matching pursuit (MP) decompositions of signals. Our approach relies con viewing the continuously parametrized dictionary as an embedded manifold in the signal space on which the tools of differential, (Riemannian) geometry can be applied.  The main contribution of this paper is twofold. First, we prove that if a discrete dictionary reaches a minimal density criterion, then the corresponding discrete MP (dMP) is equivalent in terms of convergence to a weakened hypothetical continuous MP. Interestingly, the corresponding weakness factor depends on a density measure of the discrete dictionary. Second, we show that the insertion of a simple geometric gradient ascent optimization on the atom dMP selection maintains the previous comparison but with a weakness factor at least two times closer to unity than without optimization.  Finally, we present numerical experiments confirming our theoretical predictions for decomposition of signals and images on regular discretizations of dictionary parametrizations.
