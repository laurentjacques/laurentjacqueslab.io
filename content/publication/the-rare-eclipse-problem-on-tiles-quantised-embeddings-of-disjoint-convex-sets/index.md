---
title: "The Rare Eclipse Problem on Tiles: Quantised Embeddings of Disjoint Convex Sets"
author: ["Laurent Jacques"]
tags: [1, 2017, "bibtex"]
draft: false
subtitle: ''
summary: ''
authors:
- Valerio Cambareri
- Chunlei Xu
- Laurent Jacques
tags:
- '"random projection"'
- '"quantization"'
- '"dithering"'
- '"rare eclipse problem"'
- '"classification"'
- '"non-linear dimensionality reduction"'
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.075117Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of the 12th International Conference of Sampling Theory
  and Applications, Tallinn, 2017*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/187188
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1702.04664
doi: https://dx.doi.org/10.1109/SAMPTA.2017.8024364
---

**Abstract**: Quantised random embeddings are an efficient dimensionality reduction technique which preserves the distances of low-complexity signals up to some controllable additive and multiplicative distortions. In this work, we instead focus on verifying when this technique preserves the separability of two disjoint closed convex sets, i.e., in a quantised view of the "rare eclipse problem" introduced by Bandeira et al. in 2014. This separability would ensure exact classification of signals in such sets from the signatures output by this non-linear dimensionality reduction.  We here present a result relating the embedding's dimension, its quantiser resolution and the sets' separation, as well as some numerically testable conditions to illustrate it. Experimental evidence is then provided in the special case of two \\(\ell\_2\\)-balls, tracing the phase transition curves that ensure these sets' separability in the embedded domain.
