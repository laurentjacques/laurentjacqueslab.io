---
title: "A Quantized Johnson Lindenstrauss Lemma: The Finding of Buffon's Needle"
author: ["Laurent Jacques"]
tags: [2, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
tags:
- '"AlterSense"'
- '"Compressed sensing"'
- '"Johnson Lindenstrauss lemma"'
- '"nonlinear dimensionality reduction"'
- '"quantization"'
- '"random projections"'
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.433470Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/165738
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1309.1507
- name: ipynb
  url: TestingQuasiIsometricEmbedding.ipynb
- name: Demo
  url: demo/a-quantized-johnson-lindenstrauss-lemma-the-finding-of-buffon-s-needle-demo/
doi: https://dx.doi.org/10.1109/TIT.2015.2453355
---

**Abstract**: In 1733, Georges-Louis Leclerc, Comte de Buffon in France, set the ground of geometric probability theory by defining an enlightening problem: What is the probability that a needle thrown randomly on a ground made of equispaced parallel strips lies on two of them? In this work, we show that the solution to this problem, and its generalization to \\(N\\) dimensions, allows us to discover a quantized form of the Johnson-Lindenstrauss (JL) Lemma, i.e., one that combines a linear dimensionality reduction procedure with a uniform quantization of precision \\(\delta>0\\). In particular, given a finite set \\(\mathcal S \subset \mathbb R^N\\) of \\(S\\) points and a distortion level \\(\epsilon>0\\), as soon as \\(M > M\_0 = O(\epsilon^-2 log S)\\), we can (randomly) construct a mapping from \\((\mathcal S, \ell\_2)\\) to \\((\delta\mathbb Z^M, \ell\_1)\\) that approximately preserves the pairwise distances between the points of \\(\mathcal S\\).  Interestingly, compared to the common JL Lemma, the mapping is quasi-isometric and we observe both an additive and a multiplicative distortions on the embedded distances.  These two distortions, however, decay as \\(O(\sqrt{\log S}/M)\\) when \\(M\\) increases.  Moreover, for coarse quantization, i.e., for high \\(\delta\\) compared to the set radius, the distortion is mainly additive, while for small \\(\delta\\) we tend to a Lipschitz isometric embedding. Finally, we prove the existence of a "nearly" quasi-isometric embedding of \\((\mathcal S, \ell\_2)\\) into \\((\delta\mathbb Z^M, \ell\_2)\\). This one involves a non-linear distortion of the \\(\ell\_2\\)-distance in \\(\mathcal S\\) that vanishes for distant points in this set. Noticeably, the additive distortion in this case is slower, and decays as \\(O(\sqrt[4]{\log S}/M)\\).

----
