---
title: "Discriminative and efficient label propagation on complementary graphs for multi-object tracking"
author: ["Laurent Jacques"]
tags: [2, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Amit Kumar K.C
- Laurent Jacques
- Christophe De Vleeschouwer
tags:
- '"tracking"'
- '"video analysis"'
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:21.182129Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Pattern Analysis and Machine Intelligence*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/172873
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1504.01124 
doi: https://dx.doi.org/10.1109/TPAMI.2016.2533391
---

**Abstract**: Given a set of detections, detected at each time instant independently, we investigate how to associate them across time. This is done by propagating labels on a set of graphs, each graph capturing how either the spatiotemporal or the appearance cues promote the assignment of identical or distinct labels to a pair of detections.  The graph construction is motivated by a locally linear embedding of the detection features. Interestingly, the neighborhood of a node in appearance graph is defined to include all the nodes for which the appearance feature is available (even if they are temporally distant). This gives our framework the uncommon ability to exploit the appearance features that are available only sporadically. Once the graphs have been defined, multi-object tracking is formulated as the problem of finding a label assignment that is consistent with the constraints captured each graph, which results into a difference of convex (DC) program. We propose to decompose the global objective function into node-wise subproblems. This not only allows a computationally efficient solution, but also supports an incremental and scalable construction of the graph, thereby making the framework applicable to large graphs and practical tracking scenarios.  Moreover, it opens the possibility of parallel implementation.
