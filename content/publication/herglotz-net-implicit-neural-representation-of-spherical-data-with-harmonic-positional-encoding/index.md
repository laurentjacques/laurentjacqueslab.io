---
title: "Herglotz-NET: Implicit Neural Representation of Spherical Data with Harmonic Positional Encoding"
author: ["Laurent Jacques"]
tags: [3, 2025]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Théo Hanon
- Nicolas Mil-Homens Cavaco
- John Kiely
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"Herglotz"'
- '"spherical harmonics"'
- '"spectral analysis"'
- '"implicit neural representation"'
#categories: []
date: '2025-02-20'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named Symbol’s value as variable is void: featured.jpg/png to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. Symbol’s value as variable is void: projects references Symbol’s value as variable is void: content/project/deep-learning/index.md.
#   Otherwise, set Symbol’s value as variable is void: projects.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '3'
abstract: ''
publication: '*submitted to SAMPTA2025*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2502.13777 #add arxiv identifier here
#- name: DIAL
#  url: http://hdl.handle.net/ #add dial handle or comment this out
---

**Abstract**: Representing and processing data in spherical domains presents unique challenges, primarily due to the curvature of the domain, which complicates the application of classical Euclidean techniques. Implicit neural representations (INRs) have emerged as a promising alternative for high-fidelity data representation; however, to effectively handle spherical domains, these methods must be adapted to the inherent geometry of the sphere to maintain both accuracy and stability. In this context, we propose Herglotz-NET (HNET), a novel INR architecture that employs a harmonic positional encoding based on complex Herglotz mappings. This encoding yields a well-posed representation on the sphere with interpretable and robust spectral properties. Moreover, we present a unified expressivity analysis showing that any spherical-based INR satisfying a mild condition exhibits a predictable spectral expansion that scales with network depth. Our results establish HNET as a scalable and flexible framework for accurate modeling of spherical data.

{{< figure src="/ox-hugo/2025-02-21-T11-42-33.png" caption="<span class=\"figure-number\">Figure 1: </span>Representation of the real part of one Herglotz atom for a Herglotz vector (1,1,1)/3 + i⁢(1,−1,0)/2, and different values of frequencies ω0. The atom is centered on the real part (1,1,1)/3 with oscillations locally oriented along the direction (1,−1,0)/2 pointed by the imaginary part and frequency proportional to the frequencies ω0." width="50%" >}}
