---
title: "Low-rank plus sparse trajectory decomposition for direct exoplanet imaging"
author: ["Laurent Jacques"]
tags: [1, 2023]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Simon Vary
- Hazan Daglayan
- Laurent Jacques
- P.-A. Absil
tags: # One keywords per item "-" with format - '"keyword"'
- '""'
#categories: []
date: '2023-06-04'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
abstract: ''
publication: '*2023 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.1109/ICASSP49357.2023.10096197'

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2301.07018 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/276195 #add dial handle or comment this out
---

**Abstract**: We propose a direct imaging method for the detection of exoplanets based on a combined low-rank plus structured sparse model. For this task, we develop a dictionary of possible effective circular trajectories a planet can take during the observation time, elements of which can be efficiently computed using rotation and convolution operation. We design a simple alternating iterative hard-thresholding algorithm that jointly promotes a low-rank background and a sparse exoplanet foreground, to solve the non-convex optimisation problem. The experimental comparison on the β-Pictoris exoplanet benchmark dataset shows that our method has the potential to outperform the widely used Annular PCA for specific planet light intensities in terms of the Receiver operating characteristic (ROC) curves.
