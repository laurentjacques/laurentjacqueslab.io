---
title: "A modified 4D ROOSTER method using the Chambolle-Pock algorithm"
author: ["Laurent Jacques"]
tags: [1, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Cyril Mory
- Laurent Jacques
tags:
- '"inverse problem"'
- '"computed tomography"'
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:34.931827Z'
publication_types:
- '1'
abstract: 
publication: '*Proc. of the Third International Conference on Image Formation in X-Ray
  Computed Tomography*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/157085
---

**Abstract**: The 4D RecOnstructiOn using Spatial and TEmpo- ral Regularization method is a recent 4D cone beam computed tomography algorithm. 4D ROOSTER has not been rigorously proved to converge. This paper aims to reformulate it using the Chambolle & Pock primal-dual optimization scheme. The convergence of this reformulated 4D ROOSTER is therefore guaranteed.
