---
title: "Multilevel Illumination Coding for Fourier Transform Interferometry in Fluorescence Spectroscopy"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Laurent Jacques
tags:
- '"Hyperspectral"'
- '"Fourier transform interferometry"'
- '"Fluorescence spectroscopy"'
- '"Compressive sensing"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:30+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:30.424556Z'
publication_types:
- '1'
abstract: 
publication: '*2018 25th IEEE International Conference on Image Processing (ICIP)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211219
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1803.03217
doi: https://dx.doi.org/10.1109/icip.2018.8451347
---

**Abstract**: Fourier Transform Interferometry (FTI) is an interferometric procedure for acquiring HyperSpectral (HS) data. Recently, it has been observed that the light source highlighting a (biologic) sample can be coded before the FTI acquisition in a procedure called Coded Illumination-FTI (CI-FTI). This turns HS data reconstruction into a Compressive Sensing (CS) problem regularized by the sparsity of the HS data.  CI-FTI combines the high spectral resolution of FTI with the advantages of reduced-light-exposure imaging in biology. In this paper, we leverage multilevel sampling scheme recently developed in CS theory to adapt the coding strategy of CI-FTI to the spectral sparsity structure of HS data in Fluorescence Spectroscopy (FS). This structure is actually extracted from the spectral signatures of actual fluorescent dyes used in FS. Accordingly, the optimum illumination coding as well as the theoretical recovery guarantee are derived. We conduct numerous numerical experiments on synthetic and experimental data that show the faithfulness of the proposed theory to experimental observations.
