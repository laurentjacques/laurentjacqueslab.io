---
title: "Low Rank and Group-Average Sparsity Driven Convex Optimization for Direct Exoplanets Imaging"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Benoît Pairet
- Laurent Jacques
- Carlos Gomez Gonzalez
- Olivier Absil
tags:
- '"AlterSense"'
- '"exo-planet detection"'
- '"low-rank apprixmation"'
- '"inverse problem"'
- '"sparse approximation"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:33.645592Z'
publication_types:
- '1'
abstract: 
publication: '"*Proceedings of the third international Traveling Workshop on Interactions between Sparse models and Technology iTWIST''16*"'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178310
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1609.04167
---

**Abstract**: Direct exoplanets imaging is a challenging task for two main reasons. First, the host star is several order of magnitude brighter than exoplanets. Second, the great distance between us and the star system makes the exoplanets-star angular dis- tance very small. Furthermore, imperfections on the optics along with atmospheric disturbances create speckles that look like plan- ets. Ten years ago, astronomers introduced Angular Differential Imaging (ADI) that uses the rotation of the Earth to add diversity to the data. Signal processing then allows to separate the starlight from the planet’s signal. After that, a detection signal to noise ra- tio (SNR) is computed. We present here a sparsity driven convex optimisation program along with a new SNR map that beat the state of the art methods for small angular separation.
