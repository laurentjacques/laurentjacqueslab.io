---
title: "A panorama on multiscale geometric representations, intertwining spatial, directional and frequency selectivity"
author: ["Laurent Jacques"]
tags: [2, 2011]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Laurent Duval
- Caroline Chaux
- Gabriel Peyré
tags:
- '"Review"'
- '"Multiscale"'
- '"Frames"'
- '"Edges"'
- '"Textures"'
- '"Image processing"'
- '"Haar wavelet"'
- '"Non-Euclidean wavelets"'
- '"Geometric representations"'
- '"Oriented decompositions"'
- '"Scale-space"'
- '"Wavelets"'
- '"Atoms"'
- '"Sparsity"'
- '"Redundancy"'
- '"Bases"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:24.062584Z'
publication_types:
- '2'
abstract: 
publication: '*Signal Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/87548
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1101.5320
doi: https://dx.doi.org/10.1016/j.sigpro.2011.04.025
---

**More information**:

-   This paper is part of the special issue "[Advances in Multirate Filter Bank Structures and Multiscale Representations](http://www.laurent-duval.eu/lcd-cfp-signal-processing-2011-multirate-multiscale-papers.html)."
-   Here is a [webpage](http://www.laurent-duval.eu/siva-panorama-multiscale-geometric-representations.html) related to this work (on [Laurent Duval](http://www.laurent-duval.eu)'s website).
