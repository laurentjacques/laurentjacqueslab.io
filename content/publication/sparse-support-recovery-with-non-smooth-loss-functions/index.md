---
title: "Sparse Support Recovery with Non-smooth Loss Functions"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Kévin Degraux
- Gabriel Peyré
- M. Jalal Fadili
- Laurent Jacques
tags:
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:34+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:33.931424Z'
publication_types:
- '1'
abstract: 
publication: '*Advances in Neural Information Processing Systems 29: 30th Annual Conference on Neural Information Processing Systems 2016*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/187195
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1611.01030
---

**Abstract**: In this paper, we study the support recovery guarantees of underdetermined sparse regression using the ℓ1-norm as a regularizer and a non-smooth loss function for data fidelity. More precisely, we focus in detail on the cases of ℓ1 and ℓ∞ losses, and contrast them with the usual ℓ2 loss.While these losses are routinely used to account for either sparse (ℓ1 loss) or uniform (ℓ∞ loss) noise models, a theoretical analysis of their performance is still lacking. In this article, we extend the existing theory from the smooth ℓ2 case to these non-smooth cases. We derive a sharp condition which ensures that the support of the vector to recover is stable to small additive noise in the observations, as long as the loss constraint size is tuned proportionally to the noise level. A distinctive feature of our theory is that it also explains what happens when the support is unstable. While the support is not stable anymore, we identify an "extended support" and show that this extended support is stable to small additive noise. To exemplify the usefulness of our theory, we give a detailed numerical analysis of the support stability/instability of compressed sensing recovery with these different losses. This highlights different parameter regimes, ranging from total support stability to progressively increasing support instability.
