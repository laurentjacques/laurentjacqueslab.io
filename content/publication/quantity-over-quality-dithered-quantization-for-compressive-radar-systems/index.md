---
title: "Quantity over Quality: Dithered Quantization for Compressive Radar Systems"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Thomas Feuillen
- Chunlei Xu
- Jérôme Louveaux
- Luc Vandendorpe
- Laurent Jacques
tags:
- '"Radar"'
- '"FMCW"'
- '"Ranging"'
- '"Compressive Sensing"'
- '"Quantization"'
- '"Dither"'
- '"Projected Back Projection"'
- '"Iterative Hard Thresholding"'
- '"1-bit"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:28.769189Z'
publication_types:
- '1'
abstract: 
publication: '*IEEE Radarconf*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/213366
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1811.11575
---

**Abstract**: In this paper, we investigate a trade-off between the number of radar observations (or measurements) and their resolution in the context of radar range estimation.  To this end, we introduce a novel estimation scheme that can deal with strongly quantized received signals, going as low as 1-bit per signal sample. We leverage for this a dithered quantized compressive sensing framework that can be applied to classic radar processing and hardware. This allows us to remove ambiguous scenarios prohibiting correct range estimation from (undithered) quantized base-band radar signal. Two range estimation algorithms are studied: Projected Back Projection (PBP) and Quantized Iterative Hard Thresholding (QIHT). The effectiveness of the reconstruction methods combined with the dithering strategy is shown through Monte Carlo simulations.  Furthermore we show that: (i), in dithered quantization, the accuracy of target range estimation improves when the bit-rate (i.e., the total number of measured bits) increases, whereas the accuracy of other undithered schemes saturate in this case; and (ii), for ﬁxed, low bit-rate scenarios, severely quantized dithered schemes exhibit better performances than their full resolution counterparts. These observations are conﬁrmed using real measurements obtained in a controlled environment, demonstrating the feasibility of the method in real ranging applications.
