---
title: "A short note on compressed sensing with partially known signal support"
author: ["Laurent Jacques"]
tags: [2, 2010]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
tags:
- '"Sparse signal recovery"'
- '"Compressed sensing"'
- '"Convex optimization"'
- '"Instance optimality"'
categories: []
date: '2010-01-01'
lastmod: 2021-08-06T23:54:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:24.655630Z'
publication_types:
- '2'
abstract: 
publication: '*Signal Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/33575
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/0908.0660
doi: https://dx.doi.org/10.1016/j.sigpro.2010.05.025
---

**Abstract**: This short note studies a variation of the compressed sensing paradigm introduced recently by Vaswani et al., i.e., the recovery of sparse signals from a certain number of linear measurements when the signal support is partially known. In this framework, we propose a reconstruction method based on a convex minimization program Coined innovative Basis Pursuit De Noise (or iBPDN). Under the common l(2)-fidelity constraint made on the available measurements, this optimization promotes the (l(1)) sparsity of the candidate signal over the complement of this known part. In particular, this paper extends the results of Vaswani et al. to the cases of compressible signals and noisy measurements by showing that iBPDN is l(2)-l(1) instance optimal. The corresponding proof relies on a small adaption of the results of Candes in 2008 for characterizing the stability of the Basis Pursuit DeNoise (BPDN) program. We also emphasize an interesting link between our method and the recent work of Davenport et al. on the delta-stable embeddings and the cancel-then-recover strategy applied to our problem. For both approaches, reconstructions are indeed stabilized when the sensing matrix respects the Restricted Isometry Property for the same sparsity order. (C) 2010 Elsevier B.V. All rights reserved.
