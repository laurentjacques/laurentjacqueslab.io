---
title: "Signal Processing with Optical Quadratic Random Sketches"
author: ["Laurent Jacques"]
tags: [1, 2023]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Rémi Delogne
- Vincent Schellekens
- Laurent Daudet
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"optical processing unit"'
- '"rank-one projection"'
- '"random sketches"'
- '"signal processing"'
#categories: []
date: '2023-06-04'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
abstract: ''
publication: '*2023 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.1109/ICASSP49357.2023.10096869'

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2212.00660 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/276198 #add dial handle or comment this out
---

**Abstract**:  Random data sketching (or projection) is now a classical technique enabling, for instance, approximate numerical linear algebra and machine learning algorithms with reduced computational complexity and memory. In this context, the possibility of performing data processing (such as pattern detection or classification) directly in the sketched domain without accessing the original data was previously achieved for linear random sketching methods and compressive sensing. In this work, we show how to estimate simple signal processing tasks (such as deducing local variations in a image) directly using random quadratic projections achieved by an optical processing unit. The same approach allows for naive data classification methods directly operated in the sketched domain. We report several experiments confirming the power of our approach.
