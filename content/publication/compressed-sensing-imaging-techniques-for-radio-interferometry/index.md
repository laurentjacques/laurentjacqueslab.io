---
title: "Compressed sensing imaging techniques for radio interferometry"
author: ["Laurent Jacques"]
tags: [2, 2009]
draft: false
subtitle: ''
summary: ''
authors:
- Yves Wiaux
- Laurent Jacques
- G. Puy
- A. M. M. Scaife
- P. Vandergheynst
tags:
- '"techniques: image processing"'
- '"techniques: interferometric"'
- '"cosmic microwave background"'
categories: []
date: '2009-01-01'
lastmod: 2021-08-06T23:54:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:24.792956Z'
publication_types:
- '2'
abstract: 
publication: '*Royal Astronomical Society. Monthly Notices*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/35591
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/0812.4933
doi: https://dx.doi.org/10.1111/j.1365-2966.2009.14665.x
---

**Abstract**: Radio interferometry probes astrophysical signals through incomplete and noisy Fourier measurements. The theory of compressed sensing demonstrates that such measurements may actually suffice for accurate reconstruction of sparse or compressible signals. We propose new generic imaging techniques based on convex optimization for global minimization problems defined in this context. The versatility of the framework notably allows introduction of specific prior information on the signals, which offers the possibility of significant improvements of reconstruction relative to the standard local matching pursuit algorithm CLEAN used in radio astronomy.  We illustrate the potential of the approach by studying reconstruction performances on simulations of two different kinds of signals observed with very generic interferometric configurations. The first kind is an intensity field of compact astrophysical objects.  The second kind is the imprint of cosmic strings in the temperature field of the cosmic microwave background radiation, of particular interest for cosmology.
