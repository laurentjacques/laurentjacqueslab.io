---
title: "Equivariance-based self-supervised learning for audio signal recovery from clipped measurements"
author: ["Laurent Jacques"]
tags: [3]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Victor Sechaud
- Laurent Jacques
- Patrice Abry
- Julián Tachella
tags: # One keywords per item "-" with format - '"keyword"'
- '"signal declipping"'
- '"unsupervised learning"'
- '"self-supervised learning"'
- '"equivariance"'
#categories: []
date: '2024-10-28'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named Symbol’s value as variable is void: featured.jpg/png to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. Symbol’s value as variable is void: projects references Symbol’s value as variable is void: content/project/deep-learning/index.md.
#   Otherwise, set Symbol’s value as variable is void: projects.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
abstract: ''
publication: '*32nd European Signal Processing Conference (EUSIPCO)*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2409.15283 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/292968 #add dial handle or comment this out
---

**Abstract**: In numerous inverse problems, state-of-the-art solving strategies involve training neural networks from ground truth and associated measurement datasets that, however, may be expensive or impossible to collect. Recently, self-supervised learning techniques have emerged, with the major advantage of no longer requiring ground truth data. Most theoretical and experimental results on self-supervised learning focus on linear inverse problems. The present work aims to study self-supervised learning for the non-linear inverse problem of recovering audio signals from clipped measurements. An equivariance-based selfsupervised loss is proposed and studied. Performance is assessed on simulated clipped measurements with controlled and varied levels of clipping, and further reported on standard real music signals. We show that the performance of the proposed equivariance-based self-supervised declipping strategy compares favorably to fully supervised learning while only requiring clipped measurements alone for training.
