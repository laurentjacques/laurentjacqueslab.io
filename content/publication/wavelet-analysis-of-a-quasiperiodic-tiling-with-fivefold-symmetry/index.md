---
title: "Wavelet analysis of a quasiperiodic tiling with fivefold symmetry"
author: ["Laurent Jacques"]
tags: [2, 1999]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
- R Twarock
tags:
- '"continuous wavelet transform"'
- '"quasiperiodic tiling"'
- '"Coxeter group"'
- '"scale-angle measure"'
categories: []
date: '1999-01-01'
lastmod: 2021-08-06T23:54:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.963363Z'
publication_types:
- '2'
abstract: 
publication: '*Physics Letters. Section A: General, Atomic and Solid State Physics*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/44082
doi: https://dx.doi.org/10.1016/S0375-9601(99)00634-9
---

**Abstract**: We determine all (statistical) rotation-dilation symmetries of a planar quasiperiodic tiling with fivefold symmetry, with a two-dimensional continuous wavelet transform, using a modified Cauchy wavelet and the scale-angle measure. The tiling is constructed via an affine extension of the Coxeter group H-2 and its statistical symmetries were unknown. (C) 1999 Published by Elsevier Science B.V. All rights reserved.
