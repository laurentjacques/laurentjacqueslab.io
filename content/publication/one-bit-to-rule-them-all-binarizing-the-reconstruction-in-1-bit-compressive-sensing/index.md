---
title: "One Bit to Rule Them All: Binarizing the Reconstruction in 1-bit Compressive Sensing"
author: ["Laurent Jacques"]
tags: [1, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Thomas Feuillen
- Mike Davies
- Luc Vandendorpe
- Laurent Jacques
tags:
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:27+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:27.064786Z'
publication_types:
- '1'
abstract: 
publication: "*in Proceedings of iTWIST'20, Paper-ID: 26, Nantes, France, December,
   2-4, 2020*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242108
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2008.07264
---

**Abstract**: This work focuses on the reconstruction of sparse signals from their 1-bit measurements. The context is the one of 1-bit compressive sensing where the measurements amount to quantizing (dithered) random projections. Our main contribution shows that, in addition to the measurement process, we can additionally reconstruct the signal with a binarization of the sensing matrix. This binary representation of both the measurements and sensing matrix can dramatically simplify the hardware architecture on embedded systems, enabling cheaper and more power efficient alternatives.  Within this framework, given a sensing matrix respecting the restricted isometry property (RIP), we prove that for any sparse signal the quantized projected back-projection (QPBP) algorithm achieves a reconstruction error decaying like O(m-1/2)when the number of measurements m increases. Simulations highlight the practicality of the developed scheme for different sensing scenarios, including random partial Fourier sensing.
