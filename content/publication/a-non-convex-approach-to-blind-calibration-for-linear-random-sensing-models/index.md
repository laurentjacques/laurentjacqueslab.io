---
title: "A Non-Convex Approach to Blind Calibration for Linear Random Sensing Models"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Valerio Cambareri
- Laurent Jacques
tags:
- '"AlterSense"'
- '"blind calibration"'
- '"random sensing"'
- '"non-convex optimization"'
- '"Wirtinger flow"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.250001Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of the third international Traveling Workshop on Interactions
   between Sparse models and Technology iTWIST'16*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178307
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1609.04167
---

**Abstract**: Performing blind calibration is highly important in modern sensing strategies, particularly when calibration aided by multiple, accurately designed training signals is infeasible or resource-consuming. We here address it as a naturally-formulated non-convex problem for a linear model with sub-Gaussian ran- dom sensing vectors in which both the sensor gains and the sig- nal are unknown. A sample complexity bound is derived to as- sess that solving the problem by projected gradient descent with a suitable initialisation provably converges to the global optimum. These findings are supported by numerical evidence on the phase transition of blind calibration and by an imaging example.
