---
title: "Interferometric single-pixel imaging with a multicore fiber"
author: ["Laurent Jacques"]
tags: [1]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Olivier Leblanc
- Matthias Hofer
- Siddharth Sivankutty
- Hervé Rigneault
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"rank-one projections"'
- '"interferometry"'
- '"lensless"'
- '"computational imaging"'
#categories: []
date: '2023-06-12'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false
# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
#abstract: ''
publication: '*Proceeding of the International Symposium on Computational Sensing 2023 (ISCS23)*'
#url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
#doi: ''
# custom links
links:
#- name: Slides
#  url: poster_LRMA22.pdf
-  name: arXiv
   icon_pack: ai
   url: https://arxiv.org/abs/2307.08562 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/276904 #add dial handle or comment this out
---

**Abstract**: Lensless illumination single-pixel imaging with a multicore fiber (MCF) is a computational imaging technique that enables potential endoscopic observations of biological samples at cellular scale. In this work, we show that this technique is tantamount to collecting multiple symmetric rank-one projections (SROP) of a Hermitian _interferometric_ matrix -- a matrix encoding the spectral content of the sample image. In this model, each SROP is induced by the complex _sketching_ vector shaping the incident light wavefront with a spatial light modulator (SLM), while the projected interferometric matrix collects up to \\(O(Q^2)\\) image frequencies for a \\(Q\\)-core MCF. While this scheme subsumes previous sensing modalities, such as raster scanning (RS) imaging with beamformed illumination, we demonstrate that collecting the measurements of \\(M\\) random SLM configurations -- and thus acquiring \\(M\\) SROPs -- allows us to estimate an image of interest if \\(M\\) and \\(Q\\) scale linearly (up to log factors) with the image sparsity level, hence requiring much fewer observations than RS imaging or a complete Nyquist sampling of the \\(Q \times Q\\) interferometric matrix. This demonstration is achieved both theoretically, with a specific restricted isometry analysis of the sensing scheme, and with extensive Monte Carlo experiments. Experimental results made on an actual MCF system finally demonstrate the effectiveness of this imaging procedure on a benchmark image.
