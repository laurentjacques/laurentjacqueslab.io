---
title: "How does digital cinema compress images?"
author: ["Laurent Jacques"]
tags: [6, 2009]
draft: false
subtitle: ''
summary: ''
authors:
- Antonin Descampe
- Christophe De Vleeschouwer
- Laurent Jacques
- Ferran Marques
tags:
- '"wavelet"'
- '"jpeg2000"'
- '"digital cinema"'
- '"rate distortion"'
categories: []
date: '2009-01-01'
lastmod: 2021-08-06T23:54:43+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:43.146794Z'
publication_types:
- '6'
abstract: 
publication: '*Applied Signal Processing - A MATLAB-based approach*'
# custom links
links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/69236 #add dial handle or comment this out
doi: https://dx.doi.org/10.1007/978-0-387-74535-0_10 # add DOI id here
---

**Remark**: Chapter of the book "[Applied Signal Processing - A MATLAB-based approach](http://www.springer.com/engineering/signals/book/978-0-387-74534-3)", edited by T. Dutoit and F. Marques, May 2009, Springer, MA. ISBN:978-0-387-74534-3.
