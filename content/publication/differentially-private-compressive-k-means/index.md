---
title: "Differentially Private Compressive K-means"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Antoine Chatalic
- Florimond Houssiau
- Yves-Alexandre de Montjoye
- Laurent Jacques
- Rémi Gribonval
tags:
- '"Differential   Privacy"'
- '"Sketching"'
- '"SketchedLearning"'
- '"Compressive Learning"'
- '"Clustering"'
- '"Differential   Privacy"'
- '"Sketching"'
- '"Sketched Learning"'
- '"Compressive Learning"'
- '"Clustering"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:27.864988Z'
publication_types:
- '1'
abstract: 
publication: '*ICASSP 2019 - 2019 IEEE International Conference on Acoustics, Speech
  and Signal Processing (ICASSP)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/216655
doi: https://dx.doi.org/10.1109/icassp.2019.8682829
---

**Abstract**: This work addresses the problem of learning from large collections of data with privacy guarantees. The sketched learning framework proposes to deal with the large scale of datasets by compressing them into a single vector of generalized random moments, from which the learning task is then performed. We modify the standard sketching mechanism to provide differential privacy, using addition of Laplace noise combined with a subsampling mechanism (each moment is computed from a subset of the dataset). The data can be divided between several sensors, each applying the privacy-preserving mechanism locally, yielding a differentially-private sketch of the whole dataset when reunited. We apply this framework to the k-means clustering problem, for which a measure of utility of the mechanism in terms of a signal-to-noise ratio is provided, and discuss the obtained privacy-utility tradeoff.
