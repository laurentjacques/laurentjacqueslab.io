---
title: "Compressed Sensing: When sparsity meets sampling"
author: ["Laurent Jacques"]
tags: [6, 2011]
draft: false
authors:
- Laurent Jacques
- Pierre Vandergheynst
tags:
- '"Compressed Sensing"'
- '"sparse signal representation"'
- '"sampling theory"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:43+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:42.969230Z'
publication_types:
- '6'
abstract: 
publication: '*Optical and Digital Image Processing Fundamentals and Applications*'
# custom links
links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/87547 #add dial handle or comment this out
doi: https://dx.doi.org/10.1002/9783527635245.ch23 # add DOI id here
---

**Remark**: Chapter of the book "[Optical and Digital Image Processing - Fundamentals and Applications](http://eu.wiley.com/WileyCDA/WileyTitle/productCd-3527409564.html)", Edited by G. Cristòbal; P. Schelkens; H. Thienpont. Wiley-VCH, April 2011. ISBN:978-3-527-40956-3. ([website of the book](http://odipbook.weebly.com/))
