---
title: "Iterative Low-rank and rotating sparsity promotion for circumstellar disks imaging"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Benoît Pairet
- Laurent Jacques
- Faustine Cantalloube
tags:
- '"exoplanet detection"'
- '"circumstellar disks imagin"'
- '"low-rank"'
- '"low-rank plus sparse model"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:28.188851Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of SPARS'19*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/223576
---

**Abstract**: Recent astronomical observations open the possibility to directly image circumstellar disks, a key feature for our understanding of extra-solar systems.  However, the faint intensity of these celestial signals compared to the brightness of their hosting star make their accurate characterization a challenging processing task. While there exist several post processing techniques adapted to the extraction of (point shaped) exoplanets, this work proposes a novel iterative method for extracting the elongated structure of circumstellar signals from direct imaging datasets. This method leverages low-complexity models of both the corrupting stellar light and the disks. Our approach is assessed through numerical experiments.
