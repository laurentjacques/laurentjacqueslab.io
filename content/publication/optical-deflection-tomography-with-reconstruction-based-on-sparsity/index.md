---
title: "Optical deflection tomography with reconstruction based on sparsity"
author: ["Laurent Jacques"]
tags: [1, 2012]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Philippe Antoine
- Emmanuel Foumouo 
- Jean-Luc Dewandel
- Didier Beghuin
- Adriana Gonzalez
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '""'
#categories: []
date: ''  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
abstract: ''
publication: '*Proc. of OPTIMESS2012 (4-5 April 2012), Antwerp, Belgium.*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134471 #add dial handle or comment this out
---
