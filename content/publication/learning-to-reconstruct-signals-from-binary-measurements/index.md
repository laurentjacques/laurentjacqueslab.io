---
title: "Learning to Reconstruct Signals From Binary Measurements"
author: ["Laurent Jacques"]
tags: [2, 2023]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Julián Tachella
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"self-supervised learning"'
- '"one-bit compressive sensing"'
- '"signal set learning"'
- '"error bounds"'
#categories: []
date: '2023-11-27'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false
# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
abstract: ''
publication: 'Transactions on Machine Learning Research' #publication: '*<journal-type>*'
url_pdf: https://openreview.net/forum?id=ioFIAQOBOS
url_code: 'https://github.com/tachella/ssbm'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.48550/arXiv.2303.08691'

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2303.08691 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/281268 #add dial handle or comment this out
---

**Abstract**: Recent advances in unsupervised learning have highlighted the possibility of learning to reconstruct signals from noisy and incomplete linear measurements alone. These methods play a key role in medical and scientific imaging and sensing, where ground truth data is often scarce or difficult to obtain. However, in practice, measurements are not only noisy and incomplete but also quantized. Here we explore the extreme case of learning from binary observations and provide necessary and sufficient conditions on the number of measurements required for identifying a set of signals from incomplete binary data. Our results are complementary to existing bounds on signal recovery from binary measurements. Furthermore, we introduce a novel self-supervised learning approach, which we name SSBM, that only requires binary data for training. We demonstrate in a series of experiments with real datasets that SSBM performs on par with supervised learning and outperforms sparse reconstruction methods with a fixed wavelet basis by a large margin.
