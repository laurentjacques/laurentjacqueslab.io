---
title: "1-bit Localization Scheme for Radar using Dithered Quantized Compressed Sensing"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Thomas Feuillen
- Chunlei Xu
- Luc Vandendorpe
- Laurent Jacques
tags:
- '"Radar"'
- '"2D-localization"'
- '"Quantization"'
- '"Quantized Compressive Sensing"'
- '"FMCW"'
- '"Antenna array."'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:29+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:29.366416Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of the 5th International Workshop on Compressed Sensing
  applied to Radar, Multimodal Sensing, and Imaging (CoSeRa)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/213363
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1806.05408
---

**Abstract**: We present a novel scheme allowing for 2D target localization using highly quantized 1-bit measurements from a Frequency Modulated Continuous Wave (FMCW) radar with two receiving antennas. Quantization of radar signals introduces localization artifacts, we remove this limitation by inserting a dithering on the unquantized observations. We then adapt the projected back projection algorithm to estimate both the range and angle of targets from the dithered quantized radar observations, with provably decaying reconstruction error when the number of observations increases.  Simulations are performed to highlight the accuracy of the dithered scheme in noiseless conditionswhencomparedtothenon-ditheredandfull32-bitresolutionunderseverebit-ratereduction.Finally,measurementsare performed using a radar sensor to demonstrate the effectiveness and performances of the proposed quantized dithered scheme in real conditions.
