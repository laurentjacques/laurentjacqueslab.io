---
title: "Traitement de l'image : de l'équation de la chaleur aux ondelettes"
author: ["Laurent Jacques"]
tags: [1, 2004]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
tags: []
categories: []
date: '2004-01-01'
lastmod: 2021-08-06T23:54:41+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:41.225370Z'
publication_types:
- '1'
abstract: 
publication: '*IREM-Bordeaux*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/109011
---
