---
title: "Sparsity-driven optical deflection tomography with non-linear light ray propagation"
author: ["Laurent Jacques"]
tags: [1, 2012]
draft: false
subtitle: ''
summary: ''
authors:
- Philippe Antoine
- Emmanuel Foumouo
- Jean-Luc Dewandel
- Didier Beghuin
- Adriana Gonzalez Gonzalez
- Laurent Jacques
tags:
- '"Schlieren"'
- '"tomography"'
- '"deflectometry"'
- '"inverse problem"'
categories: []
date: '2012-01-01'
lastmod: 2021-08-06T23:54:36+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:36.647870Z'
publication_types:
- '1'
abstract: 
publication: '*Optimess 2012*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134471
---

**Abstract**: Optical deflection tomography based on phase-shifting Schlieren is well adapted for three-dimensional imaging of objects with large refractive index variations.  We propose a reconstruction algorithm that takes into account light refraction and spatial sparsity of the sample. Its performance is illustrated with optical phantoms.
