---
title: "When compressive learning fails:  blame the decoder or the sketch?"
author: ["Laurent Jacques"]
tags: [1, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Laurent Jacques
tags:
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:27+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:27.230997Z'
publication_types:
- '1'
abstract: 
publication: "*in Proceedings of iTWIST'20, Paper-ID: 22, Nantes, France, December,
   2-4, 2020*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242097
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2009.08273
---

**Abstract**: In compressive learning, a mixture model (a set of centroids or a Gaussian mixture) is learned from a sketch vector, that serves as a highly compressed representation of the dataset. This requires solving a non-convex optimization problem, hence in practice approximate heuristics (such as CLOMPR) are used. In this work we explore, by numerical simulations, properties of this non-convex optimization landscape and those heuristics.
