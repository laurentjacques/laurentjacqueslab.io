---
title: "Refractive index map reconstruction in optical deflectometry using total-variation regularization"
author: ["Laurent Jacques"]
tags: [1, 2011]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Adriana Gonzalez Gonzalez
- Emmanuel Foumouo
- Philippe Antoine
tags:
- '"Sparse signals"'
- '"Optics"'
- '"Optimization"'
- '"Deflectometry"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:37+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:37.442547Z'
publication_types:
- '1'
abstract: 
publication: '*SPIE - the International Society for Optical Engineering. Proceedings*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/87531
doi: https://dx.doi.org/10.1117/12.892729
---
