---
title: "Compressive k-Means with Differential Privacy"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Antoine Chatalic
- Florimont Houssiau
- Yves-Alexandre de Montjoye
- Laurent Jacques
- Rémi Gribonval
tags:
- '"compressive learning"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:27+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:27.733943Z'
publication_types:
- '1'
abstract: 
publication: '"*Proceedings of SPARS''19*"'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/223572
---

**Abstract**: In the compressive learning framework, one harshly compresses a whole training dataset into a single vector of generalized random moments, the sketch, from which a learning task can subsequently be performed. We prove that this loss of information can be leveraged to design a differentially private mechanism, and study empirically the privacy-utility tradeoff for the k-means clustering problem.
