---
title: "Determination of vibration amplitudes from binary phase patterns obtained by phase-shifting time-averaged speckle shearing interferometry"
author: ["Laurent Jacques"]
tags: [2, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Murielle Kirkove
- Stéphanie Guérit
- Laurent Jacques
- Christophe Loffet
- Fabian Languy
- Jean-François Vandenrijt
- Marc Georges
tags:
- '"Speckle interferometry"'
- '"Vibration analysis"'
- '"Fringe analysis"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:20.567238Z'
publication_types:
- '2'
abstract: 
publication: '*Applied Optics*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/208806
doi: https://dx.doi.org/10.1364/ao.57.008065
---

**Abstract**: Speckle shearing interferometry (shearography) is a full-field strain measurement technique that can be used in vibration analysis. In our case, we apply a method that combines the time-averaging and phase-shifting techniques. It produces binary phase patterns, where the phase changes are related to the zeroes of a Bessel J0 function, typical of time-averaging. However, the contrast and resolution are better compared to traditional time-averaging. In a previous paper, we have shown that this is particularly useful in vibration testing performed under industrial conditions, because fringe patterns are noisier than in quiet laboratory environments. This paper goes a step further in proposing a processing method for estimating the vibration amplitude, for helping non-experts to identify vibration modes. Since shearography measures the spatial derivative of displacement, spatial integration is required.  Prior to that, different processes like denoising, binarization, automated nodal line detection, and amplitude assignment are applied. We analyze the performance of the method on synthetic and experimental data, in the function of noise level and fringes density. Results on data acquired in an industrial environment illustrate the good performances of the proposed method.
