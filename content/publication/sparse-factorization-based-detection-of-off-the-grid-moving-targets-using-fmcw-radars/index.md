---
title: "Sparse Factorization-based Detection of Off-the-Grid Moving targets using FMCW radars"
author: ["Laurent Jacques"]
tags: [1, 2021]
draft: false
subtitle: ''
summary: ''
authors:
- Gilles Monnoyer de Galland de Carnières
- Thomas Feuillen
- Laurent Jacques
- Luc Vandendorpe
tags:
- '"Radar"'
- '"sparsity"'
- '"off-the-grid"'
- '"continuous matching pursuit"'
- '"factorization"'
categories: []
date: '2021-01-01'
lastmod: 2021-08-06T23:54:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:26.282952Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of ICASSP 2021, Toronto, Ontario, Canada, 6-11 June 2021*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/243178
---

**Abstract**: In this paper, we investigate the application of continuous sparse signal reconstruction algorithms for the estimation of the ranges and speeds of multiple moving targets using an FMCW radar. Conventionally, to be reconstructed, continuous sparse signals are approximated by a discrete representation. This discretization of the signal's parameter domain leads to mismatches with the actual signal. While increasing the grid density mitigates these errors, it dramatically increases the algorithmic complexity of the reconstruction. To overcome this issue, we propose a fast greedy algorithm for off-the-grid detection of multiple moving targets. This algorithm extends existing continuous greedy algorithms to the framework of factorized sparse representations of the signals. This factorized representation is obtained from simplifications of the radar signal model which, up to a model mismatch, strongly reduces the dimensionality of the problem. Monte-Carlo simulations of a K-band radar system validate the ability of our method to produce more accurate estimations with less computation time than the on-the-grid methods and than methods based on non-factorized representations.
