---
title: "Quantized Compressive Sensing with RIP Matrices: The Benefit of Dithering"
author: ["Laurent Jacques"]
tags: [2, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Chunlei Xu
- Laurent Jacques
tags:
- '"compressive sensing"'
- '"quantization"'
- '"dithering"'
- '"sparse representation"'
- '"measure concentration"'
- '"random embedding"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:20.246815Z'
publication_types:
- '2'
abstract: 
publication: '*Information and Inference*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/216652
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1801.05870
doi: https://dx.doi.org/10.1093/imaiai/iaz021
---

**Abstract**: Quantized compressive sensing (QCS) deals with the problem of coding compressive measurements of low-complexity signals with quantized, finite precision representations, i.e., a mandatory process involved in any practical sensing model. While the resolution of this quantization clearly impacts the quality of signal reconstruction, there actually exist incompatible combinations of quantization functions and sensing matrices that proscribe arbitrarily low reconstruction error when the number of measurements increases. This work shows that a large class of random matrix constructions known to respect the restricted isometry property (RIP) is "compatible" with a simple scalar and uniform quantization if a uniform random vector, or a random dither, is added to the compressive signal measurements before quantization. In the context of estimating low-complexity signals (e.g., sparse or compressible signals, low-rank matrices) from their quantized observations, this compatibility is demonstrated by the existence of (at least) one signal reconstruction method, the projected back projection (PBP), whose reconstruction error decays when the number of measurements increases. Interestingly, given one RIP matrix and a single realization of the dither, a small reconstruction error can be proved to hold uniformly for all signals in the considered low-complexity set. We confirm these observations numerically in several scenarios involving sparse signals, low-rank matrices, and compressible signals, with various RIP matrix constructions such as sub-Gaussian random matrices and random partial discrete cosine transform (DCT) matrices.
