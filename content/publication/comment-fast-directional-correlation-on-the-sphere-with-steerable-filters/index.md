---
title: "Fast directional correlation on the sphere with steerable filters"
author: ["Laurent Jacques"]
tags: [2, 2006, "bibtex"]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Yves Wiaux
- Laurent Jacques
- Patricio Vielva Vielva
- Pierre Vandergheynst
tags: # One keywords per item "-" with format - '"keyword"'
- '"cosmic microwave background"'
- '"data analysis"'
- '"numerical"'
- '"spherical harmonics"'
#categories: []
date: '2006-01-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
publication: '*Astrophys. J. 652, 820*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: https://dx.doi.org/10.1086/507692

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/astro-ph/0508516 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078/30990 #add dial handle or comment this out
---

**Abstract**: A fast algorithm is developed for the directional correlation of scalar band-limited signals and band-limited steerable filters on the sphere. The asymptotic complexity associated to it through simple quadrature is of order \\(O(L^5)\\), where \\(2L\\) stands for the square-root of the number of sampling points on the sphere, also setting a band limit L for the signals and filters considered. The filter steerability allows to compute the directional correlation uniquely in terms of direct and inverse scalar spherical harmonics transforms, which drive the overall asymptotic complexity. The separation of variables technique for the scalar spherical harmonics transform produces an \\(O(L^3)\\) algorithm independently of the pixelization. On equi-angular pixelizations, a sampling theorem introduced by Driscoll and Healy implies the exactness of the algorithm. The equi-angular and HEALPix implementations are compared in terms of memory requirements, computation times, and numerical stability. The computation times for the scalar transform, and hence for the directional correlation, of maps of several megapixels on the sphere (\\(L\sim 10^3\\)) are reduced from years to tens of seconds in both implementations on a single standard computer. These generic results for the scale-space signal processing on the sphere are specifically developed in the perspective of the wavelet analysis of the cosmic microwave background (CMB) temperature (T) and polarization (E and B) maps of the WMAP and Planck experiments. As an illustration, we consider the computation of the wavelet coefficients of a simulated temperature map of several megapixels with the second Gaussian derivative wavelet.
