---
title: "Compressive Classification (Machine Learning without learning)"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Laurent Jacques
tags:
- '"Compressive Learning"'
- '"Sketched Learning"'
- '"Random Neural Network"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:30+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:29.891454Z'
publication_types:
- '1'
abstract: 
publication: "*ITWIST'18*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/209861
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1812.01410
---

**Abstract**: Compressive learning is a framework where (so far unsupervised) learning tasks use not the entire dataset but a compressed summary (sketch) of it. We propose a compressive learning classification method, and a novel sketch function for images.
