---
title: "A Variable Density Sampling Scheme for Compressive Fourier Transform Interferometry"
author: ["Laurent Jacques"]
tags: [2, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Valerio Cambareri
- Philippe Antoine
- Matthieu Roblin
- Laurent Jacques
tags:
- '"Hyperspectral"'
- '"Fourier transform interferometry"'
- '"compressive sensing"'
- '"variable density sampling"'
- '"coded illumination"'
- '"coded aperture"'
- '"uorescence spectroscopy"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.925675Z'
publication_types:
- '2'
abstract: 
publication: '*SIAM Journal on Imaging Sciences*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211204
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1801.10432
doi: https://dx.doi.org/10.1137/18M1214111
---

**Abstract**: Fourier Transform Interferometry (FTI) is an appealing Hyperspectral (HS) imaging modality for many applications demanding high spectral resolution, e.g., in fluorescence microscopy. However, the effective resolution of FTI is limited by the durability of biological elements when exposed to illuminating light. Overexposed elements are subject to photo-bleaching and become unable to fluoresce. In this context, the acquisition of biological HS volumes based on sampling the Optical Path Difference (OPD) axis at Nyquist rate leads to unpleasant trade-offs between spectral resolution, quality of the HS volume, and light exposure intensity. We propose two variants of the FTI imager, i.e., Coded Illumination-FTI (CI-FTI) and Structured Illumination FTI (SI-FTI), based on the theory of compressive sensing (CS). These schemes efficiently modulate light exposure temporally (in CI-FTI) or spatiotemporally (in SI-FTI). Leveraging a variable density sampling strategy recently introduced in CS, we provide optimal illumination strategies, so that the light exposure imposed on a biological specimen is minimized while the spectral resolution is preserved. Our analysis focuses on two criteria: (i) a trade-off between exposure intensity and the quality of the reconstructed HS volume for a given spectral resolution; (ii) maximizing HS volume quality for a fixed spectral resolution and constrained light exposure budget. Our contributions can be adapted to an FTI imager without hardware modifications. The reconstruction of HS volumes from CS-FTI measurements relies on an l1-norm minimization problem promoting a spatiospectral sparsity prior.  Numerically, we support the proposed methods on synthetic data and simulated CS measurements (from actual FTI measurements) under various scenarios. In particular, the biological HS volumes can be reconstructed with a three-to-ten-fold reduction in the light exposure.
