---
title: "The PANOPTIC Camera: A Plenoptic Sensor with Real-Time Omnidirectional Capability"
author: ["Laurent Jacques"]
tags: [2, 2012]
draft: false
subtitle: ''
summary: ''
authors:
- Hossein Afshari
- Laurent Jacques
- Luigi Bagnato
- Alexandre Schmid
- Pierre Vandergheynst
- Yusuf Leblebici
tags:
- '"Omnidirectional"'
- '"Polydioptric"'
- '"Plenoptic"'
- '"Image Rendering"'
- '"Computer Vision"'
- '"Image Processing Hardware"'
- '"3-D Vision System"'
- '"FPGA"'
categories: []
date: '2012-01-01'
lastmod: 2021-08-06T23:54:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:23.902774Z'
publication_types:
- '2'
abstract: 
publication: '*Journal of Signal Processing Systems*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/126013
doi: https://dx.doi.org/10.1007/s11265-012-0668-4
---
