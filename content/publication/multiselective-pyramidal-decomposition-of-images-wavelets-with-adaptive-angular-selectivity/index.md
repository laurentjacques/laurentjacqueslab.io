---
title: "Multiselective pyramidal decomposition of images: Wavelets with adaptive angular  selectivity"
author: ["Laurent Jacques"]
tags: [2, 2007]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Jean-Pierre Antoine
tags:
- '"image decomposition"'
- '"multiselectivity"'
- '"directional wavelet"'
- '"frames"'
- '"multiresolution analysis"'
categories: []
date: '2007-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.130689Z'
publication_types:
- '2'
abstract: 
publication: '*International Journal of Wavelets, Multiresolution and Information
  Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/36954
doi: https://dx.doi.org/10.1142/S0219691307002051
---

**Abstract**: Many techniques have been devised these last ten years to add an appropriate directionality concept in decompositions of images from the specific transformations of a small set of atomic functions. Let us mention, for instance, works on directional wavelets, steerable filters, dual-tree wavelet transform, curvelets, wave atoms, ridgelet packets, etc. In general, features that are best represented are straight lines or smooth curves as those de. ning contours of objects ( e. g. in curvelets processing) or oriented textures ( e. g. wave atoms, ridgelet packets). However, real images present also a set of details less oriented and more isotropic, like corners, spots, texture components, etc. This paper develops an adaptive representation for all these image elements, ranging from highly directional ones to fully isotropic ones. This new tool can indeed be tuned relatively to these image features by decomposing them into a Littlewood-Paley frame of directional wavelets with variable angular selectivity. Within such a decomposition, 2D wavelets inherit some particularities of the biorthogonal circular multiresolution framework in their angular behavior.  Our method can therefore be seen as an angular multiselectivity analysis of images.  Two applications of the proposed method are given at the end of the paper, namely, in the fields of image denoising and N-term nonlinear approximation.
