---
title: "Structured Illumination and Variable Density Sampling for Compressive Fourier Transform Interferometry"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Laurent Jacques
tags:
- '"hyperspectral"'
- '"compressive sensing"'
- '"Fourier tranasform interferometry"'
- '"structured illumination"'
- '"coded illumination"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:29+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:29.235950Z'
publication_types:
- '1'
abstract: 
publication: '*BASP*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/214599
---

**Abstract**: Fourier Transform Interferometry (FTI) is an appealing Hyperspectral (HS) imaging modality for many applications demanding high spectral resolution, e.g., in fluorescence microscopy. However, the effective resolution of FTI is limited by the durability (or photobleaching) of biological elements when exposed to illuminating light. We propose two variants of the FTI imager, i.e., Coded Illumination-FTI (CI-FTI) and Structured Illumination FTI (SI-FTI), based on the theory of compressive sensing (CS). These schemes efficiently modulate light exposure temporally (in CI-FTI) or spatiotemporally (in SI-FTI). Leveraging a variable density sampling strategy recently introduced in CS, we provide optimal illumination strategies, so that the light exposure imposed on a biological specimen is minimized while the spectral resolution is preserved. In particular, under certain conditions, the biological HS volumes can be reconstructed with a three-to-ten-fold reduction in the light exposure.
