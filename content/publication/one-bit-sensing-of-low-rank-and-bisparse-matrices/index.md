---
title: "One-Bit Sensing of Low-Rank and Bisparse Matrices"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Simon Foucart
- Laurent Jacques
tags:
- '"one-bit compressive sensing"'
- '"low-rank"'
- '"bisparse models"'
- '"joint structure models"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:28.444112Z'
publication_types:
- '1'
abstract: 
publication: '*Proceeding of Sampta 2019*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/223577
---

**Abstract**: This note studies the worst-case recovery error of low-rank and bisparse matrices as a function of the number of one-bit measurements used to acquire them.  First, by way of the concept of consistency width, precise estimates are given on how fast the recovery error can in theory decay. Next, an idealized recovery method is proved to reach the fourth-root of the optimal decay rate for Gaussian sensing schemes. This idealized method being impractical, an implementable recovery algorithm is finally proposed in the context of factorized Gaussian sensing schemes. It is shown to provide a recovery error decaying as the sixth-root of the optimal rate.
