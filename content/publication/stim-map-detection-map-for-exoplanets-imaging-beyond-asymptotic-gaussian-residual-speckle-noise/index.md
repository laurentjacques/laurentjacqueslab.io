---
title: "STIM map: detection map for exoplanets imaging beyond asymptotic Gaussian residual speckle noise"
author: ["Laurent Jacques"]
tags: [2, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Benoît Pairet
- Faustine Cantalloube
- Carlos A Gomez Gonzalez
- Olivier Absil
- Laurent Jacques
tags:
- '"methods: statistical"'
- '"techniques: high angular resolution"'
- '"techniques: image processing"'
- '"planets and satellites: detection"'
- '"statistical"'
- '"high angular resolution"'
- '"image processing"'
- '"exoplanet detection"'
- '"statistical analysis"'
- '"image processing"'
- '"exoplanet detection"'
- '"astronomy"'
- '"measure concentration"'
- '"non-Gaussian noise"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:20.396699Z'
publication_types:
- '2'
abstract: 
publication: '*Monthly Notices of the Royal Astronomical Society*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/216654
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1810.06895
doi: https://dx.doi.org/10.1093/mnras/stz1350
---

**Abstract**: Direct imaging of exoplanets is a challenging task as it requires to reach a high contrast at very close separation to the star. Today, the main limitation in the high-contrast images is the quasi-static speckles that are created by residual instrumental aberrations. They have the same angular size as planetary companions and are often brighter, hence hindering our capability to detect exoplanets. Dedicated observation strategies and signal processing techniques are necessary to disentangle these speckles from planetary signals. The output of these methods is a detection map in which the value of each pixel is related to a probability of presence of a planetary signal. The detection map found in the literature relies on the assumption that the residual noise is Gaussian. However, this is known to lead to higher false positive rates, especially close to the star. In this paper, we re-visit the notion of detection map by analyzing the speckle noise distribution, namely the Modified Rician distribution. We use non-asymptotic analysis of the sum of random variables to show that the tail of the distribution of the residual noise decays as an exponential distribution, hence explaining the high false detection rate obtained with the Gaussian assumption. From this analysis, we introduce a novel time domain detection map and we demonstrate its capabilities and the relevance of our approach through experiments on real data. We also provide an empirical rule to determine detection threshold providing a good trade off between true positive and false positive rates for exoplanet detection.
