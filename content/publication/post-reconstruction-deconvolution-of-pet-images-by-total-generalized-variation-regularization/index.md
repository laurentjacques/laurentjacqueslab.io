---
title: "Post-reconstruction deconvolution of PET images by total generalized variation regularization"
author: ["Laurent Jacques"]
tags: [1, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- Stéphanie Guérit
- Laurent Jacques
- Benoît Macq
- John Aldo Lee
tags:
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:34+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:34.804935Z'
publication_types:
- '1'
abstract: 
publication: '*Proc EUSIPCO*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/181059
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1506.04935
doi: https://dx.doi.org/10.1109/EUSIPCO.2015.7362459
---

**Abstract**: Improving the quality of positron emission tomography (PET) images, affected
  by low resolution and high level of noise, is a challenging task in nuclear medicine
  and radiotherapy. This work proposes a restoration method, achieved after tomographic
  reconstruction of the images and targeting clinical situations where raw data are
  often not accessible. Based on inverse problem methods, our contribution introduces
  the recently developed total generalized variation (TGV) norm to regularize PET
  image deconvolution. Moreover, we stabilize this procedure with additional image
  constraints such as positivity and photometry invariance. A criterion for updating
  and adjusting automatically the regularization parameter in case of Poisson noise
  is also presented. Experiments are conducted on both synthetic data and real patient
  images.
