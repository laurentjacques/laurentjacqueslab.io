---
title: "ADMM-inspired image reconstruction for terahertz off-axis digital holography"
author: ["Laurent Jacques"]
tags: [2, 2023]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Murielle Kirkove 
- Yuchen Zhao
- Olivier Leblanc 
- Laurent Jacques
- Marc Georges
tags: # One keywords per item "-" with format - '"keyword"'
- '"digital holography"'
#categories: []
date: '2023-12-08'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
abstract: ''
publication: '*Journal of the Optical Society of America A*' #publication: '*<journal-type>*'
url_pdf: https://dial.uclouvain.be/downloader/downloader.php?pid=boreal%3A281368&datastream=PDF_01
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.1364/josaa.504126'

# custom links
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/281368 #add dial handle or comment this out
---

**Abstract**: Image reconstruction in off-axis terahertz digital holography is complicated due to the harsh recording conditions and the non-convexity form of the problem. In this paper, we propose an inverse problem-based reconstruction technique that jointly reconstructs the object field and the amplitude of the reference field. Regularization in the wavelet domain promotes a sparse object solution. A single objective function combining the data-fidelity and regularization terms is optimized with a dedicated algorithm based on an alternating direction method of multipliers framework. Each iteration alternates between two consecutive optimizations using projections operating on each solution and one soft thresholding operator applying to the object solution. The method is preceded by a windowing process to alleviate artifacts due to the mismatch between camera frame truncation and periodic boundary conditions assumed to implement convolution operators. Experiments demonstrate the effectiveness of the proposed method, in particular, improvements of reconstruction quality, compared to two other methods.
