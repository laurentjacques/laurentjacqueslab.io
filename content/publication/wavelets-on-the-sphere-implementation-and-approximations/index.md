---
title: "Wavelets on the sphere: implementation and approximations"
author: ["Laurent Jacques"]
tags: [2, 2002]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurence Demanet
- Laurent Jacques
- P. Vandergheynst
tags:
- '"continuous wavelet transform"'
- '"2-sphere"'
- '"directional spherical wavelet"'
- '"approximate identity"'
categories: []
date: '2002-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.834002Z'
publication_types:
- '2'
abstract: 
publication: '*Applied and Computational Harmonic Analysis*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/41332
doi: https://dx.doi.org/10.1016/S1063-5203(02)00507-9
---

**Abstract**: We continue the analysis of the continuous wavelet transform on the 2-sphere, introduced in a previous paper. After a brief review of the transform, we define and discuss the notion of directional spherical wavelet, i.e., wavelets on the sphere that are sensitive to directions. Then we present a calculation method for data given on a regular spherical grid g. This technique, which uses the FFT, is based on the invariance of g under discrete rotations around the z axis preserving the phi sampling. Next, a numerical criterion is given for controlling the scale interval where the spherical wavelet transform makes sense, and examples are given, both academic and realistic. In a second part, we establish conditions under which the reconstruction formula holds in strong L-p sense, for 1 less than or equal to p < infinity. This opens the door to techniques for approximating functions on the sphere, by use of an approximate identity, obtained by a suitable dilation of the mother wavelet. (C) 2002 Elsevier Science (USA). All rights reserved.
