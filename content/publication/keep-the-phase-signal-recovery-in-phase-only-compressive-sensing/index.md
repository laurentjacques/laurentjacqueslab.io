---
title: "Keep the phase! Signal recovery in phase-only compressive sensing"
author: ["Laurent Jacques"]
tags: [1, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Thomas Feuillen
tags:
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).2078.1/242102

#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:26.734220Z'
publication_types:
- '1'
abstract: 
publication: "*in Proceedings of iTWIST'20, Paper-ID: 25, Nantes, France, December,
   2-4, 2020*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242102
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2011.06499
---

**Abstract**: We demonstrate that a sparse signal can be estimated from the phase of complex random measurements, in a "phase-only compressive sensing" (PO-CS) scenario. With high probability and up to a global unknown amplitude, we can perfectly recover such a signal if the sensing matrix is a complex Gaussian random matrix and the number of measurements is large compared to the signal sparsity. Our approach consists in recasting the (non-linear) PO-CS scheme as a linear compressive sensing model.  We built it from a signal normalization constraint and a phase-consistency constraint.  Practically, we achieve stable and robust signal direction estimation from the basis pursuit denoising program. Numerically, robust signal direction estimation is reached at about twice the number of measurements needed for signal recovery in compressive sensing.
