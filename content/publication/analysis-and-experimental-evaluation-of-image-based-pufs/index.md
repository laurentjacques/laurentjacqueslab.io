---
title: "Analysis and experimental evaluation of Image-based PUFs"
author: ["Laurent Jacques"]
tags: [2, 2012]
draft: false
subtitle: ''
summary: ''
authors:
- Saloomeh Shariati
- François-Xavier Standaert
- Laurent Jacques
- Benoît Macq
tags:
categories: []
date: '2012-01-01'
lastmod: 2021-08-06T23:54:23+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:23.773568Z'
publication_types:
- '2'
abstract: 
publication: '*Journal of Cryptographic Engineering*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078/123470
doi: https://dx.doi.org/10.1007/s13389-012-0041-3
---

**Abstract**: Physically Unclonable Functions (PUFs) are becoming popular tools for various applications such as anti-counterfeiting schemes. The security of a PUF-based system relies on the properties of its underlying PUF. Usually, evaluating PUF properties is not simple as it involves assessing a physical phenomenon. A recent work [1] proposed a generic security framework of physical functions allowing a sound analysis of security properties of PUFs. In this paper, we specialize this generic framework to model a system based on a particular category of PUFs called Image-based PUFs.  These PUFs are based on random visual features of the physical objects. The model enables a systematic design of the system ingredients and allows for concrete evaluation of its security properties, namely robustness and physical unclonability which are required by anti-counterfeiting systems. As a practical example, the components of the model are instantiated by Laser-Written PUF, White Light Interferometry evaluation, two binary image hashing procedures namely, Random Binary Hashing and Gabor Binary Hashing respectively, and code-offset fuzzy extraction. We experimentally evaluate security properties of this example for both image hashing methods. Our results show that, for this particular example, adaptive image hashing outperforms the non-adaptive one. The experiments also confirm the usefulness of the formalizations provided by citepufmodel to a practical example. In particular, the formalizations provide an asset for evaluating the concrete trade-off between robustness and physical unclonability.  To the best of our knowledge, this experimental evaluation of explicit trade-off between robustness and physical unclonability has been performed for the first time in this paper.
