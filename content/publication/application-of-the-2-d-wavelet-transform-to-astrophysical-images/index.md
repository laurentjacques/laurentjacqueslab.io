---
title: "Application of the 2-D wavelet transform to astrophysical images"
author: ["Laurent Jacques"]
tags: [2, 2002]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Demanet
- Jean-François Hochedez
- Laurent Jacques
- Regis Terrier
- Erwin Verwichte
tags:
categories: []
date: '2002-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.706939Z'
publication_types:
- '2'
abstract: 
publication: '*Physicalia Magazine*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/87551
---

**Abstract**: The 2-D continuous wavelet transform has been applied to a number of problems in astrophysics. We survey quickly some of these, then focus on two new applications.  The first one is the automatic detection and analysis of special objects from the solar corona in the data taken by the EIT instrument aboard the SoHO satellite.  The second problem is the detection of gamma sources in the Universe, the difficulty lying in the discrimination of faint sources against a highly nonuniform background and the large number of sources (to be) measured by satellites like EGRET and GLAST.
