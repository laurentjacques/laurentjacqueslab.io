---
title: "Non-Convex Blind Calibration for Compressed Sensing via Iterative Hard Thresholding"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Valerio Cambareri
- Laurent Jacques
tags:
- '"AlterSense"'
- '"Compressed Sensing"'
- '"Blind Calibration"'
- '"Non-Convex Optimisation"'
- '"Bilinear Inverse Problems"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:33.801611Z'
publication_types:
- '1'
abstract: 
publication: ''
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178320
---

**Abstract**: Real-world applications of compressed sensing are often limited by modelling errors between the sensing operator, which is necessary during signal recovery, and its actual physical implementation. In this paper we tackle the biconvex problem of recovering a sparse input signal jointly with some unknown and unstructured multiplicative factors affecting the sensors that capture each measurement. Our methodology relies on collecting a few snapshots under new draws of the sensing operator, and applying a greedy algorithm whose iterates perform projected gradient descent on a non-convex objective followed by a hard thresholding step. The sample complexity requirements of this setup are explored by testing the empirical phase transition of our algorithm; this evidence is complemented by a compressive imaging experiment, showing that the exact solution can be obtained with only a few snapshots.
