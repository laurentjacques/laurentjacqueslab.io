---
title: "Discrete wavelet frames on the sphere"
author: ["Laurent Jacques"]
tags: [1, 2004]
draft: false
subtitle: ''
summary: ''
authors:
- Iva Bogdanova
- Pierre Vandergheynst
- Jean-Pierre Antoine
- Laurent Jacques
- Marcela Morvidone
tags:
- '"Wavelet"'
- '"Sphere"'
- '"Frames"'
- '"Signal representation"'
categories: []
date: '2004-01-01'
lastmod: 2021-08-06T23:54:40+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:40.807274Z'
publication_types:
- '1'
abstract: 
publication: "*Proc. EUSIPCO'04, Vienna, 2004*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105943
---
