---
title: "Morphological components analysis for circumstellar disks imaging"
author: ["Laurent Jacques"]
tags: [1, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Benoît Pairet
- Faustine Cantalloube
- Laurent Jacques
tags:
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:27+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:26.870043Z'
publication_types:
- '1'
abstract: 
publication: "*in Proceedings of iTWIST'20, Paper-ID: 44, Nantes, France, December,
   2-4, 2020*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242106
---

**Abstract**: Recent developments in astronomical observations enable direct imaging of circumstellar disks. Precise characterization of such extended structure is essential to our understanding of stellar systems. However, the faint intensity of the circumstellar disks compared to the brightness of the host star compels astronomers to use tailored observation strategies, in addition to state-of-the-art optical devices. Even then, extracting the signal of circumstellar disks heavily relies on post-processing techniques.  In this work, we propose a morphological component analysis (MCA) approach that leverages low-complexity models of both the disks and the stellar light corrupting the data. In addition to disks, our method allows to image exoplanets. Our approach is tested through numerical experiments.
