---
title: "Correspondence principle between spherical and Euclidean wavelets"
author: ["Laurent Jacques"]
tags: [2, 2005]
draft: false
subtitle: ''
summary: ''
authors:
- Yves Wiaux
- P. Vandergheynst
- Laurent Jacques
tags:
- '"Astrophysics"'
- '"General Relativity and Quantum Cosmology"'
- '"Mathematical Physics"'
categories: []
date: '2005-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.267268Z'
publication_types:
- '2'
abstract: 
publication: '*The Astrophysical Journal : an international review of astronomy and
  astronomical physics*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078/30998
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/astro-ph/0502486
---

**Abstract**: Wavelets on the sphere are reintroduced and further developed independently of the original group theoretic formalism, in an equivalent, but more straightforward approach. These developments are motivated by the interest of the scale-space analysis of the cosmic microwave background (CMB) anisotropies on the sky. A new, self-consistent, and practical approach to the wavelet filtering on the sphere is developed. It is also established that the inverse stereographic projection of a wavelet on the plane (i.e. Euclidean wavelet) leads to a wavelet on the sphere (i.e. spherical wavelet).  This new correspondence principle simplifies the construction of wavelets on the sphere and allows to transfer onto the sphere properties of wavelets on the plane.  In that regard, we define and develop the notions of directionality and steerability of filters on the sphere. In the context of the CMB analysis, these notions are important tools for the identification of local directional features in the wavelet coefficients of the signal, and for their interpretation as possible signatures of non-gaussianity, statistical anisotropy, or foreground emission. But the generic results exposed may find numerous applications beyond cosmology and astrophysics.
