---
title: "Sparsity Driven People Localization with a Heterogeneous Network of Cameras"
author: ["Laurent Jacques"]
tags: [2, 2011]
draft: false
subtitle: ''
summary: ''
authors:
- Alexandre Alahi
- Laurent Jacques
- Yannick Boursier
- Pierre Vandergheynst
tags:
- '"People detection"'
- '"Localization"'
- '"Sparse approximation"'
- '"Convex optimization"'
- '"Omnidirectional cameras"'
- '"Dictionary"'
- '"Multi-view"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:24.452302Z'
publication_types:
- '2'
abstract: 
publication: '*Journal of Mathematical Imaging and Vision*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/87549
doi: https://dx.doi.org/10.1007/s10851-010-0258-7
---

**Abstract**: This paper addresses the problem of localizing people in low and high density crowds with a network of heterogeneous cameras. The problem is recasted as a linear inverse problem. It relies on deducing the discretized occupancy vector of people on the ground, from the noisy binary silhouettes observed as foreground pixels in each camera. This inverse problem is regularized by imposing a sparse occupancy vector, i.e. made of few non-zero elements, while a particular dictionary of silhouettes linearly maps these non-empty grid locations to the multiple silhouettes viewed by the cameras network. The proposed framework is (i) generic to any scene of people, i.e. people are located in low and high density crowds, (ii) scalable to any number of cameras and already working with a single camera, (iii) unconstraint on the scene surface to be monitored, and (iv) versatile with respect to the camera’s geometry, e.g. planar or omnidirectional. Qualitative and quantitative results are presented on the APIDIS and the PETS 2009 Benchmark datasets. The proposed algorithm successfully detects people occluding each other given severely de- graded extracted features, while outperforming state-of-the-art people localization techniques.
