---
title: "Performance of Compressive Sensing for Hadamard-Haar Systems"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- José Bioucas-Dias
- Laurent Jacques
tags:
- '"Hadamard transform"'
- '"Haar wavelet"'
- '"compressive sensing"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:28+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:28.632991Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of SPARS'19*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/222269
---

**Abstract**: We study the problem of image recovery from subsampled Hadamard measurements using Haar wavelet sparsity prior. This problem is of interest in, e.g., computational imaging applications relying on optical multiplexing or single pixel imaging. While the high coherence between the Hadamard and Haar bases hinders the efficacy of such devices, the multilevel density sampling strategy solves this issue by adjusting the subsampling process to the local coherence between both bases; hence allowing for successful image recovery. In this work, we compute an explicit sample-complexity bound for Hadamard-Haar systems; a seemingly missing result in the related literature.  The power of this approach is demonstrated on several experimental simulations.
