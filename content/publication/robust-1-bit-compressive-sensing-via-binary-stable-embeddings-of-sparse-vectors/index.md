---
title: "Robust 1-Bit Compressive Sensing via Binary Stable Embeddings of Sparse Vectors"
author: ["Laurent Jacques"]
tags: [2, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Jason N. Laska
- Petros T. Boufounos
- Richard G. Baraniuk
tags:
- '"Compressed sensing"'
- '"1-bit quantization"'
- '"sparse signal recovery"'
- '"binary stable embeddings"'
- '"measure concentration"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:23+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:23.466819Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078/123200
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1104.3160
- name: Demo
  icon_pack: ai
  url: demo_BIHT.html
url_code: BIHT_demo.zip 
doi: https://dx.doi.org/10.1109/TIT.2012.2234823
---

**Abstract**: The Compressive Sensing (CS) framework aims to ease the burden on analog-to-digital converters (ADCs) by reducing the sampling rate required to acquire and stably recover sparse signals. Practical ADCs not only sample but also quantize each measurement to a finite number of bits; moreover, there is an inverse relationship between the achievable sampling rate and the bit-depth. In this paper, we investigate an alternative CS approach that shifts the emphasis from the sampling rate to the number of bits per measurement. In particular, we explore the extreme case of 1-bit CS measurements, which capture just their sign. Our results come in two flavors. First, we consider ideal reconstruction from noiseless 1-bit measurements and provide a lower bound on the best achievable reconstruction error. We also demonstrate that i.i.d. random Gaussian matrices provide measurement mappings that, with overwhelming probability, achieve nearly optimal error decay. Next, we consider reconstruction robustness to measurement errors and noise and introduce the Binary - Stable Embedding (BSE) property, which characterizes the robustness of the measurement process to sign changes. We show that the same class of matrices that provide almost optimal noiseless performance also enable such a robust mapping. On the practical side, we introduce the Binary Iterative Hard Thresholding (BIHT) algorithm for signal reconstruction from 1-bit measurements that offers stateof- the-art performance.
