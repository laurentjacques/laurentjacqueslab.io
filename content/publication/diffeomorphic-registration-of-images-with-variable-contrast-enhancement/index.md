---
title: "Diffeomorphic Registration of Images with Variable Contrast Enhancement"
author: ["Laurent Jacques"]
tags: [2, 2011]
draft: false
subtitle: ''
summary: ''
authors:
- Guillaume Janssens
- Laurent Jacques
- Jonathan Orban de Xivry
- Xavier Geets
- Benoît Macq
tags:
- '"Demons"'
- '"Diffeomorphic"'
- '"Morphons"'
- '"Registration"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:24+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:24.322633Z'
publication_types:
- '2'
abstract: 
publication: '*International Journal of Biomedical Imaging*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/69253
url_pdf: http://downloads.hindawi.com/journals/ijbi/2011/891585.pdf
doi: https://dx.doi.org/10.1155/2011/891585
---
