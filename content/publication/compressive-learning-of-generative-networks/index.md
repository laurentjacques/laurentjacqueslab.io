---
title: "Compressive Learning of Generative Networks"
author: ["Laurent Jacques"]
tags: [1, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Laurent Jacques
tags:
- '"Generative networks"'
- '"Compressive Learning"'
- '"Sketched Learning"'
- '"Generative Moment Matching Networks"'
- '"Maximum Mean Discrepancy"'
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:26.416273Z'
publication_types:
- '1'
abstract: 
publication: '*European Symposium on Artificial Neural Networks, Computational Intelligence
  and Machine Learning (ESANN)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/236933
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2002.05095
---

**Abstract**: Generative networks implicitly approximate complex densities from their sampling with impressive accuracy. However, because of the enormous scale of modern datasets, this training process is often computationally expensive. We cast generative network training into the recent framework of compressive learning: we reduce the computational burden of large-scale datasets by first harshly compressing them in a single pass as a single sketch vector. We then propose a cost function, which approximates the Maximum Mean Discrepancy metric, but requires only this sketch, which makes it time-and memory-efficient to optimize.
