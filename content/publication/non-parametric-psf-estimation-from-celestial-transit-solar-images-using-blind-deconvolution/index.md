---
title: "Non-parametric PSF estimation from celestial transit solar images using blind deconvolution"
author: ["Laurent Jacques"]
tags: [2, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Adriana Gonzalez Gonzalez
- Véronique Delouille
- Laurent Jacques
tags:
- '"Point Spread Function"'
- '"Blind deconvolution"'
- '"Venus transit"'
- '"Moon transit"'
- '"SDO/AIA"'
- '"SECCHI/EUVI"'
- '"Proximal algorithms"'
- '"Sparse regularization"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.172403Z'
publication_types:
- '2'
abstract: 
publication: '*Journal of Space Weather and Space Climate*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/169498
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1412.6279
doi: https://dx.doi.org/10.1051/swsc/2015040
---

**Context**: Characterization of instrumental effects in astronomical imaging is important in order to extract accurate physical information from the observations.  The measured image in a real optical instrument is usually represented by the convolution of an ideal image with a Point Spread Function (PSF). Additionally, the image acquisition process is also contaminated by other sources of noise (read-out, photon-counting).  The problem of estimating both the PSF and a denoised image is called blind deconvolution and is ill-posed.

**Aims**: We propose a blind deconvolution scheme that relies on image regularization. Contrarily to most methods presented in the literature, our method does not assume a parametric model of the PSF and can thus be applied to any telescope.

**Methods**: Our scheme uses a wavelet analysis prior model on the image and weak assumptions on the PSF. We use observations from a celestial transit, where the occulting body can be assumed to be a black disk. These constraints allow us to retain meaningful solutions for the filter and the image, eliminating trivial, translated, and interchanged solutions. Under an additive Gaussian noise assumption, they also enforce noise canceling and avoid reconstruction artifacts by promoting the whiteness of the residual between the blurred observations and the cleaned data. Results: Our method is applied to synthetic and experimental data. The PSF is estimated for the SECCHI/EUVI instrument using the 2007 Lunar transit, and for SDO/AIA using the 2012 Venus transit. Results show that the proposed non-parametric blind deconvolution method is able to estimate the core of the PSF with a similar quality to parametric methods proposed in the literature. We also show that, if these parametric estimations are incorporated in the acquisition model, the resulting PSF outperforms both the parametric and non-parametric methods.
