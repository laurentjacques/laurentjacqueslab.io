---
title: "Going Below and Beyond, Off-the-Grid Velocity Estimation from 1-bit Radar Measurements"
author: ["Laurent Jacques"]
tags: [1, 2021]
draft: false
subtitle: ''
summary: ''
authors:
- Gilles Monnoyer de Galland de Carnières
- Thomas Feuillen
- Luc Vandendorpe
- Laurent Jacques
tags:
- '"Radar"'
- '"compressive sensing"'
- '"one-bit"'
- '"off-the-grid"'
- '"continuous matching pursuit"'
- '"Radar"'
- '"Compressive Sensing"'
- '"one-bit"'
- '"off-the-grid"'
- '"continuous matching pursuit"'
categories: []
date: '2021-01-01'
lastmod: 2021-08-06T23:54:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:26.146303Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of RadarConf 2021*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/243375
---

**Abstract**: In this paper we propose to bridge the gap between using extremely low resolution 1-bit measurements and estimating targets' parameters, such as their velocities, that exist in a continuum, i.e., by performing Off-the-Grid estimation. To that end, a Continuous version of Orthogonal Matching Pursuit (COMP) is modified in order to leverage the 1-bit measurements coming from a simple Doppler Radar. Using Monte-Carlo simulations, we show that although the resolution of the acquisition is dramatically reduced, velocity estimation of multiple targets can still be achieved and reaches performances beyond classic On-the-Grid methods. Furthermore, we show empirically that adding a random and uniform dithering before the quantization is necessary when estimating more than one target.
