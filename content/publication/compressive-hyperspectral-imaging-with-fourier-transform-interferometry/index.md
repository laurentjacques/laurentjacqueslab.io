---
title: "Compressive Hyperspectral Imaging with Fourier Transform Interferometry"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Kévin Degraux
- Valerio Cambareri
- Adriana Gonzales
- M. Roblin
- Laurent Jacques
- Philippe Antoine
tags:
- '"AlterSense"'
- '"inverse problem"'
- '"exo-planet detection"'
- '"sparse signal recovery"'
- '"low-rank approximation"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:33.184296Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of the third international Traveling Workshop on Interactions
   between Sparse models and Technology (iTWIST'16), arXiv:1609.04167*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/176827
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1609.04167
---

**Abstract**: This paper studies the fast acquisition of Hyper- Spectral (HS) data using Fourier transform interferometry (FTI). FTI has emerged as a promising alternative to capture, at a very high resolution, the wavelength coordinate as well as the spatial domain of the HS volume. A drawback of the conventional FTI devices is a typically slow acquisition process. In this paper we develop a compressive sensing (CS) framework for FTI. By exploiting the sparsity of the target HS data in a 3-D wavelet basis we show how the actual HS data can be retrieved from partial FTI measurements.  Furthermore, we develop an alternative sampling strategy, i.e., a variable density rather than uniform sampling scheme to boost the acquisition accuracy. Extensive simulations show that (i) the proposed method is applicable to realistic FTI and (ii) the variable density sampling scheme is more accurate than conventional uniform sampling.
