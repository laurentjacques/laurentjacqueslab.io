---
title: "An Analog-to-Information VGA Image Sensor Architecture for Support Vector Machine on Compressive Measurements"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Wissam Benjilali
- William Guicquero
- Laurent Jacques
- Gilles Sicard
tags:
- '"embedded object regognition"'
- '"compressive sensing"'
- '"pseudo-random permutation"'
- '"Sigma-Delta"'
- '"SVM"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:27+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:27.386154Z'
publication_types:
- '1'
abstract: 
publication: '*2019 IEEE International Symposium on Circuits and Systems (ISCAS)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/216656
doi: https://dx.doi.org/10.1109/iscas.2019.8702325
---

**Abstract**: This work presents a compact VGA (480 × 640) CMOS Image Sensor (CIS) architecture with dedicated end-of-column Compressive Sensing (CS) scheme allowing embedded object recognition. The architecture takes advantage of a low-footprint pseudo-random data mixing circuit and a first order incremental Sigma-Delta (ΣΔ) Analog to Digital Converter (ADC) to extract compressed features. The proposed CIS achieves an object recognition accuracy of ≃ 93% on the Georgia Tech face recognition database (GIT, 10 classes out of 50) thanks to a linear Support Vector Machine (SVM) classifier implemented by an optimized Digital Signal Processing (DSP). We stress that the signal independent dimensionality reduction performed by our dedicated CS scheme (1/480) allows to dramatically reduce memory requirements (≈ 32 kbit) related −in our case− to the ex-situ learned affine function of the linear SVM.
