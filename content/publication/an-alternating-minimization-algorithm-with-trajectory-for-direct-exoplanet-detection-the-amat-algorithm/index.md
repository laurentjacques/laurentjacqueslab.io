---
title: "An alternating minimization algorithm with trajectory for direct exoplanet detection – The AMAT algorithm"
author: ["Laurent Jacques"]
tags: [2, 2024]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Hazan Daglayan
- Simon Vary
- Olivier Absil
- Faustine Cantalloube
- Valentin Christiaens
- Nicolas Gillis
- Laurent Jacques
- Valentin Leplat
- P.-A. Absil
tags: # One keywords per item "-" with format - '"keyword"'
- '"exoplanet detection"'
- '"data analysis"'
- '"image processing"'
- '"detection"'
- '"exoplanet detection"'
- '"L1-norm minimization"'
- '"low-rank approximation"'
#categories: []
date: '2024-12-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named Symbol’s value as variable is void: featured.jpg/png to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. Symbol’s value as variable is void: projects references Symbol’s value as variable is void: content/project/deep-learning/index.md.
#   Otherwise, set Symbol’s value as variable is void: projects.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
abstract: ''
publication: '*Astronomy & Astrophysics, 692, pp. A126, 2024*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.1051/0004-6361/202451242'

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2410.06310 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/294770 #add dial handle or comment this out
---

**Abstract**: Effective image post-processing algorithms are vital for the successful direct imaging of exoplanets. Standard PSF subtraction methods use techniques based on a low-rank approximation to separate the rotating planet signal from the quasi-static speckles, and rely on signal-to-noise ratio maps to detect the planet. These steps do not interact or feed each other, leading to potential limitations in the accuracy and efficiency of exoplanet detection. We aim to develop a novel approach that iteratively finds the flux of the planet and the low-rank approximation of quasi-static signals, in an attempt to improve upon current PSF subtraction techniques. In this study, we extend the standard L2 norm minimization paradigm to an L1 norm minimization framework to better account for noise statistics in the high contrast images. Then, we propose a new method, referred to as Alternating Minimization Algorithm with Trajectory, that makes a more advanced use of estimating the low-rank approximation of the speckle field and the planet flux by alternating between them and utilizing both L1 and L2 norms. For the L1 norm minimization, we propose using L1 norm low-rank approximation, a low-rank approximation computed using an exact block-cyclic coordinate descent method, while we use randomized singular value decomposition for the L2 norm minimization. Additionally, we enhance the visibility of the planet signal using a likelihood ratio as a postprocessing step. Numerical experiments performed on a VLT/SPHERE-IRDIS dataset show the potential of AMAT to improve upon the existing approaches in terms of higher S/N, sensitivity limits, and ROC curves. Moreover, for a systematic comparison, we used datasets from the exoplanet data challenge to compare our algorithm to other algorithms in the challenge, and AMAT with likelihood ratio map performs better than most algorithms tested on the exoplanet data challenge.
