---
title: "Bits of Images: Inverting Local Image Binary Descriptors"
author: ["Laurent Jacques"]
tags: [1, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Emmanuel d'Angelo
- Pierre Vandergheynst
- Laurent Jacques
- Alexandre Alahi
tags:
- '"binary descriptor"'
- '"inverse problem"'
- '"1-bit compressed sensing"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:35.593712Z'
publication_types:
- '1'
abstract: 
publication: '*Bits of Images: Inverting Local Image Binary Descriptors*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134465
---

```emacs-lisp
(print (nth 5 (org-heading-components)))
```

**Abstract**: Local Binary Descriptors (LBDs) are good at matching image parts, but what information is actually carried? This question is usually masked by a comparison of matching performances. In this work, we leverage an inverse problem approach to reconstruct the image content from LBDs. We show that this task is achievable with only the information in the descriptors, excluding the need of additional data.  Furthermore, our reconstruction scheme reveals differences in the way different LBDs capture image information. The possible applications of our work are various, from privacy issues on mobile communication networks to the design of better descriptors and smart cameras.
