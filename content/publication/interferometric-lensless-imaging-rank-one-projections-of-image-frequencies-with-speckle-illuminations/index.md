---
title: "Interferometric lensless imaging: rank-one projections of image frequencies with speckle illuminations"
author: ["Laurent Jacques"]
tags: [2, 2023]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Olivier Leblanc
- Mathias Hofer 
- Siddharth Sivankutty
- Hervé Rigneault 
- Laurent Jacques

tags: # One keywords per item "-" with format - '"keyword"'
- '"lensless imaging"'
- '"rank-one projections"'
- '"interferometric matrix"'
- '"inverse problem"'
- '"computational imaging"'
- '"single-pixel"'
#categories: []
date: '2023-06-23'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
abstract: ''
publication: '*IEEE Transactions on Computational Imaging*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2306.12698 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/284957 #add dial handle or comment this out
doi: https://doi.org/10.1109/tci.2024.3359178 
---

**Abstract**: Lensless illumination single-pixel imaging with a multicore fiber (MCF) is a computational imaging technique that enables potential endoscopic observations of biological samples at cellular scale. In this work, we show that this technique is tantamount to collecting multiple symmetric rank-one projections (SROP) of an interferometric matrix--a matrix encoding the spectral content of the sample image. In this model, each SROP is induced by the complex sketching vector shaping the incident light wavefront with a spatial light modulator (SLM), while the projected interferometric matrix collects up to \\(O(Q^2)\\) image frequencies for a \\(Q\\)-core MCF. While this scheme subsumes previous sensing modalities, such as raster scanning (RS) imaging with beamformed illumination, we demonstrate that collecting the measurements of \\(M\\) random SLM configurations--and thus acquiring \\(M\\) SROPs--allows us to estimate an image of interest if \\(M\\) and \\(Q\\) scale log-linearly with the image sparsity level This demonstration is achieved both theoretically, with a specific restricted isometry analysis of the sensing scheme, and with extensive Monte Carlo experiments. On a practical side, we perform a single calibration of the sensing system robust to certain deviations to the theoretical model and independent of the sketching vectors used during the imaging phase. Experimental results made on an actual MCF system demonstrate the effectiveness of this imaging procedure on a benchmark image.
