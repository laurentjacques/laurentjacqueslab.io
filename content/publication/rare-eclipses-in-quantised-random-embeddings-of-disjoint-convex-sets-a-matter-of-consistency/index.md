---
title: "Rare Eclipses in Quantised Random Embeddings of Disjoint Convex Sets: a Matter of Consistency?"
author: ["Laurent Jacques"]
tags: [1, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Valerio Cambareri
- Chunlei Xu
- Laurent Jacques
tags:
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:31.889729Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of SPARS'17*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/187193
---

**Abstract**: We study the problem of verifying when two disjoint closed convex sets remain
  separable after the application of a quantised random embedding, as a means to ensure
  exact classification from the signatures produced by this non-linear dimensionality
  reduction. An analysis of the interplay between the embedding, its quantiser resolution
  and the sets’ separation is presented in the form of a convex problem; this is completed
  by its numerical exploration in a special case, for which the phase transition corresponding
  to exact classification is easily computed.
