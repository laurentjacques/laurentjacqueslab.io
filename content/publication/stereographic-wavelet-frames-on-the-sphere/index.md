---
title: "Stereographic wavelet frames on the sphere"
author: ["Laurent Jacques"]
tags: [2, 2005]
draft: false
subtitle: ''
summary: ''
authors:
- Iva Bogdanova
- Pierre Vandergheynst
- Jean-Pierre Antoine
- Laurent Jacques (first author)
- Marcella Morvidone
tags:
- '"continuous wavelet transform"'
- '"spherical wavelets"'
- '"discrete wavelet frames"'
- '"half-continuous frames"'
- '"controlled frames"'
categories: []
date: '2005-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.403833Z'
publication_types:
- '2'
abstract: 
publication: '*Applied and Computational Harmonic Analysis*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/39136
doi: https://dx.doi.org/10.1016/j.acha.2005.05.001
---

**Abstract**: In this paper we exploit the continuous wavelet transform (CWT) on the two-dimensional sphere S-2, introduced previously by two of us, to build associated discrete wavelet frames. We first explore half-continuous frames, i.e., frames where the position remains a continuous variable, and then move on to a fully discrete theory. We introduce the notion of controlled frames, which reflects the particular nature of the underlying theory, in particular the apparent conflict between dilation and the compactness of the S-2 manifold. We also highlight some implementation issues and provide numerical illustrations. (c) 2005 Elsevier Inc. All rights reserved.
