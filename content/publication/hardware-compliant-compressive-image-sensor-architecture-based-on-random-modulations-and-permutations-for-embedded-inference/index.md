---
title: "Hardware-Compliant Compressive Image Sensor Architecture Based on Random Modulations  and Permutations for Embedded Inference"
author: ["Laurent Jacques"]
tags: [2, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Wissam Benjilali
- William Guicquero
- Laurent Jacques
- Gilles Sicard
tags:
- '"Electrical and Electronic Engineering"'
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.781503Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Circuits and Systems I: Regular Papers*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242057
doi: https://dx.doi.org/10.1109/tcsi.2020.2971565
---

**Abstract**: This work presents a compact CMOS Image Sensor (CIS) architecture enabling embedded object recognition facilitated by a dedicated end-of-column Compressive Sensing (CS), reducing on-chip memory needs. Our sensing scheme is based on a combination of random modulations and permutations leading to an implementation with very limited hardware impacts. It is designed to meet both theoretical (i.e., stable embedding, measurements incoherence) and practical requirements (i.e., silicon footprint, power consumption). The only additional hardware compared to a standard CIS architecture using first order incremental Sigma-Delta (ΣA) Analog to Digital Converter (ADC) are a pseudo-random data mixing circuit, an in-ΣA ±1 modulator and a small Digital Signal Processor (DSP). On the algorithmic side, three variants are presented to perform the inference on compressed measurements with a tunable complexity (i.e., onevs.-all SVM, hierarchical SVM and small ANN with 1-D maxpooling). An object recognition accuracy of ≃98.8% is reached on the COIL database (COIL, 100 classes) using our dedicated Neural Network classifier. We stress that the signal-independent dimensionality reduction performed by our dedicated CS scheme (1/480 in 480 × 640 VGA resolution case) allows to dramatically reduce memory requirements mainly related to the remotely learned coefficients used for the inference stage.
