---
title: "Compressive radio-interferometric sensing with random beamforming as rank-one signal covariance projections"
author: ["Laurent Jacques"]
tags: [3, 2024]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Olivier Leblanc
- Yves Wiaux
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"radio-interferometry"'
- '"rank-one projections"'
- '"interferometric matrix"'
- '"inverse problem"'
- '"computational imaging"'
#categories: []
date: '2024-10-28'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named Symbol’s value as variable is void: featured.jpg/png to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. Symbol’s value as variable is void: projects references Symbol’s value as variable is void: content/project/deep-learning/index.md.
#   Otherwise, set Symbol’s value as variable is void: projects.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '3'
abstract: ''
publication: '*Submitted to Transactions on Computational Imaging*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2409.15031 #add arxiv identifier here
# - name: DIAL
#  url: http://hdl.handle.net/ #add dial handle or comment this out
---

**Abstract**: Radio-interferometry (RI) observes the sky at unprecedented angular resolutions, enabling the study of several far-away galactic objects such as galaxies and black holes. In RI, an array of antennas probes cosmic signals coming from the observed region of the sky. The covariance matrix of the vector gathering all these antenna measurements offers, by leveraging the Van Cittert-Zernike theorem, an incomplete and noisy Fourier sensing of the image of interest. The number of noisy Fourier measurements -- or visibilities -- scales as \\(\mathcal O(Q^2B)\\) for \\(Q\\) antennas and \\(B\\) short-time integration (STI) intervals. We address the challenges posed by this vast volume of data, which is anticipated to increase significantly with the advent of large antenna arrays, by proposing a compressive sensing technique applied directly at the level of the antenna measurements. First, this paper shows that beamforming -- a common technique of dephasing antenna signals -- usually used to focus some region of the sky, is equivalent to sensing a rank-one projection (ROP) of the signal covariance matrix. We build upon our recent work [arXiv:2306.12698v3 [eess.IV]​](https://arxiv.org/abs/2306.12698) to propose a compressive sensing scheme relying on random beamforming, trading the $Q^2$-dependence of the data size for a smaller number \\(N\_{\mathrm p}\\) ROPs. We provide image recovery guarantees for sparse image reconstruction. Secondly, the data size is made independent of \\(B\\) by applying \\(N\_{\mathrm m}\\) Bernoulli modulations of the ROP vectors obtained for the STI. The resulting sample complexities, theoretically derived in a simpler case without modulations and numerically obtained in phase transition diagrams, are shown to scale as \\(\mathcal O(K)\\) where \\(K\\) is the image sparsity. This illustrates the potential of the approach.
