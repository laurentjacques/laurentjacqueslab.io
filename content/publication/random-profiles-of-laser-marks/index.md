---
title: "Random Profiles of Laser Marks"
author: ["Laurent Jacques"]
tags: [1, 2010]
draft: false
subtitle: ''
summary: ''
authors:
- Saloomeh Shariati
- François-Xavier Standaert
- Laurent Jacques
- Benoît Macq
- M. Salhi
- Philippe Antoine
tags:
categories: []
date: '2010-01-01'
lastmod: 2021-08-06T23:54:38+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:38.768633Z'
publication_types:
- '1'
abstract: 
publication: '*PROCEEDINGS OF THE SYMPOSIUM ON INFORMATION THEORY IN THE BENELUX*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/81802
---

**Abstract**: In this paper, we propose a secure anti-counterfeiting solution based on the ex- traction of a unique identifier out of the random profile of laser marks that are engraved on the surface or in the bulk of physical objects. Given today's tech- nology, the 3D profile of a laser mark can be considered uncloneable. Indeed, reproducing a mark would be so expensive that it thwarts any attempt to build a twin of a given mark. Actually, we rely on the resolution difference that can be achieved between the engraving which can be seen as pretty coarse, and the much more precise reading process. The solution we propose exploits these phys- ical features in the design of a comprehensive anti-counterfeiting system based on existing tools. Experimental results demonstrate the feasibility of using the framework for

real applications.
