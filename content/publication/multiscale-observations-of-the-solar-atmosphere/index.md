---
title: "Multiscale observations of the solar atmosphere"
author: ["Laurent Jacques"]
tags: [1, 2003]
draft: false
subtitle: ''
summary: ''
authors:
- JF Hochedez
- Samuel Gissot
- Laurent Jacques
- Jean-Pierre Antoine
tags: []
categories: []
date: '2003-01-01'
lastmod: 2021-08-06T23:54:42+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:41.907566Z'
publication_types:
- '1'
abstract: 
publication: '*GROUP 24: Physical and Mathematical Aspects of Symmetries.*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/61239
---

**Abstract**: The observations of the Extreme ultraviolet Imaging Telescope (EIT) contain various small features of the solar coronal regions: EUV bright points, ephemeral regions, brightenings, loops segments, cosmic ray hits, etc. In this work we present an automated method extracting small objects. It is based on the continuous 2D Mexican Hat Wavelet transform. We study global and long-term statistical properties of the small features. Their distribution will be used for solar cycle studies and prediction of solar events (Space Weather applications).
