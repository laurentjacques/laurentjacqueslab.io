---
title: "Multispectral Compressive Imaging Strategies Using Fabry–Pérot Filtered Sensors"
author: ["Laurent Jacques"]
tags: [2, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Kevin Degraux
- Valerio Cambareri
- Bert Geelen
- Laurent Jacques
- Gauthier Lafruit
tags:
- '"Multispectral imaging"'
- '"compressed sensing"'
- '"spectral filters"'
- '"Fabry–Pérot"'
- '"random convolution"'
- '"generalized inpainting"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:20.706485Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Computational Imaging*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211763
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1802.02040
doi: https://dx.doi.org/10.1109/tci.2018.2864659
---

**Abstract**: In this paper, we introduce two novel acquisition schemes for multispectral compressive imaging. Unlike most existing methods, the proposed computational imaging techniques do not include any dispersive element, as they use a dedicated sensor that integrates narrowband Fabry–Pérot spectral filters at the pixel level. The first scheme leverages joint inpainting and superresolution to fill in those voxels that are missing due to the device's limited pixel count. The second scheme introduces spatial random convolutions in link with the framework of compressive sensing, but is more complex and may be limited by diffraction. In both cases, we solve the associated inverse problems by using the same signal prior and recovery algorithm. Specifically, we propose a redundant, analysis-sparse signal prior in a convex formulation. We validate our schemes through numerical simulation in different realistic setups.  Moreover, we highlight some practical guidelines and discuss complexity tradeoffs to integrate these schemes into actual computational imaging systems. Our conclusion is that the second technique performs best when targeting high compression levels if embodied in a properly designed and calibrated setup. Otherwise the first, simpler technique should be favored.
