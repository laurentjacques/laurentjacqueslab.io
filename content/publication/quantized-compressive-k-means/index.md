---
title: "Quantized Compressive K-Means"
author: ["Laurent Jacques"]
tags: [2, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Laurent Jacques
tags:
- '"1-bit universal quantization"'
- '"compressed sensing"'
- '"compressive learning"'
- '"k-means clustering"'
- '"1-bit universal quantization; compressed sensing"'
- '"compressive learning"'
- '"k-means clustering"'
- '"1-bit universal quantization"'
- '"compressed sensing"'
- '"compressive learning"'
- '"k-means clustering"'
- '"1-bit universal quantization"'
- '"compressed sensing"'
- '"compressive learning"'
- '"k-means clustering"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:20.854293Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Signal Processing Letters*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/202890
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1804.10109
doi: https://dx.doi.org/10.1109/lsp.2018.2847908
---

**Abstract**: The recent framework of compressive statistical learning proposes to design tractable learning algorithms that use only a heavily compressed representation - or sketch - of massive datasets. Compressive K-Means (CKM) is such a method: It aims at estimating the centroids of data clusters from pooled, nonlinear, and random signatures of the learning examples. While this approach significantly reduces computational time on very large datasets, its digital implementation wastes acquisition resources because the learning examples are compressed only after the sensing stage. The present work generalizes the CKM sketching procedure to a large class of periodic nonlinearities including hardware-friendly implementations that compressively acquire entire datasets.  This idea is exemplified in a quantized CKM procedure, a variant of CKM that leverages 1-bit universal quantization (i.e., retaining the least significant bit of a standard uniform quantizer) as the periodic sketch nonlinearity. Trading for this resource-efficient signature (standard in most acquisition schemes) has almost no impact on the clustering performance, as illustrated by numerical experiments.
