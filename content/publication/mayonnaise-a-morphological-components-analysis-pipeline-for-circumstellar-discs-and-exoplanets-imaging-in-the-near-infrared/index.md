---
title: "MAYONNAISE: a morphological components analysis pipeline for circumstellar discs and exoplanets imaging in the near-infrared"
author: ["Laurent Jacques"]
tags: [2, 2021]
draft: false
subtitle: ''
summary: ''
authors:
- Benoît Pairet
- Faustine Cantalloube
- Laurent Jacques
tags:
- '"Space and Planetary Science"'
- '"Astronomy and Astrophysics"'
- '"techniques: high angular resolution"'
- '"techniques: image processing"'
- '"planet–disc interactions"'
- '"protoplanetary discs"'
- '"circumstellar matter"'
categories: []
date: '2021-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.195022Z'
publication_types:
- '2'
abstract: 
publication: '*Monthly Notices of the Royal Astronomical Society*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/246586
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2008.05170
doi: https://dx.doi.org/10.1093/mnras/stab607
---

**Abstract**: Imaging circumstellar discs in the near-infrared provides unprecedented information about the formation and evolution of planetary systems. However, current post-processing techniques for high-contrast imaging using ground-based telescopes have a limited sensitivity to extended signals and their morphology is often plagued with strong morphological distortions. Moreover, it is challenging to disentangle planetary signals from the disc when the two components are close or intertwined.  We propose a pipeline that is capable of detecting a wide variety of discs and preserving their shapes and flux distributions. By construction, our approach separates planets from discs. After analysing the distortions induced by the current angular differential imaging (ADI) post-processing techniques, we establish a direct model of the different components constituting a temporal sequence of high-contrast images. In an inverse problem framework, we jointly estimate the starlight residuals and the potential extended sources and point sources hidden in the images, using low-complexity priors for each signal. To verify and estimate the performance of our approach, we tested it on VLT/SPHERE-IRDIS data, in which we injected synthetic discs and planets. We also applied our approach on observations containing real discs. Our technique makes it possible to detect discs from ADI data sets of a contrast above 3 × 10−6 with respect to the host star. As no specific shape of the discs is assumed, we are capable of extracting a wide diversity of discs, including face-on discs. The intensity distribution of the detected disc is accurately preserved and point sources are distinguished, even close to the disc.
