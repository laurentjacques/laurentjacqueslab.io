---
title: "The Separation Capacity of Random Neural Networks"
author: ["Laurent Jacques"]
tags: [2, 2021]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Sjoerd Dirksen
- Martin Genzel 
- Laurent Jacques 
- Alexander Stollenwerk
tags: # One keywords per item "-" with format - '"keyword"'
- '"random neural network"'
- '"measure concentration"'
- '"separation capacity"'
- '"machine learning"'
- '"classification"'
#categories: []
date: '2022-11-25'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
publication: '*Journal of Machine Learning Research*' #publication: '*<journal-type>*'
#url_pdf: 
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
#doi: ''

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2108.00207 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/267732 #add dial handle or comment this out
---

**Abstract**: Neural networks with random weights appear in a variety of machine learning applications, most prominently as the initialization of many deep learning algorithms and as a computationally cheap alternative to fully learned neural networks. In the present article we enhance the theoretical understanding of random neural nets by addressing the following data separation problem: under what conditions can a random neural network make two classes \\(\mathcal{X}^-, \mathcal{X}^+ \subset \mathbb{R}^d\\) (with positive distance) linearly separable? We show that a sufficiently large two-layer ReLU-network with standard Gaussian weights and uniformly distributed biases can solve this problem with high probability. Crucially, the number of required neurons is explicitly linked to geometric properties of the underlying sets \\(\mathcal{X}^-, \mathcal{X}^+\\) and their mutual arrangement. This instance-specific viewpoint allows us to overcome the usual curse of dimensionality (exponential width of the layers) in non-pathological situations where the data carries low-complexity structure. We quantify the relevant structure of the data in terms of a novel notion of mutual complexity (based on a localized version of Gaussian mean width), which leads to sound and informative separation guarantees. We connect our result with related lines of work on approximation, memorization, and generalization.
