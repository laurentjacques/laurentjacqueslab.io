---
title: "Quadratic polynomial kernel approximation with asymmetric embeddings"
author: ["Laurent Jacques"]
tags: [1, 2024]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Rémi Delogne
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"random projections"'
- '"random Fourier featuress"'
- '"one-bit quantization"'
- '"random sketches"'
#categories: []
date: '2024-10-28'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named Symbol’s value as variable is void: featured.jpg/png to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. Symbol’s value as variable is void: projects references Symbol’s value as variable is void: content/project/deep-learning/index.md.
#   Otherwise, set Symbol’s value as variable is void: projects.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
abstract: ''
publication: '*International Workshop on Deep Learning and Kernel Machines (2024)*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
links:
# - icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/285963 #add dial handle or comment this out
---

**Abstract**: Random embedding techniques, such as random Fourier features, are widely used to sketch initial data to a new, kernelised feature space. In this work, we leverage a specific property of random rank-one projection operators, the sign product embedding, to approximate a quadratic polynomial kernel using the scalar product of a pair asymmetric vector embeddings, with one taking only binary values. We demonstrate empirically that the approximated kernel compares favourably to the initial one on toy binary classification examples.
