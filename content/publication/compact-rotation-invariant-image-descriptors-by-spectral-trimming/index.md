---
title: "Compact Rotation Invariant Image Descriptors By Spectral Trimming"
author: ["Laurent Jacques"]
tags: [1, 2011]
draft: false
subtitle: ''
summary: ''
authors:
- Maxime Taquet
- Laurent Jacques
- Benoît Macq
- Sylvain Jaume
tags:
- '"Compact Descriptor"'
- '"Invariant features"'
- '"Spectral trimming"'
- '"Transmission Electron Microscope"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:37+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:36.938348Z'
publication_types:
- '1'
abstract: 
publication: '*Image Processing (ICIP), 2011 18th IEEE International Conference on*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/88687
doi: https://dx.doi.org/10.1109/ICIP.2011.6115878
---

**Abstract**: Image descriptors are widely used in applications such as object recognition, pattern classification and image registration. The descriptors encode the local visual content of the image to provide a compact, robust and distinctive representation of objects. If images differ in orientation, descriptors must be rotation invariant.  This paper introduces a compact rotation invariant descriptor. The approach is based on the representation of the local visual content by a graph. A function living on the graph vertices is evaluated and transformed through spectral trimming. This transform is rotation invariant and reduces the dimensionality of the descriptor.  The performance of the introduced descriptor is as good as the SIFT descriptor performance, while being about ten times more compact, as shown by experiments on transmission electron microscope images.
