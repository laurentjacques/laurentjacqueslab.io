---
title: "Compressive Hyperspectral Imaging using Coded Fourier Transform Interferometry"
author: ["Laurent Jacques"]
tags: [1, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Valerio Cambareri
- Laurent Jacques
- Philippe Antoine
- Matthieu Roblin
tags:
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:31+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:31.356674Z'
publication_types:
- '1'
abstract: 
publication: "*Proceedings of SPARS'17*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/187192
---

**Abstract**: Fourier Transform Interferometry (FTI) is a Hyperspectral (HS) imaging technique that is specially desirable in high spectral resolution applications, such as spectral microscopy in biology. The current resolution limit of FTI is actually due to the durability of biological elements when exposed to illuminating light. We propose two variants of the FTI imager, i.e., coded illumination-FTI and coded aperture-FTI, that efficiently allocate the illumination distribution with a variable density sampling strategy, so that the exposure time of the biological specimen is minimized while spectral resolution is preserved. We derive a theoretical analysis for both proposed methods. Our results are supported by several experimental simulations.
