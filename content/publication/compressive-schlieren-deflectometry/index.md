---
title: "Compressive Schlieren Deflectometry"
author: ["Laurent Jacques"]
tags: [1, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Prasad Sudhakara Murthy
- Laurent Jacques
- Xavier Dubois
- Philippe Antoine
- Luc Joannes
tags:
- '"compressed sensing"'
- '"deflectometry"'
- '"schlieren"'
- '"optics"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:36+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:35.915166Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of ICASSP 2013*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/145738
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1212.0433
doi: https://dx.doi.org/10.1109/ICASSP.2013.6638816
---

**Abstract**: Schlieren deflectometry aims at characterizing the deflections undergone by refracted incident light rays at any surface point of a transparent object. For smooth surfaces, each surface location is actually associated with a sparse deflection map (or spectrum). This paper presents a novel method to compressively acquire and reconstruct such spectra. This is achieved by altering the way deflection information is captured in a common Schlieren Deflectometer, i.e., the deflection spectra are indirectly observed by the principle of spread spectrum compressed sensing. These observations are realized optically using a 2-D Spatial Light Modulator (SLM) adjusted to the corresponding sensing basis and whose modulations encode the light deviation subsequently recorded by a CCD camera. The efficiency of this approach is demonstrated experimentally on the observation of few test objects. Further, using a simple parametrization of the deflection spectra we show that relevant key parameters can be directly computed using the measurements, avoiding full reconstruction.
