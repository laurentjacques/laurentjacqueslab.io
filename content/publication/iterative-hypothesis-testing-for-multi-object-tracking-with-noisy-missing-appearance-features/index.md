---
title: "Iterative Hypothesis Testing for Multi-object Tracking with Noisy/Missing Appearance Features"
author: ["Laurent Jacques"]
tags: [1, 2012]
draft: false
subtitle: ''
summary: ''
authors:
- Amit Kumar K.C.
- Damien Delannay
- Laurent Jacques
- Christophe De Vleeschouwer
tags:
- '"multi-object tracking"'
- '"graph-based tracking"'
- '"noisy/missing features"'
- '"iterative hypothesis testing"'
categories: []
date: '2012-01-01'
lastmod: 2021-08-06T23:54:36+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:36.430990Z'
publication_types:
- '1'
abstract: 
publication: '*11th Asian Conference on Computer Vision, Detection and Tracking in
  Challenging Environments Workshop, Daejeon, Korea, Lecture Notes in Computer Science,
  Vol. 7724-7727*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078/117017
doi: https://dx.doi.org/10.1007/978-3-642-37484-5_34
---

**Abstract**: This paper assumes prior detections of multiple targets at each time instant, and uses a graph-based approach to connect those de- tections across time, based on their position and appearance estimates. In contrast to most earlier works in the field, our framework has been designed to exploit the appearance features, even when they are only sporadically available, or affected by a non-stationary noise, along the sequence of detections. This is done by implementing an iterative hy- pothesis testing strategy to progressively aggregate the detections into short trajectories, named tracklets. Specifically, each iteration considers a node, named key-node, and investigates how to link this key-node with other nodes in its neighbourhood, under the assumption that the target appearance is defined by the key-node appearance estimate. This is done through shortest path computation in a temporal neighbourhood of the key-node. The approach is conservative in that it only aggregates the shortest paths that are sufficiently better compared to alternative paths. It is also multi-scale in that the size of the investigated neighbourhood is increased proportionally to the number of detections already aggregated into the key-node. The multi-scale and iterative nature of the process makes it both computationally efficient and effective.  Experimental val- idations are performed extensively on a 15 minutes long real-life basket- ball dataset, captured by 7 cameras, and also on PETS’09 dataset.
