---
title: "Quantization and Compressive Sensing"
author: ["Laurent Jacques"]
tags: [6, 2015]
draft: false
authors:
- Petros Boufounos
- Laurent Jacques
- Felix Krahmer
- Rayan Saab
tags:
- '"Compressed Sensing"'
- '"Quantization"'
- '"Information Theory"'
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:42+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#  caption: ''
#  focal_point: ''
#  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
publishDate: '2021-08-06T21:54:42.488737Z'
publication_types:
- '6'
abstract: 
publication: '*Compressed Sensing and its Applications*'
# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1405.1194 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/165736 #add dial handle or comment this out
doi: https://dx.doi.org/10.1007/978-3-319-16042-9_7
---

**Abstract**: Quantization is an essential step in digitizing signals, and, therefore, an indispensable component of any modern acquisition system. This chapter explores the interaction of quantization and compressive sensing and examines practical quantization strategies for compressive acquisition systems. Specifically, we first provide a brief overview of quantization and examine fundamental performance bounds applicable to any quantization approach. Next, we consider several forms of scalar quantizers, namely uniform, non-uniform, and 1-bit. We provide performance bounds and fundamental analysis, as well as practical quantizer designs and reconstruction algorithms that account for quantization. Furthermore, we provide an overview of Sigma-Delta (Σ Δ) quantization in the compressed sensing context, and also discuss implementation issues, recovery algorithms, and performance bounds. As we demonstrate, proper accounting for quantization and careful quantizer design has significant impact in the performance of a compressive acquisition system.

**Remark**: This is a chapter of the book "[Compressed Sensing and Its Applications](http://www.springer.com/birkhauser/mathematics/book/978-3-319-16041-2)", Edited by H. Boche, R. Calderbank, G. Kutyniok, J. Vybiral (Birkhäuser Mathematics, Springer 2015). ISBN:978-3-319-16041-2 (in combination with the [MATHEON workshop](http://www3.math.tu-berlin.de/numerik/csa2013/index.html)).
