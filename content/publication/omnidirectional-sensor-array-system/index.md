---
title: "Omnidirectional Sensor Array System"
author: ["Laurent Jacques"]
tags: [8, 2010]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- L. Bagnato
- L. Jacques
- P. Vandergheynst
- H. Afshari
- A. Schmid
- Y. Leblebici
tags: # One keywords per item "-" with format - '"keyword"'
- '"panoptic"'
- '"omnidirectional camera"'
- '"depth imaging"'
#categories: []
date: '2010-01-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '8'
#abstract: ''
publication: '*Ecole Polytechnique Fédérale de Lausanne (EPFL).*' #publication: '*<journal-type>*'
#url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
#doi: ''

# custom links
links:
- name: US20140146132
  url: https://patents.google.com/patent/US20140146132
- name: WO2012056437A1
  url: https://patents.google.com/patent/WO2012056437A1
- name: DIAL
  url: http://hdl.handle.net/2078.1/126013 #add dial handle or comment this out
---

**Abstract**: An omnidirectional sensor array system, for example a panoptic camera, comprising a plurality of sensors arranged on a support of predetermined shape to acquire data, wherein said sensors are directional and wherein each sensor is attached to a processing node which comprises integrated electronics that carries out at least a portion of the signal processing algorithms locally in order to reduce the computational load of a central hardware unit.

**Classifications**: H04N5/23238 Control of image capture or reproduction to achieve a very large field of view, _e.g._, panorama.
