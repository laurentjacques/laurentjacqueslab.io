---
title: "Remove, Complete and Detect: Matrix Completion for Direct Imaging of Exoplanets"
author: ["Laurent Jacques"]
tags: [3, 2021]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Xavier Lambein
- Benoît Pairet
- Laurent Jacques
- Faustine Cantalloube
- Olivier Absil
- P.-A. Absil 
tags: # One keywords per item "-" with format - '"keyword"'
- '""'
#categories: []
date: '2021-01-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '3'
abstract: ''
#publication: '**' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: ''

# custom links
#links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
#- name: DIAL
#  url: http://hdl.handle.net/ #add dial handle or comment this out
---
