---
title: "Compressive Single-pixel Fourier Transform Imaging using Structured Illumination"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- José M Bioucas-Dias
- Laurent Jacques
tags:
- '"Hyperspectral"'
- '"Fourier transform interferometry"'
- '"Single pixel imaging"'
- '"Compressive sensing"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:27+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:27.574820Z'
publication_types:
- '1'
abstract: 
publication: '*2019 ICASSP*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211196
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1810.13200
doi: https://dx.doi.org/10.1109/ICASSP.2019.8683396
---

**Abstract**: Single Pixel (SP) imaging is now a reality in many applications, eg, biomedical ultrathin endoscope and fluorescent spectroscopy. In this context, many schemes exist to improve the light throughput of these device, eg, using structured illumination driven by compressive sensing theory. In this work, we consider the combination of SP imaging with Fourier Transform Interferometry (SP-FTI) to reach high-resolution HyperSpectral (HS) imaging, as desirable, eg, in fluorescent spectroscopy. While this association is not new, we here focus on optimizing the spatial illumination, structured as Hadamard patterns, during the optical path progression. We follow a variable density sampling strategy for space-time coding of the light illumination, and show theoretically and numerically that this scheme allows us to reduce the number of measurements and light-exposure of the observed object compared to conventional compressive SP-FTI.
