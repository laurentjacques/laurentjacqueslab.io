---
title: "A (256*256) pixel 76.7mW CMOS imager/ compressor based on real-time in-pixel  compressive sensing:"
author: ["Laurent Jacques"]
tags: [1, 2010]
draft: false
subtitle: ''
summary: ''
authors:
- V. Majidzadeh
- Laurent Jacques
- A. Schmid
- P. Vandergheynst
- Y. Leblebici
tags:
- '"Compressed Sensing"'
- '"Sparse approximation"'
- '"CMOS"'
- '"Random Convolution"'
- '"Compressive Imaging"'
categories: []
date: '2010-01-01'
lastmod: 2021-08-06T23:54:38+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:38.025920Z'
publication_types:
- '1'
abstract: 
publication: '*2010 IEEE International Symposium on Circuits and Systems. ISCAS 2010*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/67356
doi: https://dx.doi.org/10.1109/ISCAS.2010.5538021
---

**Abstract**: A CMOS imager is presented which has the ability to perform localized compressive sensing on-chip. In-pixel convolutions of the sensed image with measurement matrices are computed in real time, and a proposed programmable two-dimensional scrambling technique guarantees the randomness of the coefficients used in successive observation.  A power and area-efficient implementation architecture is presented making use of a single ADC. A 256\*256 imager has been developed as a test vehicle in a 0.18 mu m CIS technology. Using an 11-bit ADC, a SNR of 18.6dB with a compression factor of 3.3 is achieved after reconstruction. The total power consumption of the imager

is simulated at 76.7mW from a 1.8V supply voltage.
