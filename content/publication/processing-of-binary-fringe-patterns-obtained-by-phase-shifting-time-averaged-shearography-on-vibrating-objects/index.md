---
title: "Processing of binary fringe patterns obtained by phase-shifting time-averaged shearography on vibrating objects"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Murielle Kirkove
- Stéphanie Guérit
- Laurent Jacques
- Jean-François Vandenrijt
- Fabian Languy
- Christophe Loffet
- Marc Georges
tags:
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:30+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:30.630297Z'
publication_types:
- '1'
abstract: 
publication: ''
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/208824
---

**Abstract**: Shearography can be used for full-field strain measurements in the field of vibration analysis. It provides the spatial derivative of the optical phase difference of the vibration modes amplitude along the so-called shear direction. The shearographic setup considered here is based on the phase-shifting time-averaged technique. It can easily be applied experimentally, but its drawback is that binary phase patterns are obtained. These phase changes are related to the zeroes of a Bessel function.  Retrieving the corresponding displacement maps is not straightforward. Moreover, integration of shearographic results need to be performed, and this step is sensitive to noise in the patterns. In this paper, different processing stages are described, from fringe denoising to integration of the displacement maps. The application on data acquired in industrial environment illustrates the good performances of the proposed method.
