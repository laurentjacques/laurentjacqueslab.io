---
title: "Compressive Imaging Through Optical Fiber with Partial Speckle Scanning"
author: ["Laurent Jacques"]
tags: [2, 2021, "bibtex"]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Stéphanie Guérit
- Siddharth Sivankutty
- John A. Lee
- Hervé Rigneault
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"Compressive sensing"'
- '"lensless endoscope"'
- '"lensless imaging"'
- '"speckle imaging"'
- '"speckle scanning"'
#categories: []
date: '2021-01-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
publication: '*SIAM Journal on Imaging Sciences*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: https://doi.org/10.1137/21M1407586

# custom links
links:
- icon: arxiv
  icon_pack: ai
  name: arXiv
  url: https://arxiv.org/abs/2104.10959 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/260300 #add dial handle or comment this out
---

**Abstract**: The lensless endoscope (LE) is a promising device to acquire in vivo images at a cellular scale. The tiny size of the probe enables a deep exploration of the tissues. Lensless endoscopy with a multicore fiber (MCF) commonly uses a spatial light modulator (SLM) to coherently combine, at the output of the MCF, few hundreds of beamlets into a focus spot. This spot is subsequently scanned across the sample to generate a fluorescent image. We propose here a novel scanning scheme, partial speckle scanning (PSS), inspired by compressive sensing theory, that avoids the use of an SLM to perform fluorescent imaging in LE with reduced acquisition time. Such a strategy avoids photo-bleaching while keeping high reconstruction quality. We develop our approach on two key properties of the LE: (i) the ability to easily generate speckles, and (ii) the memory effect in MCF that allows to use fast scan mirrors to shift light patterns. First, we show that speckles are sub-exponential random fields. Despite their granular structure, an appropriate choice of the reconstruction parameters makes them good candidates to build efficient sensing matrices. Then, we numerically validate our approach and apply it on experimental data. The proposed sensing technique outperforms conventional raster scanning: higher reconstruction quality is achieved with far fewer observations. For a fixed reconstruction quality, our speckle scanning approach is faster than compressive sensing schemes which require to change the speckle pattern for each observation.
