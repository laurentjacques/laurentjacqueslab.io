---
title: "Fast spin ±2 spherical harmonics transforms and application in cosmology"
author: ["Laurent Jacques"]
tags: [2, 2007, "bibtex"]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Yves Wiaux
- Laurent Jacques 
- Pierre Vandergeynst
tags: # One keywords per item "-" with format - '"keyword"'
- '"computational methods"'
- '"data analysis"'
- '"cosmology"'
- '"cosmic microwave background"'
- '"spherical harmonics"'
#categories: []
date: '2007-01-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
publication: '*Journal of Computational Physics*' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
links:
- name: DIAL
  url: http://hdl.handle.net/2078/30992
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/astro-ph/0508514
doi: https://dx.doi.org/10.1016/j.jcp.2007.07.005
---

**Abstract**: A fast and exact algorithm is developed for the spin \\(\pm2\\) spherical harmonics transforms on equi-angular pixelizations on the sphere. It is based on the Driscoll and Healy fast scalar spherical harmonics transform. The theoretical exactness of the transform relies on a sampling theorem. The associated asymptotic complexity is of order \\(O\big(L^2\\, (\log\_2 L)^{2}\big)\\), where \\(2L\\) stands for the square-root of the number of sampling points on the sphere, also setting a band limit L for the spin \\(\pm2\\) functions considered. The algorithm is presented as an alternative to existing fast algorithms with an asymptotic complexity of order \\(O(L^3)\\) on other pixelizations. We also illustrate these generic developments through their application in cosmology, for the analysis of the cosmic microwave background (CMB) polarization data.
