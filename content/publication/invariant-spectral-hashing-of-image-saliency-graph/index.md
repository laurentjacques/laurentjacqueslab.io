---
title: "Invariant Spectral Hashing of Image Saliency Graph"
author: ["Laurent Jacques"]
tags: [1, 2010]
draft: false
subtitle: ''
summary: ''
authors:
- Maxime Taquet
- Laurent Jacques
- Christophe De Vleeschouwer
- Benoît Macq
tags:
categories: []
date: '2010-01-01'
lastmod: 2021-08-06T23:54:38+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:38.380049Z'
publication_types:
- '1'
abstract: 
publication: '*Proc. of the 3rd International Workshop on Computational Topology in
  Image Context*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/88686
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1009.3029
---
