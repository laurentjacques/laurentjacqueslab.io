---
title: "Factorization over interpolation: A fast continuous orthogonal matching pursuit"
author: ["Laurent Jacques"]
tags: [1, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Gilles Monnoyer de Galland de Carnières
- Luc Vandendorpe
- Laurent Jacques
tags:
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:26+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:26.585453Z'
publication_types:
- '1'
abstract: 
publication: "*in Proceedings of iTWIST'20, Paper-ID: 45, Nantes, France, December,
   2-4, 2020*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242104
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2007.01060
---

**Abstract**: We propose a fast greedy algorithm to compute sparse representations of signals from continuous dictionaries that are factorizable, i.e., with atoms that can be separated as a product of sub-atoms. Existing algorithms strongly reduce the computational complexity of the sparse decomposition of signals in discrete factorizable dictionaries. On another flavour, existing greedy algorithms use interpolation strategies from a discretization of continuous dictionaries to perform off-the-grid decomposition. Our algorithm aims to combine the factorization and the interpolation concepts to enable low complexity computation of continuous sparse representation of signals. The efficiency of our algorithm is highlighted by simulations of its application to a radar system.
