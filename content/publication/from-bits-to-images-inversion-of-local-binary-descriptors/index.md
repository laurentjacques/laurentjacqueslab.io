---
title: "From bits to images: Inversion of local binary descriptors"
author: ["Laurent Jacques"]
tags: [2, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Emmanuel D'Angelo
- Pierre Vandergheynst
- Alexandre Alahi
- Laurent Jacques
tags: []
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:23+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:23.198784Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Pattern Analysis and Machine Intelligence*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/159549
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1211.1265 
doi: https://dx.doi.org/10.1109/TPAMI.2013.228
---

**Abstract**: Local Binary Descriptors are becoming more and more popular for image matching tasks, especially when going mobile. While they are extensively studied in this context, their ability to carry enough information in order to infer the original image is seldom addressed. In this work, we leverage an inverse problem approach to show that it is possible to directly reconstruct the image content from Local Binary Descriptors. This process relies on very broad assumptions besides the knowledge of the pattern of the descriptor at hand. This generalizes previous results that required either a prior learning database or non-binarized features. Furthermore, our reconstruction scheme reveals differences in the way different Local Binary Descriptors capture and encode image information. Hence, the potential applications of our work are multiple, ranging from privacy issues caused by eavesdropping image keypoints streamed by mobile devices to the design of better descriptors through the visualization and the analysis of their geometric content. © 2013 IEEE.
