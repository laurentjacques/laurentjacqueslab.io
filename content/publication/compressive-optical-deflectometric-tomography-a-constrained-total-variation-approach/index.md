---
title: "Compressive optical deflectometric tomography: a constrained total-variation approach"
author: ["Laurent Jacques"]
tags: [2, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Adriana Gonzalez Gonzalez
- Laurent Jacques
- Christophe De Vleeschouwer
- Philippe Antoine
tags:
- '"Optical Deflectometric Tomography"'
- '"Refractive index map"'
- '"Total Variation Minimization"'
- '"NFFT"'
- '"Primal-Dual Optimization"'
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:23+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.888202Z'
publication_types:
- '2'
abstract: 
publication: '*Inverse Problems and Imaging*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/143487
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1209.0654
doi: https://dx.doi.org/10.3934/ipi.2014.8.421
---

**Abstract**: Optical Deflectometric Tomography (ODT) provides an accurate characterization of transparent materials whose complex surfaces present a real challenge for manufacture and control. In ODT, the refractive index map (RIM) of a transparent object is reconstructed by measuring light deflection under multiple orientations. We show that this imaging modality can be made compressive, i.e., a correct RIM reconstruction is achievable with far less observations than required by traditional minimum energy (ME) or Filtered Back Projection (FBP) methods. Assuming a cartoon-shape RIM model, this reconstruction is driven by minimizing the map Total-Variation under a fidelity constraint with the available observations. Moreover, two other realistic assumptions are added to improve the stability of our approach: the map positivity and a frontier condition.  Numerically, our method relies on an accurate ODT sensing model and on a primal-dual minimization scheme, including easily the sensing operator and the proposed RIM constraints. We conclude this paper by demonstrating the power of our method on synthetic and experimental data under various compressive scenarios. In particular, the potential compressiveness of the stabilized ODT problem is demonstrated by observing a typical gain of 24dB compared to ME and of 30dB compared to FBP at only 5% of 360 incident light angles for moderately noisy sensing.
