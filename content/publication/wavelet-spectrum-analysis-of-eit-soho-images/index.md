---
title: "Wavelet spectrum analysis of EIT/SOHO images"
author: ["Laurent Jacques"]
tags: [2, 2005]
draft: false
subtitle: ''
summary: ''
authors:
- V Delouille
- J De Patoul
- JF Hochedez
- Laurent Jacques
- Jean-Pierre Antoine
tags:
categories: []
date: '2005-01-01'
lastmod: 2021-08-06T23:54:25+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:25.570015Z'
publication_types:
- '2'
abstract: 
publication: '*Solar Physics : a journal for solar and solar-stellar research and
  the study of solar terrestrial physics*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/39088
doi: https://dx.doi.org/10.1007/s11207-005-5620-3
---

**Abstract**: The extreme ultraviolet imaging telescope (EIT) of SOHO offers a unique record of the solar atmosphere for its sampling in temperature, field of view, resolution, duration, and cadence. To investigate globally and locally its topology and evolution during the solar cycle, we consider a multi-scale approach, and more precisely we use the wavelet spectrum. We present three results among the applications of such a procedure. First, we estimate the typical dimension of the supergranules as seen in the 30.4 nm passband, and we show that the evolution of the characteristic network scale is almost in phase with the solar cycle. Second, we build pertinent time series that give the evolution of the signal energy present in the corona at different scales. We propose a method that detects eruptions and post-flaring activity in EUV image sequences. Third, we introduce a new way to extract active regions in EIT images, with perspectives in, e.g., long-term irradiance analysis.
