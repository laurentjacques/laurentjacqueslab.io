---
title: "Sparse Support Recovery with $ell_{infty}$ Data Fidelity"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Kévin Degraux
- Gabriel Peyré
- Jalal M. Fadili
- Laurent Jacques
tags:
- '"convex optimization"'
- '"certificates"'
- '"sparsistency"'
- '"sparse signal recovery"'
- '"quantization"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:34+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:34.127999Z'
publication_types:
- '1'
abstract: 
publication: '"*Proceedings of the third international Traveling Workshop on Interactions between Sparse models and Technology (iTWIST''16), arXiv:1609.04167*"'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/176828
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1609.04167
---

**Abstract**: This paper investigates non-uniform guarantees of \\(ell\_1\\) minimization, subject to an \\(ell\_infty\\) data fidelity constraint, to stably recover the support of a sparse vector when solving noisy linear inverse problems. Our main contribution consists in giving a sufficient condition, framed in terms of the notion of dual certificates, to ensure that a solution of the \\(ell\_1-ell\_infty\\) convex program has a support containing that of the original vector, when the noise level is sufficiently small.
