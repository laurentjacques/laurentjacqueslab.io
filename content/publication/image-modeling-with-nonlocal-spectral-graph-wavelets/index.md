---
title: "Image modeling with nonlocal spectral graph wavelets"
author: ["Laurent Jacques"]
tags: [6, 2012]
draft: false
authors:
- David Hammond
- Laurent Jacques
- Pierre vandergheynst
tags:
- '"Graph wavelet"'
- '"Non-local image processing"'
- '"Denoising"'
date: '2012-01-01'
lastmod: 2021-08-06T23:54:42+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:42.763617Z'
publication_types:
- '6'
abstract: 
publication: 'Image Processing and Analysing With Graphs: Theory and Practice'
# custom links
links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/105886 #add dial handle or comment this out
#doi: https://dx.doi.org/ # add DOI id here
---

**Remark**: Chapter of the book "[Image Processing and Analysing With Graphs: Theory and Practice](http://www.crcpress.com/product/isbn/9781439855072)", Edited by O. Lézoray and L. Grady. (CRC Press, Taylor & Francis Group, In Press). ISBN:978-1-439-85507-2.
