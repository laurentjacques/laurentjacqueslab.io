---
title: "Quantitative characterization of biofunctionalization layers by robust image analysis for biosensor applications"
author: ["Laurent Jacques"]
tags: [2, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- Jonathan Rasson
- Numa Couniot
- Nancy Van Overstraeten
- Laurent Jacques
- Laurent Francis
- Denis Flandre
tags:
- '"Biosensors"'
- '"Biofunctionalization"'
- '"Surface chemistry"'
- '"Confocal fluorescence microscopy"'
- '"Image processing"'
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.756104Z'
publication_types:
- '2'
abstract: 
publication: '*Sensors and Actuators B: Chemical : international journal devoted to
  research and development of physical and chemical transducers*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/165420
doi: https://dx.doi.org/10.1016/j.snb.2015.09.028
---

**Abstract**: This work describes the development of a characterization method for biofunctionalized surfaces and its use for biosensor applications. The method is based on the processing of fluorescence images obtained by confocal microscopy. It retrieves quantitative data about the quantity and distribution of surface bound biomolecules, used to improve a biosensor performances in terms of specificity and sensitivity. Two biofunctionalization protocols were compared using the developed tools based on the fluorescence intensity, the surface coverage and the homogeneity of repartition of the fluorescence. In addition, the stability of the layer under flow, as well as the reproducibility and robustness of the protocols were evaluated.
