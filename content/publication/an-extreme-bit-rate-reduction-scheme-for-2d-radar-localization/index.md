---
title: "An extreme bit-rate reduction scheme for 2D radar localization"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Thomas Feuillen
- Luc Vandendorpe
- Laurent Jacques
tags:
- '"Radar"'
- '"compressive sensing"'
- '"quantization"'
- '"sparsity"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:29+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:29.753184Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of "International Traveling Workshop on Interactions Between
  Sparse Models and Technology"*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211764
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1812.05359
---

**Abstract**: In this paper, we further expand on the work in [1] that focused on the localization of targets in a 2D space using 1-bit dithered measurements coming from a 2 receiving antennae radar. Our aim is to further reduce the hardware requirements and bit-rate, by dropping one of the baseband IQ channel from each receiving antenna.  To that end, the structure of the received signals is exploited to recover the positions of multiple targets. Simulations are performed to highlight the accuracy and limitations of the proposed scheme under severe bit-rate reduction.
