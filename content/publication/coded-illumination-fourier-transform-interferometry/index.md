---
title: "Coded-Illumination Fourier Transform Interferometry"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Valerio Cambareri
- Kévin Degraux
- Adriana Cristina Gonzalez Gonzalez
- Matthieu Roblin
- Laurent Jacques
- Philippe Antoine
tags:
- '"Interferometry"'
- '"Inverse Problem"'
- '"Compressive Imaging"'
- '"Fourier Transform Interferometry"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.967685Z'
publication_types:
- '1'
abstract: 
publication: '*Golden Jubilee Meeting of the Royal Belgian Society for Microscopy
  (RBSM)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178328
---
