---
title: "Weighted fidelity in non-uniformly quantized compressed sensing"
author: ["Laurent Jacques"]
tags: [1, 2011]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- David Hammond
- Jalal Fadili
tags:
- '"Compressed Sensing"'
- '"Quantization"'
- '"Sparse signal representation"'
categories: []
date: '2011-01-01'
lastmod: 2021-08-06T23:54:38+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:37.854027Z'
publication_types:
- '1'
abstract: 
publication: '*International Conference on Image Processing. Proceedings*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105891
doi: https://dx.doi.org/10.1109/ICIP.2011.6115846
---
