---
title: "Image Deconvolution by Local Order Preservation of Pixels Values"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Stéphanie Guérit
- Laurent Jacques
- John Aldo Lee
tags:
- '"PET imaging"'
- '"deconvolution"'
- '"inverse problem"'
- '"local constraints"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:33.317628Z'
publication_types:
- '1'
abstract: 
publication: '*IEEE*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/175180
doi: https://dx.doi.org/10.1109/EUSIPCO.2016.7760307
---

**Abstract**: Positron emission tomography is more and more used in radiation oncology, since it conveys useful functional information about cancerous lesions. Its rather low spatial resolution, however, prevents accurate tumor delineation and heterogeneity assessment. Post-reconstruction deconvolution with the measured point-spread function can address this issue, provided it does not introduce undesired artifacts. These usually result from inappropriate regularization, which is either absent or making too strong assumptions about the structure of the signal. This paper proposes a deconvolution method that is based on inverse problem theory and involves a new regularization term that preserves local pixel value order relationships. Such regularization entails relatively mild constraints that are directly inferred from the observed data. This paper investigates the theoretical properties of the proposed regularization and describes its numerical implementation with a primal-dual algorithm. Preliminary experiments with synthetic images are presented to compare quantitatively and qualitatively the pro posed method to other regularization schemes, like TV and TGV.
