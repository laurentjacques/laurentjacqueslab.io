---
title: "The 2-D wavelet transform in image processing: Two novel applications"
author: ["Laurent Jacques"]
tags: [1, 2003]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
tags:
- '"Wavelet"'
- '"Image processing"'
- '"Angular multiselectivity"'
- '"Curvature estimation"'
categories: []
date: '2004-01-01'
lastmod: 2021-08-06T23:54:43+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:43.352990Z'
publication_types:
- '1'
abstract: 
publication: '*Contemporary  Problems in Mathematical Physics (Cotonou, Bénin, Nov. 2003)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105954
doi: https://doi.org/10.1142/9789812702487_0023
---

**Abstract**: In this paper, we describe two recent developments of the two-directional wavelet transform, namely, (i) the angular multiselectivity scheme and its application to image denoising, and (ii) a technique for measuring the curvature radius of a curve with application to astrophysical images. In both cases, the key is to use directional wavelets, with fixed or variable angular selectivity.
