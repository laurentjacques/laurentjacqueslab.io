---
title: "$(\\ell_{1},\\ell_{2})$-RIP and Projected Back-Projection Reconstruction for Phase-Only Measurements"
author: ["Laurent Jacques"]
tags: [2, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Thomas Feuillen
- Mike E. Davies
- Luc Vandendorpe
- Laurent Jacques
tags:
- '"Signal Processing"'
- '"Electrical and Electronic Engineering"'
- '"Applied Mathematics"'
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.486678Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Signal Processing Letters*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/242058
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1912.02880
doi: https://dx.doi.org/10.1109/lsp.2020.2973506
---

**Abstract**: This letter analyzes the performances of a simple reconstruction method, namely the Projected Back-Projection (PBP), for estimating the direction of a sparse signal from its phase-only (or amplitude-less) complex Gaussian random measurements, i.e., an extension of one-bit compressive sensing to the complex field. To study the performances of this algorithm, we show that complex Gaussian random matrices respect, with high probability, a variant of the Restricted Isometry Property (RIP) relating to the L1 -norm of the sparse signal measurements to their L2-norm. This property allows us to upper-bound the reconstruction error of PBP in the presence of phase noise. Monte Carlo simulations are performed to highlight the performance of our approach in this phase-only acquisition model when compared to error achieved by PBP in classical compressive sensing.
