---
title: "A Non-Convex Approach to Blind Calibration from Linear Sub-Gaussian Random Measurements"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Valerio Cambareri
- Laurent Jacques
tags:
- '"AlterSense"'
- '"blind-calibration"'
- '"non-convex optimization"'
- '"Wirtinger flow"'
- '"random sensing"'
- '"compressive sensing"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.381025Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of the "37th WIC Symposium on Information Theory in the
  Benelux, 6th joint WIC/IEEE SP Symposium on Information Theory and Signal Processing
  in the Benelux"*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178326
---

**Abstract**: Blind calibration is a bilinear inverse problem arising in modern sensing strategies, whose solution becomes crucial when traditional calibration aided by multiple, accurately designed training signals is either infeasible or resource-consuming.  We here address this problem for sensing schemes described by a linear model, in which the measured signal is projected on sub-Gaussian random sensing vectors each being affected by an unknown gain. By using multiple draws of the random sensing vectors we are able to solve this problem in its natural, non-convex form simply by projected gradient descent from a suitably chosen initialisation. Moreover, we obtain a sample complexity bound under which we are able to prove that this algorithm converges to the global optimum. Numerical evidence on the phase transition of this algorithm, as well as a practical computational sensing example support our theoretical findings.
