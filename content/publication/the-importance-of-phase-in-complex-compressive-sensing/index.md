---
title: "The Importance of Phase in Complex Compressive Sensing"
author: ["Laurent Jacques"]
tags: [2, 2021]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Thomas Feuillen
tags:
- '"Compressive sensing"'
- '"complex signals"'
- '"phase observations"'
- '"signal recovery"'
- '"inverse problem"'
- '"convex optimization"'
- '"compressive sensing"'
- '"complex signals"'
- '"phase observations"'
- '"signal recovery"'
- '"inverse problem"'
- '"convex optimization"'
- '"compressive sensing"'
- '"complex signals"'
- '"phase observations"'
- '"signal recovery"'
- '"inverse problem"'
- '"convex optimization"'
categories: []
date: '2021-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.335675Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/246584
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2001.02529
doi: https://dx.doi.org/10.1109/tit.2021.3073566
---

**Abstract**: We consider the question of estimating a real low-complexity signal (such as a sparse vector or a low-rank matrix) from the phase of complex random measurements.  We show that in this phase-only compressive sensing (PO-CS) scenario, we can perfectly recover such a signal with high probability and up to global unknown amplitude if the sensing matrix is a complex Gaussian random matrix and the number of measurements is large compared to the complexity level of the signal space. Our approach proceeds by recasting the (non-linear) PO-CS scheme as a linear compressive sensing model built from a signal normalization constraint, and a phase-consistency constraint imposing any signal estimate to match the observed phases in the measurement domain.  Practically, stable and robust estimation of the signal direction is achieved from any instance optimal algorithm of the compressive sensing literature (such as basis pursuit denoising). This is ensured by proving that the matrix associated with this equivalent linear model satisfies with high probability the restricted isometry property under the above condition on the number of measurements. We finally observe experimentally that robust signal direction recovery is reached at about twice the number of measurements needed for signal recovery in compressive sensing.
