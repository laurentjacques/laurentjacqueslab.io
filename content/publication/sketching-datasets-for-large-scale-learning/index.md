---
title: "Sketching Datasets for Large-Scale Learning"
author: ["Laurent Jacques"]
tags: [2, 2021, "bibtex"]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Rémi Gribonval
- Antoine Chatalic
- Nicolas Keriven
- Vincent Schellekens
- Laurent Jacques
- Philip Schniter
tags: # One keywords per item "-" with format - '"keyword"'
- '""'
#categories: []
date: '2021-09-01'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
#publication: '**' #publication: '*<journal-type>*'
url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
doi: '10.1109/MSP.2021.3092574'

# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2008.01839 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/250436 #add dial handle or comment this out
---

(for a longer and free version on arXiv see [here](https://arxiv.org/abs/2008.01839))

**Abstract**: This article considers "compressive learning," an approach to large-scale machine learning where datasets are massively compressed before learning (e.g., clustering, classification, or regression) is performed. In particular, a "sketch" is first constructed by computing carefully chosen nonlinear random features (e.g., random Fourier features) and averaging them over the whole dataset. Parameters are then learned from the sketch, without access to the original dataset. This article surveys the current state-of-the-art in compressive learning, including the main concepts and algorithms, their connections with established signal-processing methods, existing theoretical guarantees -- on both information preservation and privacy preservation, and important open problems.
