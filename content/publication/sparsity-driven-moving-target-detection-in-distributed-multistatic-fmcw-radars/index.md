---
title: "Sparsity-Driven Moving Target Detection in Distributed Multistatic FMCW Radars"
author: ["Laurent Jacques"]
tags: [1, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Gilles Monnoyer de Galland de Carnières
- Thomas Feuillen
- Laurent Jacques
- Luc Vandendorpe
tags:
- '"Radar"'
- '"sparsity"'
- '"FMCW"'
- '"matching pursuit"'
- '"factorization"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:29+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:29.069544Z'
publication_types:
- '1'
abstract: 
publication: '*in Proceedings of IEEE CAMSAP 2019, Le Gosier, France, December, 15-18,
  2019*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/243175
doi: https://dx.doi.org/10.1109/CAMSAP45676.2019.9022656
---

**Abstract**: We investigate the problem of sparse target detection from widely distributed multistatic textitFrequency Modulated Continuous Wave (FMCW) radar systems (using chirp modulation). Unlike previous strategies e.g., developed for FMCW or distributed multistatic radars), we propose a generic framework that scales well in terms of computational complexity for high-resolution space-velocity grid. Our approach assumes that the target signal is sparse in a discrete space-velocity domain, hence allowing for non-static target detection, and the resulting multiple baseband radar signals share a common support. By simplifying the representation of the FMCW radar signals, we propose a versatile scheme balancing complexity and detection accuracy. In particular, we design a low-complexity, factorized alternative for the Matching Pursuit algorithm leveraging this simplified model, as well as an iterative methodology to compensate for the errors caused by the model simplifications. Extensive Monte-Carlo simulations of a K-band radar system show that our method achieves a fast estimation of moving target's parameters on dense grids, with controllable accuracy, and reaching state-of-the-art performances compared to previous sparsity-driven approaches.
