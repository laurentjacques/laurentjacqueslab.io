---
title: "Breaking the waves: asymmetric random periodic features for low-bitrate kernel machines"
author: ["Laurent Jacques"]
tags: [2, 2021]
draft: false

subtitle: ''
summary: ''
authors:
- Vincent Schellekens
- Laurent Jacques
tags:
- '"random projections"'
- '"random Fourier featuress"'
- '"one-bit quantization"'
- '"random sketches"'
categories: []
date: '2021-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:18.895284Z'
publication_types:
- '2'
abstract: 
publication: '*Information and Inference: A Journal of the IMA*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/246236
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2004.06560
doi: https://dx.doi.org/10.1093/imaiai/iaab008
---

**Abstract**: Many signal processing and machine learning applications are built from evaluating a kernel on pairs of signals, e.g. to assess the similarity of an incoming query to a database of known signals. This nonlinear evaluation can be simplified to a linear inner product of the random Fourier features of those signals: random projections followed by a periodic map, the complex exponential. It is known that a simple quantization of those features (corresponding to replacing the complex exponential by a different periodic map that takes binary values, which is appealing for their transmission and storage), distorts the approximated kernel, which may be undesirable in practice. Our take-home message is that when the features of only one of the two signals are quantized, the original kernel is recovered without distortion; its practical interest appears in several cases where the kernel evaluations are asymmetric by nature, such as a client-server scheme. Concretely, we introduce the general framework of asymmetric random periodic features, where the two signals of interest are observed through random periodic features: random projections followed by a general periodic map, which is allowed to be different for both signals. We derive the influence of those periodic maps on the approximated kernel, and prove uniform probabilistic error bounds holding for all signal pairs from an infinite low-complexity set. Interestingly, our results allow the periodic maps to be discontinuous, thanks to a new mathematical tool, i.e. the mean Lipschitz smoothness. We then apply this generic framework to semi-quantized kernel machines (where only one signal has quantized features and the other has classical random Fourier features), for which we show theoretically that the approximated kernel remains unchanged (with the associated error bound), and confirm the power of the approach with numerical simulations.
