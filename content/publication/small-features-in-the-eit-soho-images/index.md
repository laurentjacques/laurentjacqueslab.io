---
title: "Small features in the EIT-SoHO images"
author: ["Laurent Jacques"]
tags: [1, 2002]
draft: false
subtitle: ''
summary: ''
authors:
- J-F. Hochedez
- Laurent Jacques
- A. Zhukov
- F. Clette
- Jean-Pierre Antoine
tags:
- '"Wavelet transform"'
- '"Sun corona"'
- '"Feature detection"'
- '"Image processing"'
categories: []
date: '2002-01-01'
lastmod: 2021-08-06T23:54:42+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:42.104105Z'
publication_types:
- '1'
abstract: 
publication: "*Proc. SOHO-11 Conference 'From Solar Min to Max: Half a Solar Cycle
   with SOHO' (March 2002, Davos, CH)*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105955
---
