---
title: "Consistent Basis Pursuit for Signal and Matrix Estimates in Quantized Compressed Sensing"
author: ["Laurent Jacques"]
tags: [2, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Laurent Jacques
- Valerio Cambareri
- Kévin Degraux
- Christophe De Vleeschouwer
tags:
- '"AlterSense"'
- '"Compressive Sensing"'
- '"Quantized Compressive Sensing"'
- '"Random Projections"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:21.765269Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Signal Processing Letters*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/167939
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1507.08268
- name: Demo
  icon_pack: ai
  url: DemoCoBP.html
url_code: CoBP_files.zip
doi: https://dx.doi.org/10.1109/LSP.2015.2497543
---

**Abstract**: This paper focuses on the estimation of low-complexity signals when they are observed through \\(M\\) uniformly quantized compressive observations. Among such signals, we consider 1-D sparse vectors, low-rank matrices, or compressible signals that are well approximated by one of these two models. In this context, we prove the estimation efficiency of a variant of Basis Pursuit Denoise, called Consistent Basis Pursuit (CoBP), enforcing consistency between the observations and the re-observed estimate, while promoting its low-complexity nature. We show that the reconstruction error of CoBP decays like \\(M^{-1/4}\\) when all parameters but \\(M\\) are fixed. Our proof is connected to recent bounds on the proximity of vectors or matrices when (i) those belong to a set of small intrinsic "dimension", as measured by the Gaussian mean width, and (ii) they share the same quantized (dithered) random projections. By solving CoBP with a proximal algorithm, we provide some extensive numerical observations that confirm the theoretical bound as \\(M\\) is increased, displaying even faster error decay than predicted. The same phenomenon is observed in the special, yet important case of 1-bit CS.
