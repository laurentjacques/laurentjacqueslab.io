---
title: "Sparsity-driven people localization algorithm: evaluation in crowded scenes environments"
author: ["Laurent Jacques"]
tags: [1, 2009]
draft: false
subtitle: ''
summary: ''
authors:
- A. Alahi
- Laurent Jacques
- Y. Boursier
- P. Vandergheynst
tags:
- '"sparse signal representation"'
- '"people localization"'
- '"Convex optimization"'
categories: []
date: '2009-01-01'
lastmod: 2021-08-06T23:54:40+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:39.973227Z'
publication_types:
- '1'
abstract: 
publication: '*2009 Twelfth IEEE International Workshop on Performance Evaluation
  of Tracking and Surveillance (PETS-Winter 2009)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/67514
doi: https://dx.doi.org/10.1109/PETS-WINTER.2009.5399487
---

**Abstract**: We propose to evaluate our sparsity driven people localization framework on crowded complex scenes. The problem is recast as a linear inverse problem. It relies on deducing an occupancy vector, i.e. the discretized occupancy of people on the ground, from the noisy binary silhouettes observed as foreground pixels in each camera. This inverse problem is regularized by imposing a sparse occupancy vector, i.e. made of few nonzero elements, while a particular dictionary of silhouettes linearly maps these non-empty grid locations to the multiple silhouettes viewed by the cameras network. The proposed approach is (i) generic to any scene of people, i.e. people are located in low and high density crowds, (ii) scalable to any number of cameras and already working with a single camera, (iii) unconstraint on the scene surface to be monitored. Qualitative and quantitative results are presented given the PETS 2009 dataset. The proposed algorithm detects people in high density crowd,

count and track them given severely degraded foreground silhouettes.
