---
title: "Asymmetric compressive learning guarantees with applications to quantized sketches"
author: ["Laurent Jacques"]
tags: [2, 2021, "bibtex"]
draft: false
authors: # One author per item "-"
- Vincent Schellekens
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"Compressive statistical learning"'
- '"asymmetric embeddings"'
- '"quantization"'
- '"Gaussian mixture modeling"'
date: '2022-03-24'  #format: 'yyyy-mm-dd'
# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
caption: ''
focal_point: ''
preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '2'
publication: '*IEEE Transactions on Signal Processing*'
url_pdf: 
url_preprint:
doi: 10.1109/TSP.2022.3157486
links:
- icon: arxiv
  icon_pack: ai
  name: arXiv
  url: https://arxiv.org/abs/2104.10061
- name: DIAL
  url: http://hdl.handle.net/2078.1/265613
---

**Abstract**: The compressive learning framework reduces the computational cost of training on large-scale datasets. In a sketching phase, the data is first compressed to a lightweight sketch vector, obtained by mapping the data samples through a well-chosen feature map, and averaging those contributions. In a learning phase, the desired model parameters are then extracted from this sketch by solving an optimization problem, which also involves a feature map. When the feature map is identical during the sketching and learning phases, formal statistical guarantees (excess risk bounds) have been proven. However, the desirable properties of the feature map are different during sketching and learning (e.g. quantized outputs, and differentiability, respectively). We thus study the relaxation where this map is allowed to be different for each phase. First, we prove that the existing guarantees carry over to this asymmetric scheme, up to a controlled error term, provided some Limited Projected Distortion (LPD) property holds. We then instantiate this framework to the setting of quantized sketches, by proving that the LPD indeed holds for binary sketch contributions. Finally, we further validate the approach with numerical simulations, including a large-scale application in audio event classification.
