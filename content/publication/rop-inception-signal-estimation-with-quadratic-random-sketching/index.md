---
title: "ROP inception: signal estimation with quadratic random sketching"
author: ["Laurent Jacques"]
tags: [1, 2022]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Rémi Delogne
- Vincent Schellekens
- Laurent Jacques
tags: # One keywords per item "-" with format - '"keyword"'
- '"rank-one projection"' 
- '"signal estimation"' 
- '"sketching techniques"' 
- '"optical processing unit"'
#categories: []
date: '2022-10-06'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false
# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
#abstract: ''
publication: '*European Symposium on Artificial Neural Networks, Computational Intelligence and Machine Learning, 2022, Bruges, Belgium*' #publication: '*<journal-type>*'
#url_pdf: ./ROPincept.pdf
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
#doi: ''
# custom links
links:
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/2205.08225 #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/265617 #add dial handle or comment this out
- name: PDF
  url: ROPincept.pdf
- name: Poster
  url: ESANN_ROPincept_2022.pdf
---

**Abstract**: Rank-one projections (ROP) of matrices and quadratic random sketching of signals support several data processing and machine learning methods, as well as recent imaging applications, such as phase retrieval or optical processing units. In this paper, we demonstrate how signal estimation can be operated directly through such quadratic sketches---equivalent to the ROPs of the "lifted signal" obtained as its outer product with itself---without explicitly reconstructing that signal. Our analysis relies on showing that, up to a minor debiasing trick, the ROP measurement operator satisfies a generalised sign product embedding (SPE) property. In a nutshell, the SPE shows that the scalar product of a signal sketch with the _sign_ of the sketch of a given pattern approximates the square of the projection of that signal on this pattern. This thus amounts to an insertion (an _inception_) of a ROP model inside a ROP sketch. The effectiveness of our approach is evaluated in several synthetic experiments.
