---
title: "On The Exact Recovery Condition of Simultaneous Orthogonal Matching Pursuit"
author: ["Laurent Jacques"]
tags: [2, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-François Determe
- Jérôme Louveaux
- Laurent Jacques
- François Horlin
tags:
- '"Compressed sensing"'
- '"exact recovery condition"'
- '"restricted isometry constant"'
- '"restricted isometry property"'
- '"restricted orthogonality constant"'
- '"simultaneous orthogonal matching pursuit"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.305341Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Signal Processing Letters*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/168615
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1508.04040
doi: https://dx.doi.org/10.1109/LSP.2015.2506989
---

**Abstract**: Several exact recovery criteria (ERC) ensuring that orthogonal matching pursuit (OMP) identifies the correct support of sparse signals have been developed in the last few years. These ERC rely on the restricted isometry property (RIP), the associated restricted isometry constant (RIC) and sometimes the restricted orthogonality constant (ROC). In this paper, three of the most recent ERC for OMP are examined.  The contribution is to show that these ERC remain valid for a generalization of OMP, entitled simultaneous orthogonal matching pursuit (SOMP), that is capable to process several measurement vectors simultaneously and return a common support estimate for the underlying sparse vectors. The sharpness of the bounds is also briefly discussed in light of previous works focusing on OMP.
