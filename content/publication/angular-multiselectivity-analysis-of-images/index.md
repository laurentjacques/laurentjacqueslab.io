---
title: "Angular multiselectivity analysis of images"
author: ["Laurent Jacques"]
tags: [1, 2003]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
tags: []
categories: []
date: '2003-01-01'
lastmod: 2021-08-06T23:54:41+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:41.370147Z'
publication_types:
- '1'
abstract: 
publication: '*SPIE - the International Society for Optical Engineering. Proceedings*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/68032
doi: https://dx.doi.org/10.1117/12.504800
---

**Abstract**: Two-dimensional wavelet analysis with directional frames is well-adapted and efficient for detecting oriented features in images but, for (quasi-)isotropic components, it is unnecessarily redundant. Using wavelets with variable angular selectivity leads to a prohibitive computing cost in the continuous wavelet formalism.  We propose a solution based on a multiresolution analysis in the angular variable (transferred from a biorthogonal analysis on the line), in addition to the usual multiresolution in scale. The resulting scheme is efficient and competitive with

traditional methods. Some applications are given to image denoising.
