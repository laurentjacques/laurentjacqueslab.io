---
title: "Compressive Imaging and Characterization of Sparse Light Deflection Maps"
author: ["Laurent Jacques"]
tags: [2, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- Prasad Sudhakara Murthy
- Laurent Jacques
- Xavier Dubois
- Philippe Antoine
- Luc Joannes
tags:
- '"deflectometry"'
- '"compressive imaging"'
- '"compressed sensing"'
- '"optical metrology"'
- '"proximal methods"'
- '"Chambolle--Pock   Read More: http://epubs.siam.org/doi/abs/10.1137/140974341?journalCode=sjisbi"'
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.610595Z'
publication_types:
- '2'
abstract: 
publication: '*SIAM Journal on Imaging Sciences*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/165737
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1406.6425
doi: https://dx.doi.org/10.1137/140974341
---

**Abstract**: Light rays incident on a transparent object of uniform refractive index undergo deflections, which uniquely characterize the surface geometry of the object.  Associated with each point on the surface is a deflection map (or spectrum) which describes the pattern of deflections in various directions. This article presents a novel method to efficiently acquire and reconstruct sparse deflection spectra induced by smooth object surfaces. To this end, we leverage the framework of compressed sensing (CS) in a particular implementation of a schlieren deflectometer, i.e., an optical system providing linear measurements of deflection spectra with programmable spatial light modulation patterns. In particular, we design those modulation patterns on the principle of spread spectrum CS for reducing the number of observations.  Interestingly, the ability of our device to simultaneously observe the deflection spectra on a dense discretization of the object surface is related to a particular multiple measurement vector model. This scheme allows us to estimate both the noise power and the instrumental point spread function in a specific calibration procedure.  We formulate the spectrum reconstruction task as the solving of a linear inverse problem regularized by an analysis sparsity prior which uses a translation invariant wavelet frame. Our results demonstrate the capability and advantages of using a CS-based approach for deflectometric imaging both on simulated data and experimental deflectometric data. Finally, the paper presents an extension of our method showing how we can extract the main deflection direction in each point of the object surface from a few compressive measurements, without needing any costly reconstruction procedures.  This compressive characterization is then confirmed with experimental results on simple plano-convex and multifocal intraocular lenses studying the evolution of the main deflection as a function of the object point location.
