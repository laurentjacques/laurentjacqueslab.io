---
title: "A non-convex blind calibration method for randomised sensing strategies"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
authors:
- Valerio Cambareri
- Laurent Jacques
tags:
- '"AlterSense"'
- '"bilinear inverse problems"'
- '"Blind calibration"'
- '"non-convex optimisation"'
- '"sample complexity"'
- '"computational sensing"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false
# Projects (optional).
# Associate this post with one or more of your projects.
# Simply enter your project's folder or file name without extension.
# E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
# Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.558522Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of International Workshop on "Compressed Sensing Theory and its Applications to Radar, Sonar and Remote Sensing (CoSeRa)", 2016*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178303
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1605.02615
doi: https://dx.doi.org/10.1109/CoSeRa.2016.7745690
---

**Abstract**: The implementation of computational sensing strategies often faces calibration problems typically solved by means of multiple, accurately chosen training signals, an approach that can be resource-consuming and cumbersome. Conversely, blind calibration does not require any training, but corresponds to a bilinear inverse problem whose algorithmic solution is an open issue. We here address blind calibration as a non-convex problem for linear random sensing models, in which we aim to recover an unknown signal from its projections on sub-Gaussian random vectors each subject to an unknown multiplicative factor (gain). To solve this optimisation problem we resort to projected gradient descent starting from a suitable initialisation. An analysis of this algorithm allows us to show that it converges to the global optimum provided a sample complexity requirement is met, i.e., relating convergence to the amount of information collected during the sensing process. Finally, we present some numerical experiments in which our algorithm allows for a simple solution to blind calibration of sensor gains in computational sensing applications.
