---
title: "Robust phase unwrapping by convex optimization"
author: ["Laurent Jacques"]
tags: [1, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Adriana Gonzalez Gonzalez
- Laurent Jacques
tags:
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:35+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:35.411349Z'
publication_types:
- '1'
abstract: 
publication: '*Image Processing (ICIP), 2014 IEEE International Conference on*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/167962
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1407.8040
doi: https://dx.doi.org/10.1109/ICIP.2014.7025343
---

**Abstract**: The 2-D phase unwrapping problem aims at retrieving a “phase” image from its modulo 2π observations. Many applications, such as interferometry or synthetic aperture radar imaging, are concerned by this problem since they proceed by recording complex or modulated data from which a “wrapped” phase is extracted. Although 1-D phase unwrapping is trivial, a challenge remains in higher dimensions to overcome two common problems: noise and discontinuities in the true phase image. In contrast to state-of-the-art techniques, this work aims at simultaneously unwrap and denoise the phase image. We propose a robust convex optimization approach that enforces data fidelity constraints expressed in the corrupted phase derivative domain while promoting a sparse phase prior. The resulting optimization problem is solved by the Chambolle-Pock primal-dual scheme. We show that under different observation noise levels, our approach compares favorably to those that perform the unwrapping and denoising in two separate steps.
