---
title: "The Separation Capacity of Random Neural Networks"
author: ["Laurent Jacques"]
tags: [1, 2021]
draft: false
#subtitle: ''
#summary: ''
authors: # One author per item "-"
- Sjoerd Dirksen
- Martin Genzel
- Laurent Jacques
- Alexander Stollenwerk
tags: # One keywords per item "-" with format - '"keyword"'
- '"random neural network"'
- '"measure concentration"'
- '"separation capacity"'
#categories: []
date: '2021-11-04'  #format: 'yyyy-mm-dd'
#featured: false
#draft: false

# Featured image
# To use, add an image named YY to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
#image:
#caption: ''
#focal_point: ''
#preview_only: false
# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. YY references YY.
#   Otherwise, set Y.
#projects: []
publication_types:
#  - '0' = Uncategorized
#  - '1' = Conference paper
#  - '2' = Journal article
#  - '3' = Preprint / Working Paper
#  - '4' = Report
#  - '5' = Book
#  - '6' = Book section
#  - '7' = Thesis (v4.2+ required)
#  - '8' = Patent (v4.2+ required)
- '1'
#abstract: ''
publication: '*2021 Online International Conference on Computational Harmonic Analysis (Online-ICCHA2021).*' #publication: '*<journal-type>*'
#url_pdf:
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'
#doi: ''

# custom links
links:
#- icon: arxiv
#  name: arXiv
#  icon_pack: ai
#  url: https://arxiv.org/abs/ #add arxiv identifier here
- name: DIAL
  url: http://hdl.handle.net/2078.1/265615 #add dial handle or comment this out
---

**Abstract**: Neural networks (NNs) with random weights appear in a variety of machine learning applications, perhaps most prominently as initialization of many deep learning algorithms. We take one step closer to their theoretical foundation by addressing the following data separation problem: Under what conditions can a random NN make two classes \\(\mathcal X^{-}, \mathcal X^{\plus} \subset \mathbb R^{d}\\) (with positive distance) linearly separable? We show that a two-layer ReLU-network with standard Gaussian weights and uniformly distributed biases can solve this problem with high probability. Crucially, the number of required neurons is explicitly linked to geometric properties of the underlying sets \\(\mathcal X^{-}\\) and \\(\mathcal X^{\plus}\\). This instance-specific viewpoint allows us to overcome the usual curse of dimensionality (exponential width of the layers) in non-pathological situations where the data carries low-complexity structure. The key ingredient to make this intuition precise is a geometric notion of complexity (based on the Gaussian mean width), which leads to sound and informative separation guarantees. On a technical level, our proof strategy departs from the standard statistical learning approach, and instead relies on tools from convex geometry and high-dimensional probability. Besides the mathe- matical results, we also discuss several connections to machine learning, such as memorization, generalization, and adversarial robustness.

The related abstract is a very condensed overview of our recent findings [provided in this preprint](https://arxiv.org/abs/2108.00207). For more details, possible refinements, related literature, and proofs, we refer to this article.
