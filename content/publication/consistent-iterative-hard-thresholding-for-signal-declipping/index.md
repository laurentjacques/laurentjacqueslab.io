---
title: "Consistent Iterative Hard Thresholding for Signal Declipping"
author: ["Laurent Jacques"]
tags: [1, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Srdjan Kitic
- Laurent Jacques
- Nilesh Madhu
- Michael Peter Hopwood
- Ann Spriet
- Christophe De Vleeschouwer
tags:
- '"signal declipping"'
- '"inverse problem"'
- '"audio restoration"'
- '"iterative hard thresholding"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:36+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:36.123367Z'
publication_types:
- '1'
abstract: 
publication: '*Acoustics, Speech and Signal Processing (ICASSP), 2013 IEEE International
  Conference on*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134470
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1303.1023
doi: https://dx.doi.org/10.1109/ICASSP.2013.6638804
---

**Abstract**: Clipping or saturation in audio signals is a very common problem in signal processing, for which, in the severe case, there is still no satisfactory solution.  In such case, there is a tremendous loss of information, and traditional methods fail to appropriately recover the signal. We propose a novel approach for this signal restoration problem based on the framework of Iterative Hard Thresholding. This approach, which enforces the consistency of the reconstructed signal with the clipped observations, shows superior performance in comparison to the state-of-the-art declipping algorithms. This is confirmed on synthetic and on actual high-dimensional audio data processing, both on SNR and on subjective user listening evaluations.
