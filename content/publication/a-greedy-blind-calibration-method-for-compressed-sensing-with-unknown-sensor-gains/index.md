---
title: "A greedy blind calibration method for compressed sensing with unknown sensor gains"
author: ["Laurent Jacques"]
tags: [1, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Valerio Cambareri
- Amirafshar Moshtaghpour
- Laurent Jacques
tags:
- '"Calibration"'
- '"Blind Calibration"'
- '"Sensors"'
- '"Iterative Hard Thresholding"'
- '"Non-Convex Optimisation"'
- '"Bilinear Inverse Problems"'
- '"Information theory"'
- '"Complexity theory"'
- '"Imaging"'
- '"Compressed sensing"'
- '"Mathematical model"'
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:31+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:31.216382Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of IEEE International Symposium on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/187185
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1610.02851
doi: https://dx.doi.org/10.1109/ISIT.2017.8006705
---

**Abstract**: The realisation of sensing modalities based on the principles of compressed sensing is often hindered by discrepancies between the mathematical model of its sensing operator, which is necessary during signal recovery, and its actual physical implementation, which can amply differ from the assumed model. In this paper we tackle the bilinear inverse problem of recovering a sparse input signal and some unknown, unstructured multiplicative factors affecting the sensors that capture each compressive measurement. Our methodology relies on collecting a few snapshots under new draws of the sensing operator, and applying a greedy algorithm based on projected gradient descent and the principles of iterative hard thresholding. We explore empirically the sample complexity requirements of this algorithm by testing its phase transition, and show in a practically relevant instance of this problem for compressive imaging that the exact solution can be obtained with only a few snapshots.
