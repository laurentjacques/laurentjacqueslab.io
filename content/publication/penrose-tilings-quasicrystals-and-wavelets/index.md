---
title: "Penrose tilings, quasicrystals, and wavelets"
author: ["Laurent Jacques"]
tags: [1, 1999]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
- Pierre Vandergheynst
tags:
- '"Wavelets"'
- '"Quasicrystals"'
- '"Symmetries"'
- '"Image Processing"'
categories: []
date: '1999-01-01'
lastmod: 2021-08-06T23:54:42+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:42.315630Z'
publication_types:
- '1'
abstract: 
publication: '*Proc. SPIE 3813, Wavelet Applications in Signal and Image Processing
  VII*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105958
doi: https://dx.doi.org/10.1117/12.366795
---
