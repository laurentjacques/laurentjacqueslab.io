---
title: "Improving the Correlation Lower Bound for Simultaneous Orthogonal Matching Pursuit"
author: ["Laurent Jacques"]
tags: [2, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Francois Determe
- Jérôme Louveaux
- Laurent Jacques
- Francois Horlin
tags:
- '"noisy analysis"'
- '"correlation lower bound improvement"'
- '"simultaneous orthogonal matching pursuit algorithm"'
- '"SOMP algorithm"'
- '"sparse signal acquisition"'
- '"multiple measurement vector model"'
- '"suitable dictionary"'
- '"current signal residual"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:22+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:22.042457Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Signal Processing Letters*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178294
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1608.08454
doi: https://dx.doi.org/10.1109/LSP.2016.2612759
---

**Abstract**: The simultaneous orthogonal matching pursuit (SOMP) algorithm aims to find the joint support of a set of sparse signals acquired under a multiple measurement vector model. Critically, the analysis of SOMP depends on the maximal inner product of any atom of a suitable dictionary and the current signal residual, which is formed by the subtraction of previously selected atoms. This inner product, or correlation, is a key metric to determine the best atom to pick at each iteration. This letter provides, for each iteration of SOMP, a novel lower bound of the aforementioned metric for the atoms belonging to the correct and common joint support of the multiple signals. Although the bound is obtained for the noiseless case, its main purpose is to intervene in noisy analyses of SOMP. Finally, it is shown for specific signal patterns that the proposed bound outperforms state-of-the-art results for SOMP and orthogonal matching pursuit (OMP) as a special case.
