---
title: "Taking the edge off quantization: projected back projection in dithered compressive sensing"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Chunlei Xu
- Vincent Schellekens
- Laurent Jacques
tags:
- '"Quantized compressive sensing"'
- '"scalar uniform quantization"'
- '"uniform dithering"'
- '"projected back projection"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:31+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:30.895705Z'
publication_types:
- '1'
abstract: 
publication: '*IEEE*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/209869
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1805.04348
doi: https://dx.doi.org/10.1109/SSP.2018.8450784
---

**Abstract**: Quantized compressive sensing (QCS) deals with the problem of representing compressive signal measurements with finite precision representation, i.e., a mandatory process in any practical sensor design. To characterize the signal reconstruction quality in this framework, most of the existing theoretical analyses lie heavily on the quantization of sub-Gaussian random projections (e.g., Gaussian or Bernoulli).  We show here that a simple uniform scalar quantizer is compatible with a large class of random sensing matrices known to respect, with high probability, the restricted isometry property (RIP). Critically, this compatibility arises from the addition of a uniform random vector, or "dithering", to the linear signal observations before quantization. In this setting, we prove the existence of (at least) one signal reconstruction method, i.e., the projected back projection (PBP), whose reconstruction error decays when the number of quantized measurements increases. This holds with high probability in the estimation of sparse signals and low-rank matrices. We validate numerically the predicted error decay as the number of measurements increases.
