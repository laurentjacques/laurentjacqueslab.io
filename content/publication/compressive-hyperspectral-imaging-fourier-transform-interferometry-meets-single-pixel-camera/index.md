---
title: "Compressive hyperspectral imaging: Fourier transform interferometry meets single pixel camera"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- José M Bioucas-Dias
- Laurent Jacques
tags:
- '"Hyperspectral"'
- '"Fourier transform interferometry"'
- '"Single pixel imaging"'
- '"Compressive sensing"'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:30+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:30.283959Z'
publication_types:
- '1'
abstract: 
publication: "*iTWIST'18*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211197
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1809.00950
---

**Abstract**: This paper introduces a single-pixel HyperSpectral (HS) imaging framework based on Fourier Transform Interferometry (FTI). By combining a space-time coding of the light illumination with partial interferometric observations of a collimated light beam (observed by a single pixel), our system benefits from (i) reduced measurement rate and light-exposure of the observed object compared to common (Nyquist) FTI imagers, and (ii) high spectral resolution as desirable in, eg, Fluorescence Spectroscopy (FS). From the principles of compressive sensing with multilevel sampling, our method leverages the sparsity" in level" of FS data, both in the spectral and the spatial domains. This allows us to optimize the space-time light coding using time-modulated Hadamard patterns. We confirm the effectiveness of our approach by a few numerical experiments.
