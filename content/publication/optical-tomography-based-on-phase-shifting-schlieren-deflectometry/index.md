---
title: "Optical tomography based on phase-shifting schlieren deflectometry"
author: ["Laurent Jacques"]
tags: [1, 2010]
draft: false
subtitle: ''
summary: ''
authors:
- Emmanuel Foumouo
- J.-L. Dewandel
- L. Joannes
- D. Beghuin
- Laurent Jacques
- Philippe Antoine
tags:
- '"IMCN:NAPS"'
- '"optical tomography"'
- '"deflectometry"'
- '"inverse problem"'
categories: []
date: '2010-01-01'
lastmod: 2021-08-06T23:54:38+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:38.577283Z'
publication_types:
- '1'
abstract: 
publication: '*Optical Sensing and Detection*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/67400
doi: https://dx.doi.org/10.1117/12.854063
---

**Abstract**: We present a new optical tomography technique based on phase-shifting schlieren deflectometry. The principle is that of computerized tomography. The three-dimensional profile is reconstructed from the deflection angles of rays passing through the tested object. We have investigated optical phantoms chosen in view of the characterization of dendritic growth in a solidification process. Promising results have been obtained with a homogeneous sphere and a bundle of 200 mu m fibers. The deviation angles

exceed two degrees with a variation of the refractive index Delta n = 0.025.
