---
title: "Heterogenous void growth revealed by in situ 3-D X-ray mocrotomography using automatic cavity tracking"
author: ["Laurent Jacques"]
tags: [2, 2014]
draft: false
subtitle: ''
summary: ''
authors:
- Liza Lecarme
- Eric Maire
- Amit Kumar K.C.
- Christophe De Vleeschouwer
- Laurent Jacques
- Aude Simar
- Thomas Pardoen
tags:
- '"CISM:CECI"'
categories: []
date: '2014-01-01'
lastmod: 2021-08-06T23:54:23+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:23.334848Z'
publication_types:
- '2'
abstract: 
publication: '*Acta Materialia*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/135237
doi: https://dx.doi.org/10.1016/j.actamat.2013.10.014
---

**Abstract**: Ductile fracture by nucleation, growth and coalescence of internal voids is the dominant fracture mechanism in metals at ambient temperature. Micromechanics-based models for each elementary mechanism have been developed and enhanced over the past 40 years, allowing microstructure-informed failure predictions essentially assuming homogeneous damage evolution. In situ 3-D microtomography has been instrumental to assess these models from experimental evolution of average void density and size.  Nevertheless, statistical effects on the damage evolution associated with microstructure distribution heterogeneities have not yet been addressed quantitatively due to the difficulty in tracking individual cavities. Here, we show, from 3-D in situ microtomography characterization of a Ti–6Al–4V alloy, factor 4 variations among the growth rate of individual cavities undergoing the same stress triaxiality and same plastic deformation.  This statistical analysis has been made possible owing to an advanced tracking algorithm relying on a graph-based data association approach initially developed for the field of computer vision to track target motions. The measured variations originate from void shape and crystal orientation effects, as well as from local constraints changes due to the presence of two phases with different strengths.
