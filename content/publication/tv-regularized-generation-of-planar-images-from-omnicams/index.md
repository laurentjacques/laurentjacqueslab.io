---
title: "TV-Regularized Generation of Planar Images from Omnicams"
author: ["Laurent Jacques"]
tags: [1, 2009]
draft: false
subtitle: ''
summary: ''
authors:
- Yannick Boursier
- Laurent Jacques
- Didier Raboud
- Pascal Frossard
- Jalal Fadili
- ïerre Vandergheynst
tags:
- '"Computer Vision"'
- '"Omnidirectional camera"'
- '"Total Variation"'
- '"Inpainting"'
categories: []
date: '2009-01-01'
lastmod: 2021-08-06T23:54:40+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:40.410740Z'
publication_types:
- '1'
abstract: 
publication: '*Image Processing (ICIP), 2009 16th IEEE International Conference on*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105934
doi: https://dx.doi.org/10.1109/ICIP.2009.5413712
---
