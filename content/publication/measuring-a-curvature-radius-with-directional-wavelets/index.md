---
title: "Measuring a curvature radius with directional wavelets"
author: ["Laurent Jacques"]
tags: [1, 2003]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
tags: []
categories: []
date: '2003-01-01'
lastmod: 2021-08-06T23:54:41+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:41.749676Z'
publication_types:
- '1'
abstract: 
publication: '*GROUP 24: Physical and Mathematical Aspects of Symmetries.*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/61238
---

**Abstract**: We present in this paper a new technique based on the Continuous Wavelet Transfrom (CWT) for estimating the curvature radius of contours delimiting simple objects in images. This method exploits the link which exists between the angular

response of directional wavelets and the curvature radius of the analyzed curves.
