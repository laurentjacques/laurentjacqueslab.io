---
title: "Close Encounters of the Binary Kind: Signal Reconstruction Guarantees for Compressive Hadamard Sampling with Haar Wavelet Basis"
author: ["Laurent Jacques"]
tags: [2, 2020]
draft: false
subtitle: ''
summary: ''
authors:
- Amirafshar Moshtaghpour
- Jose M. Bioucas-Dias
- Laurent Jacques
tags:
- '"Hadamard transform"'
- '"Haar wavelet"'
- '"variable density sampling"'
- '"compressive sensing"'
- '"Hadamard transform"'
- '"Haar wavelet"'
- '"variable density sampling"'
- '"compressive sensing"'
categories: []
date: '2020-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.644613Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/229736
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1907.09795
doi: https://dx.doi.org/10.1109/tit.2020.2992852
---

**Abstract**: We investigate the problems of 1-D and 2-D signal recovery from subsampled Hadamard measurements using Haar wavelet as a sparsity inducing prior. These problems are of interest in, e.g., computational imaging applications relying on optical multiplexing or single-pixel imaging. However, the realization of such modalities is often hindered by the coherence between the Hadamard and Haar bases. The variable and multilevel density sampling strategies solve this issue by adjusting the subsampling process to the local and multilevel coherence, respectively, between the two bases; hence enabling successful signal recovery. In this work, we compute an explicit sample-complexity bound for Hadamard-Haar systems as well as uniform and non-uniform recovery guarantees; a seemingly missing result in the related literature. We explore the faithfulness of the numerical simulations to the theoretical results and show in a practically relevant instance, e.g., single-pixel camera, that the target signal can be recovered from a few Hadamard measurements.
