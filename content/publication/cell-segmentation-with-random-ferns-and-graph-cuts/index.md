---
title: "Cell segmentation with random ferns and graph-cuts"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Arnaud Browet
- Christophe De Vleeschouwer
- Laurent Jacques
- Navrita Mathiah
- Bechara Saykali
- Isabelle Migeotte
tags:
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:32+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:32.843472Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of IEEE International Conference on Image Processing*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/175857
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1602.05439
doi: https://dx.doi.org/10.1109/ICIP.2016.7533140
---

**Abstract**: The progress in imaging techniques have allowed the study of various aspect of cellular mechanisms. To isolate individual cells in live imaging data, we introduce an elegant image segmentation framework that effectively extracts cell boundaries, even in the presence of poor edge details. Our approach works in two stages. First, we estimate pixel interior/border/exterior class probabilities using random ferns.  Then, we use an energy minimization framework to compute boundaries whose localization is compliant with the pixel class probabilities. We validate our approach on a manually annotated dataset.
