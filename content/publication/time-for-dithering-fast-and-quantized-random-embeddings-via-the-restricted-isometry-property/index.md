---
title: "Time for dithering: fast and quantized random embeddings via the restricted isometry property"
author: ["Laurent Jacques"]
tags: [2, 2017]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- Valerio Cambareri
tags:
- '"random projections"'
- '"quantized rank-one projections"'
- '"nonlinear embeddings"'
- '"quantization"'
- '"dither"'
- '"restricted isometry property"'
- '"dimensionality reduction"'
- '"compressive sensing"'
- '"low-complexity signal models"'
- '"fast and structured sensing matrices"'
categories: []
date: '2017-01-01'
lastmod: 2021-08-06T23:54:21+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:21.630186Z'
publication_types:
- '2'
abstract: 
publication: '*Information and Inference*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/187184
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1607.00816
doi: https://dx.doi.org/10.1093/imaiai/iax004
---

**Abstract**: Recently, many works have focused on the characterization of non-linear dimensionality reduction methods obtained by quantizing linear embeddings, e.g., to reach fast processing time, efficient data compression procedures, novel geometry-preserving embeddings or to estimate the information/bits stored in this reduced data representation.  In this work, we prove that many linear maps known to respect the restricted isometry property (RIP) can induce a quantized random embedding with controllable multiplicative and additive distortions with respect to the pairwise distances of the data points beings considered. In other words, linear matrices having fast matrix-vector multiplication algorithms (e.g., based on partial Fourier ensembles or on the adjacency matrix of unbalanced expanders) can be readily used in the definition of fast quantized embeddings with small distortions. This implication is made possible by applying right after the linear map an additive and random "dither" that stabilizes the impact of the uniform scalar quantization operator applied afterwards. For different categories of RIP matrices, i.e., for different linear embeddings of a metric space \\((mathcal K ⊂ mathbb R^n, ell\_q)\\) in \\((mathbb R^m, ell\_p)\\) with \\(p,q ≥ 1\\), we derive upper bounds on the additive distortion induced by quantization, showing that it decays either when the embedding dimension \\(m\\) increases or when the distance of a pair of embedded vectors in \\(mathcal K\\) decreases. Finally, we develop a novel "bi-dithered" quantization scheme, which allows for a reduced distortion that decreases when the embedding dimension grows and independently of the considered pair of vectors.
