---
title: "Jointly low-rank and bisparse recovery: Questions and partial answers"
author: ["Laurent Jacques"]
tags: [2, 2019]
draft: false
subtitle: ''
summary: ''
authors:
- Simon Foucart
- Rémi Gribonval
- Laurent Jacques
- Holger Rauhut
tags:
- '"Applied Mathematics"'
- '"Analysis"'
- '"Compressive sensing"'
- '"simultaneity of structures"'
- '"rank-one measurements"'
- '"sample complexity"'
- '"restricted isometry properties"'
- '"iterative thresholding algorithms"'
- '"head and tail projections"'
categories: []
date: '2019-01-01'
lastmod: 2021-08-06T23:54:20+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:20.114335Z'
publication_types:
- '2'
abstract: 
publication: '*Analysis and Applications*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/223571
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1902.04731
doi: https://dx.doi.org/10.1142/s0219530519410094
---

**Abstract**: We investigate the problem of recovering jointly \\(r\\)-rank and \\(s\\)-bisparse matrices from as few linear measurements as possible, considering arbitrary measurements as well as rank-one measurements. In both cases, we show that \\(m \asymp r s \ln(en/s)\\) measurements make the recovery possible in theory, meaning via a nonpractical algorithm. In case of arbitrary measurements, we investigate the possibility of achieving practical recovery via an iterative-hard-thresholding algorithm when \\(m \asymp r s^\gamma \ln(en/s)\\) for some exponent \\(\gamma > 0\\). We show that this is feasible for \\(\gamma = 2\\), and that the proposed analysis cannot cover the case \\(\gamma \leq 1\\). The precise value of the optimal exponent \\(\gamma \in [1,2]\\) is the object of a question, raised but unresolved in this paper, about head projections for the jointly low-rank and bisparse structure. Some related questions are partially answered in passing. For rank-one measurements, we suggest on arcane grounds an iterative-hard-thresholding algorithm modified to exploit the nonstandard restricted isometry property obeyed by this type of measurements.
