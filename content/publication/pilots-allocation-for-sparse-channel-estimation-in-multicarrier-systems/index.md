---
title: "Pilots allocation for sparse channel estimation in multicarrier systems"
author: ["Laurent Jacques"]
tags: [1, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- François Rottenberg
- Kévin Degraux
- Laurent Jacques
- François Horlin
- Jérôme Louveaux
tags: []
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:34+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:34.664833Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of the Symposium on Information Theory in the Benelux,
  (Brussels, Belgium)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/168001
---
