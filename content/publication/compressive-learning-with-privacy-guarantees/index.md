---
title: "Compressive learning with privacy guarantees"
author: ["Laurent Jacques"]
tags: [2, 2021]
draft: false
subtitle: ''
summary: ''
authors:
- Antoine Chatalic
- Vincent Schellekens
- Florimon Houssiau
- Yves-Alexandre de Montjoye
- Laurent Jacques
- Rémi Gribonval
tags:
- '"compressive learning"'
- '"privacy"'
- '"statistical learning"'
- '"inverse problems"'
- '"differential privacy"'
categories: []
date: '2021-01-01'
lastmod: 2021-08-06T23:54:19+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:19.056270Z'
publication_types:
- '2'
abstract: 
publication: '*Information and Inference: A Journal of the IMA*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/246235
- name: HAL
  icon: hal
  icon_pack: ai
  url: https://hal.inria.fr/hal-02496896
doi: https://dx.doi.org/10.1093/imaiai/iaab005
---

**Abstract**: This work addresses the problem of learning from large collections of data with privacy guarantees. The compressive learning framework proposes to deal with the large scale of datasets by compressing them into a single vector of generalized random moments, called a sketch vector, from which the learning task is then performed.  We provide sharp bounds on the so-called sensitivity of this sketching mechanism.  This allows us to leverage standard techniques to ensure differential privacy—a well-established formalism for defining and quantifying the privacy of a random mechanism—by adding Laplace of Gaussian noise to the sketch. We combine these standard mechanisms with a new feature subsampling mechanism, which reduces the computational cost without damaging privacy. The overall framework is applied to the tasks of Gaussian modeling, k-means clustering and principal component analysis, for which sharp privacy bounds are derived. Empirically, the quality (for subsequent learning) of the compressed representation produced by our mechanism is strongly related with the induced noise level, for which we give analytical expressions.
