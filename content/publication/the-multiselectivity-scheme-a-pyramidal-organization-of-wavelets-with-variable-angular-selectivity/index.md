---
title: "The multiselectivity scheme: A pyramidal organization of wavelets with variable  angular selectivity"
author: ["Laurent Jacques"]
tags: [1, 2004]
draft: false
subtitle: ''
summary: ''
authors:
- Jean-Pierre Antoine
- Laurent Jacques
tags:
- '"Directional Wavelet"'
- '"Image Processing"'
- '"Multiselectivity"'
categories: []
date: '2004-01-01'
lastmod: 2021-08-06T23:54:41+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:40.951023Z'
publication_types:
- '1'
abstract: 
publication: "*Proc. EUSIPCO'04, Vienna, 2004*"
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105944
---
