---
title: "A Low-Memory Compressive Image Sensor Architecture for Embedded Object Recognition"
author: ["Laurent Jacques"]
tags: [1, 2018]
draft: false
subtitle: ''
summary: ''
authors:
- Wissam Benjilali
- William Guicquero
- Gilles Sicard
- Laurent Jacques
tags:
- '"Embedded Object Recognition"'
- '"Image Sensor"'
- '"Compressive Sensing"'
- '"Support Vector Machine"'
- '"Sigma-Delta ADC"'
- '"DSP."'
categories: []
date: '2018-01-01'
lastmod: 2021-08-06T23:54:29+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:29.559473Z'
publication_types:
- '1'
abstract: 
publication: '*Proceedings of MWSCAS 2018*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/211768
---

**Abstract**: This work presents a compact image sensor architecture with end-of-column digital processing dedicated to perform embedded object recognition. The architecture takes advantage of a Compressed Sensing (CS) scheme to extract compressed features and to reduce data dimensionality based on a low footprint pseudo random data mixing.  Taking advantage of the intrinsic property of a first order incremental Sigma-Delta (Sigma Delta) Analog to Digital Converter (ADC), an optimized Digital Signal Processing (DSP) is proposed to implement the affine projection applied by a linear Support Vector Machine (SVM) classifier. This architecture allows to achieve an acceptable object recognition accuracy of 80% on the Georgia Tech face database (50 classes).  On the other hand, the signal independent dimensionality reduction performed by our dedicated sensing scheme (1/512) allows to dramatically reduce memory requirements (125 kbits) related -in our case- to the ex-situ learned affine function of the linear SVM.
