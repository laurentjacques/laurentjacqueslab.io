---
title: "Interpolation on manifolds using Bézier functions"
author: ["Laurent Jacques"]
tags: [1, 2016]
draft: false
subtitle: ''
summary: ''
authors:
- Pierre-Yves Gousenbourger
- Pierre-Antoine Absil
- Benedikt Wirth
- Laurent Jacques
tags:
- '"optimization on manifolds"'
- '"Bézier interpolation"'
- '"Karcher mean"'
- '"piecewise Bézier curves"'
categories: []
date: '2016-01-01'
lastmod: 2021-08-06T23:54:33+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:33.444885Z'
publication_types:
- '1'
abstract: 
publication: '"*Proceedings of the third international Traveling Workshop on Interactions between Sparse models and Technology iTWIST''16*"'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/178314
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1609.04167
---

**Abstract**: Given a set of data points lying on a smooth manifold, we present methods to interpolate those with piecewise Bézier splines. The spline is composed of Bézier curves (resp. surfaces) patched together such that the spline is continuous and differentiable at any point of its domain. The spline is optimized such that its mean square acceleration is minimized when the manifold is the Euclidean space.  We show examples on the sphere S2 and on the special orthogonal group SO(3).
