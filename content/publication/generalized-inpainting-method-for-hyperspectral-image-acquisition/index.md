---
title: "Generalized inpainting method for hyperspectral image acquisition"
author: ["Laurent Jacques"]
tags: [1, 2015]
draft: false
subtitle: ''
summary: ''
authors:
- Kévin Degraux
- Valerio Cambareri
- Laurent Jacques
- Bert Geelen
- Carolina Blanch
- Gauthier Lafruit
tags:
- '"Hyperspectral"'
- '"inpainting"'
- '"iterative hard thresholding"'
- '"sparse models"'
- '"CMOS"'
- '"Fabry-Pérot"'
categories: []
date: '2015-01-01'
lastmod: 2021-08-06T23:54:34+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:34.303836Z'
publication_types:
- '1'
abstract: 
publication: '*Proc. ICIP 2015*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/167999
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1502.01853
doi: https://dx.doi.org/10.1109/ICIP.2015.7350811
---

**Abstract**: A recently designed hyperspectral imaging device enables multiplexed acquisition of an entire data volume in a single snapshot thanks to monolithically-integrated spectral filters. Such an agile imaging technique comes at the cost of a reduced spatial resolution and the need for a demosaicing procedure on its interleaved data.  In this work, we address both issues and propose an approach inspired by recent developments in compressed sensing and analysis sparse models. We formulate our superresolution and demosaicing task as a 3-D generalized inpainting problem. Interestingly, the target spatial resolution can be adjusted for mitigating the compression level of our sensing. The reconstruction procedure uses a fast greedy method called Pseudo-inverse IHT. We also show on simulations that a random arrangement of the spectral filters on the sensor is preferable to regular mosaic layout as it improves the quality of the reconstruction. The efficiency of our technique is demonstrated through numerical experiments on both synthetic and real data as acquired by the snapshot imager.
