---
title: "Compressive Sampling of Pulse Trains : Spread the Spectrum!"
author: ["Laurent Jacques"]
tags: [1, 2009]
draft: false
subtitle: ''
summary: ''
authors:
- Farid Naini
- Rémi Gribonval
- Laurent Jacques
- Pierre Vandergheynst
tags:
- '"Compressed Sensing"'
- '"Pulse Train"'
- '"Spread Spectrum"'
categories: []
date: '2009-01-01'
lastmod: 2021-08-06T23:54:39+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:39.713703Z'
publication_types:
- '1'
abstract: 
publication: '*Acoustics, Speech and Signal Processing, 2009. ICASSP 2009. IEEE International
  Conference on*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/105940
doi: https://dx.doi.org/10.1109/ICASSP.2009.4960224
---
