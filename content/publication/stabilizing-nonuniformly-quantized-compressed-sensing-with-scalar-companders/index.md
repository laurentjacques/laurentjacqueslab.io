---
title: "Stabilizing Nonuniformly Quantized Compressed Sensing with Scalar Companders"
author: ["Laurent Jacques"]
tags: [2, 2013]
draft: false
subtitle: ''
summary: ''
authors:
- Laurent Jacques
- david hammond
- Jalal Fadili
tags:
- '"icteam/elen"'
- '"compressed sensing"'
- '"quantization"'
- '"non-uniform"'
- '"companders"'
- '"stability"'
- '"generalized basis bursuit"'
- '"measure concentration"'
categories: []
date: '2013-01-01'
lastmod: 2021-08-06T23:54:23+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:23.642386Z'
publication_types:
- '2'
abstract: 
publication: '*IEEE Transactions on Information Theory*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/134382
- icon: arxiv
  name: arXiv
  icon_pack: ai
  url: https://arxiv.org/abs/1206.6003
doi: https://dx.doi.org/10.1109/TIT.2013.2281815
---

**Abstract**: This paper studies the problem of reconstructing sparse or compressible signals from compressed sensing measurements that have undergone nonuniform quantization.  Previous approaches to this Quantized Compressed Sensing (QCS) problem based on Gaussian models (bounded l2-norm) for the quantization distortion yield results that, while often acceptable, may not be fully consistent: re-measurement and quantization of the reconstructed signal do not necessarily match the initial observations. Quantization distortion instead more closely resembles heteroscedastic uniform noise, with variance depending on the observed quantization bin. Generalizing our previous work on uniform quantization, we show that for nonuniform quantizers described by the "compander" formalism, quantization distortion may be better characterized as having bounded weighted lp-norm (p >= 2), for a particular weighting. We develop a new reconstruction approach, termed Generalized Basis Pursuit DeNoise (GBPDN), which minimizes the sparsity of the reconstructed signal under this weighted lp-norm fidelity constraint.  We prove that for B bits per measurement and under the oversampled QCS scenario (when the number of measurements is large compared to the signal sparsity) if the sensing matrix satisfies a proposed generalized Restricted Isometry Property, then, GBPDN provides a reconstruction error of sparse signals which decreases like O(2^-B/sqrtp+1): a reduction by a factor sqrtp+1 relative to that produced by using the l2-norm.  Besides the QCS scenario, we also show that GBPDN applies straightforwardly to the related case of CS measurements corrupted by heteroscedastic Generalized Gaussian noise with provable reconstruction error reduction. Finally, we describe an efficient numerical procedure for computing GBPDN via a primal-dual convex optimization scheme, and demonstrate its effectiveness through simulations.
