---
title: "Sport players detection and tracking with a mixed network of planar and omnidirectional cameras"
author: ["Laurent Jacques"]
tags: [1, 2009]
draft: false
subtitle: ''
summary: ''
authors:
- A. Alahi
- Y. Boursier
- Laurent Jacques
- P. Vandergheynst
tags:
- '"sparse signal representation"'
- '"Convex Optimization"'
- '"people localization"'
categories: []
date: '2009-01-01'
lastmod: 2021-08-06T23:54:40+02:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2021-08-06T21:54:40.243923Z'
publication_types:
- '1'
abstract: 
publication: '*2009 Third ACM/IEEE International Conference on Distributed Smart Cameras
  (ICDSC)*'
links:
- name: DIAL
  url: http://hdl.handle.net/2078.1/67571
doi: https://dx.doi.org/10.1109/ICDSC.2009.5289406
---

**Abstract**: A generic approach is presented to detect and track people with a network of fixed and omnidirectional cameras given severely degraded foreground silhouettes.  The problem is formulated as a sparsity constrained inverse problem. A dictionary made of atoms representing the silhouettes of a person at a given location is used within the problem formulation. A reweighted scheme is considered to better approximate the sparsity prior. Although the framework is generic to any scene, the focus of this paper is to evaluate the performance of the proposed approach on a basketball game. The main challenges come from the players' behavior, their similar appearance, and the mutual occlusions present in the views. In addition, the extracted foreground silhouettes are severely degraded due to the polished floor reflecting the players, and the strong shadow present in the scene. We present qualitative and quantitative

results wi
th the APIDIS dataset as part of the ICDSC sport challenge.
