---
title: "Learning to Reconstruct Signals From Binary Measurements"
author: ["Laurent Jacques"]
tags: ["invited talk", "self-supervised learning", "one-bit compressive sensing", "learning", "signal set", "error bounds"]
draft: false
date: '2023-07-10'
date_end: '2023-07-14'
#links: 
#- name: Slides
#  icon: chalkboard
#  icon_packs: fa 
#  url: ''
url_video: ''
event: 'Special session on "Quantization in Signal Processing and Data Science", organized by Johannes Maly (LMU Munich) and Sjoerd Dirksen (Ultrecht University), at the "Sampling Theory and Applications Conference" (SAMPTA 2023).'
event_url: 'https://sampta2023.github.io/'
location: ' Yale, USA'
url_slides: 'Sampta23.pdf'
---

**Abstract**: Recent advances in unsupervised learning have highlighted the possibility of learning to reconstruct signals from noisy and incomplete linear measurements alone. These methods play a key role in medical and scientific imaging and sensing, where ground truth data is often scarce or difficult to obtain. However, in practice measurements are not only noisy and incomplete but also quantized.
Here we explore the extreme case of learning from binary observations and provide necessary and sufficient conditions on the number of measurements required for identifying a set of signals from incomplete binary data. Our results are complementary to existing bounds on signal recovery  from binary measurements. Furthermore, we introduce a novel self-supervised learning approach that only requires binary data for training. We demonstrate in a series of experiments with real datasets that our approach is on par with supervised learning and outperforms sparse reconstruction methods with a fixed wavelet basis by a large margin.

Joint work with [Julián Tachella](https://tachella.github.io/) (CNRS & ENS Lyon)
