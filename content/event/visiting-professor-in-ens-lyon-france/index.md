---
title: "Visiting professor in ENS Lyon, France"
author: ["Laurent Jacques"]
tags: ["scientific stay", "collaboration"]
draft: false
date: '2023-10-22'
date_end: '2023-11-04'
location: 'ENS Lyon, France'
#headless: true
---

From October 22nd till November 4th 2023, I was a visiting professor at the [Sisyph laboratory](http://www.ens-lyon.fr/PHYSIQUE/teams/signaux-systemes-physique), [École Normale Supérieure de Lyon](http://www.ens-lyon.fr/) (Lyon, France) invited by [Nelly Pustelnik](http://perso.ens-lyon.fr/nelly.pustelnik/) and [Julián Tachella](https://tachella.github.io/).

During this stay, on October 31st, I gave two talks,

-   in Creatis Lab, INSA, Lyon, "Interferometric Single-pixel Imaging with a Multicore Fiber" ([slides](./CreatisLJ311023.pdf))
-   in ENSLyon, "Keep the phase! Signal recovery in phase-only compressive sensing" ([slides](./PO-CS.pdf))

This visit is the first part of two scientific stays in ENS Lyon, the second being scheduled mid May 2024.
