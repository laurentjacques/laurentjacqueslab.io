---
title: "A simple gradient descent algorithm for blind gain calibration of randomized sensing devices"
author: ["Laurent Jacques"]
tags: ["invited seminar", "non convex", "blind calibration", "gradient descent", "biconvex", "random optimization"]
draft: false
date: '2016-10-04'
date_end: '2016-10-07'
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: 'Blind-Calib-Rennes.pdf'
url_video: ''
event: 'Invited in the group of Remi Gribonval (IRISA/INRIA)'
event_url: ''
location: 'Rennes, France'
---

In the context of my participation to the PhD "soutenance" of Marwa Chaffi (CentraleSupélec, Rennes, France).
