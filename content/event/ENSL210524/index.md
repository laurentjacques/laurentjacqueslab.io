---
title: "\"Learning to Reconstruct Signals From Binary Measurements\""
author: ["Laurent Jacques"]
tags: ["self-supervised learning", "one-bit compressive sensing", "learning prior", "learning signal set", "error bounds"]
draft: false
date: '2024-05-21'
#date_end: '2024-05-21'
#links: 
#- name: Slides
#  icon: chalkboard
#  icon_packs: fa 
#  url: ''
url_video: ''
event: 'Machine Learning and Signal Processing Seminars'
event_url: 'https://www.ens-lyon.fr/PHYSIQUE/seminars/machine-learning-and-signal-processing'
location: 'ENS Lyon, France'
url_slides: 'OneBitLearning.pdf'
---

**Abstract**: Recent advances in unsupervised learning have highlighted the possibility of learning to reconstruct signals from noisy and incomplete linear measurements alone. These methods play a key role in medical and scientific imaging and sensing, where ground truth data is often scarce or difficult to obtain. However, in practice measurements are not only noisy and incomplete but also quantized. Here we explore the extreme case of learning from binary observations and provide necessary and sufficient conditions on the number of measurements required for identifying a set of signals from incomplete binary data. Our results are complementary to existing bounds on signal recovery from binary measurements. Furthermore, we introduce a novel self-supervised learning approach that only requires binary data for training. We demonstrate in a series of experiments with real datasets that our approach is on par with supervised learning and outperforms sparse reconstruction methods with a fixed wavelet basis by a large margin.

This is a joint work with [Julián Tachella](https://tachella.github.io/) (CNRS &amp; ENS Lyon, France).
