---
title: "What can we learn from the Compressed Sensing theory?"
author: ["Laurent Jacques"]
date: 2012-03-22
tags: ["invited seminar", "seminar", "compressive sensing"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: lille-22032012.pdf
event: 'Research group of P. Chainais and P. Bas, "Laboratoire d''Automatique, Génie Informatique et Signal" (LAGIS)'
location: 'Ecole Centrale de Lille, France'
---

**Abstract**: The recent theory of Compressed Sensing (CS) induces a revolution in the design of signal sensors and of imaging devices. By the advent of increased computing capabilities, along with recent theoretical and numerical breakthroughs in the fields of Image Processing, Sparse Signal Representations, Inverse Problem solving and Convex Optimization, the term Sensing is no more a synonym for readily rendering human readable signals. More efficient systems than state of the art can be built by strongly interconnecting sensor design, signal reconstruction algorithms and prior knowledge on the signals of interest.

In this introductory talk, I will review the mathematical foundations of the CS paradigm, how it is tuned to sparse signal acquisition.
Then, I will present some recent developments of this theory when it must be combined with quantization, either at high bit rate, or in the extreme 1-bit sensing case which records only the sign of the compressive measurements. Finally, I will show how CS leads to efficient signal sensing procedures in computer vision and in tomographic applications like in X-Ray Computed Tomography and in Optical Deflectometry.
