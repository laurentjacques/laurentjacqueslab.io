---
title: "Compressive Single-Pixel Fourier Transform Imaging Using Structured Illumination"
author: ["Laurent Jacques"]
date: 2019-05-12
tags: ["invitated talk", "Fourier transform interferometry", "computational imaging"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: "ICASSP19_1pixFTI.pdf"
url_video: ''
event: 'Special session "Recent Advances in Signal Processing for Large-Scale Computational Imaging", ICASSP''19'
event_url: 'https://2019.ieeeicassp.org'
location: 'Brighton, UK'
---
