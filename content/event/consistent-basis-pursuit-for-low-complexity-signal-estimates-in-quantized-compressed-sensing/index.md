---
title: "Consistent Basis Pursuit for Low-Complexity Signal Estimates in Quantized Compressed Sensing"
author: ["Laurent Jacques"]
tags: ["invited seminar", "consistent basis pursuit", "quantized compressive sensing"]
draft: false
date: '2015-11-17'
date_end: '2015-11-20'
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: 'QCS-Rennes-final.pdf'
url_video: ''
event: 'Invited in the group of Remi Gribonval (IRISA/INRIA)'
location: 'Rennes, France'
---

In the context of my participation to the PhD "pré-soutenance" of Marwa Chaffi (CentraleSupélec, Rennes, France).
