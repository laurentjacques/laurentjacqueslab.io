---
title: "A Non-Convex Blind Calibration Method for Randomised Sensing Strategies"
author: ["Laurent Jacques"]
date: 2016-09-19
tags: ["invited talk", "non-convex", "optimization", "randomized optimization"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: COSERA16_BlindCal.pdf
event: 'CoSeRA 2016 (September 19-22, 2016)'
event_url: 'http://workshops.fhr.fraunhofer.de/cosera/'
location: 'Aachen, Germany'
---

Joint work with with Valerio Cambareri.
