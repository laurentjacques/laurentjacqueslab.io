---
title: "Structured Illumination and Variable Density Sampling for Compressive Fourier Transform Interferometry"
author: ["Laurent Jacques"]
date: 2019-02-03
tags: ["invited talk", "Fourier transform interferometry", "computational imaging"]
draft: false
event: 'Special session "Computational BiomEdical imaging" at the International BASP Frontiers workshop 2019 (February 3 - 8, 2019)'
event_url: 'http://www.baspfrontiers.org'
location: 'Villars-sur-Ollon, Switzerland'
---
