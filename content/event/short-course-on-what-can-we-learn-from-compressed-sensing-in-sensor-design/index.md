---
title: "What can we learn from Compressed Sensing in sensor design?"
author: ["Laurent Jacques"]
date: 2015-01-08
tags: ["tutorial", "sensor", "compressive sensing", "codesign"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: LJacques_GDR_120914.pdf
event: "FETCH2015 French winter school" 
event_url: "https://sites.google.com/site/fetch2015b/"
location: "UCLouvain, Belgium"
---
