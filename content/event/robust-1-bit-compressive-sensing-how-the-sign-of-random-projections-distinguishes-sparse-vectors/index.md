---
title: "Robust 1-Bit Compressive Sensing: How the Sign of Random Projections Distinguishes Sparse Vectors"
author: ["Laurent Jacques"]
date: 2011-12-21
tags: ["invited talk", " compressive sensing", "1-bit"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: 1bitCS_GCFNRS11.pdf
event: 'FNRS Contact Group "Wavelet and applications"'
location: 'ULB, Brussels, Belgium'
---

Joint work with J. Laska, P. Boufounos, and R. Baraniuk.
