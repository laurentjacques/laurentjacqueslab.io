---
title: "Keep the phase! Signal recovery in phase-only compressive sensing"
author: ["Laurent Jacques"]
date: 2021-02-04
tags: ["invited seminar", "phase-only", "compressive sensing"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: PO-CS-CASI-040221.pdf
url_video: 'https://bbb.unilim.fr/playback/presentation/2.0/playback.html?meetingId=fb3a312290447cb0d631d266b7700d4cedaf13ff-1612453052008&t=0h37m10s#'
event: ''
event_url: ''
location: '"Comité d''Animation Scientifique pour l''Interdisciplinarité" (CASI), Institut XLIM (Limoges, France)'
---

(Invited by T. Fromentèze. Joint work with Thomas Feuillen.)

**Abstract**:  In this seminar, we show how a sparse signal can be estimated from the phase of complex random measurements, in a "phase-only compressive sensing" (PO-CS) scenario. With high probability and up to a global unknown amplitude, we can perfectly recover such a signal if the sensing matrix is a complex Gaussian random matrix and the number of measurements is large compared to the signal sparsity. Our approach consists in recasting the (non-linear) PO-CS scheme as a linear compressive sensing model. We built it from a signal normalization constraint and a phase-consistency constraint. Practically, we achieve stable and robust signal direction estimation from the basis pursuit denoising program. Numerically, robust signal direction estimation is reached at about twice the number of measurements needed for signal recovery in compressive sensing.
