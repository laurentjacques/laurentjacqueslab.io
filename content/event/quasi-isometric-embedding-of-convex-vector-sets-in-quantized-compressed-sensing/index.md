---
title: "Quasi-isometric embedding of (convex) vector sets in quantized compressed sensing"
author: ["Laurent Jacques"]
date: 2015-12-07
tags: ["invited talk", "quasi-isometry", "random projections", "quantization"]
draft: false
event: 'Second Workshop on Optimization for Image and Signal Processing (MAORI), December 7-9, 2015'
event_url: 'http://www.lss.supelec.fr/MaoriWorkshop'
location: 'Institut Henri Poincaré, Paris, France'
---
