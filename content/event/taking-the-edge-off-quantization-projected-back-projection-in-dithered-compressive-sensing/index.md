---
title: "Taking the edge off quantization: projected back projection in dithered compressive sensing"
author: ["Laurent Jacques"]
date: 2018-06-10
tags: ["invited talk", "quantization", "compressive sensing", "dithering"]
draft: false
event: 'Special session "Signal and Information Processing with Coarsely Quantized Data" organized by M. Stein and Lee Swindlehurst, 2018 IEEE Statistical Signal Processing Workshop (SSP)'
event_url: 'https://ssp2018.org/'
location: 'Freiburg, Germany'
---

Joint work with C. Xu and V. Schellekens.
