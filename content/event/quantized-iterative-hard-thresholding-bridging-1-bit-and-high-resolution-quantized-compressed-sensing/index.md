---
title: "Quantized Iterative Hard Thresholding: Bridging 1-bit and High Resolution Quantized Compressed Sensing"
author: ["Laurent Jacques"]
date: 2013-07-02
tags: ["invited talk", "compressive sensing", "quantization"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: Sampta13_qiht.pdf
event: '"Invited Session III: Sampling and Quantization" of SAMPTA2013, July 1st-5th, 2013'
location: 'Bremen, Germany'
---

**Abstract**: In this work, we show that reconstructing a sparse signal from quantized compressive measurement can be achieved in an unified formalism whatever the (scalar) quantization resolution, i.e., from 1-bit to high resolution assumption. This is achieved by generalizing the iterative hard thresholding (IHT) algorithm and its binary variant (BIHT) introduced in previous works to enforce the consistency of the reconstructed signal with respect to the quantization model. The performance of this algorithm, simply called quantized IHT (QIHT), is evaluated in comparison with other approaches (e.g., IHT, basis pursuit denoise) for several quantization scenarios.
