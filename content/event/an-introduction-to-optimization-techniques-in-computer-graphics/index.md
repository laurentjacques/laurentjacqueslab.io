---
title: "Compressed Sensing: how to sample data from what you know"
author: ["Laurent Jacques"]
date: 2014-04-07
tags: ["tutorial", "compressive sensing"]
draft: false
subtitle: 'Part of the tutorial "An Introduction to Optimization Techniques in Computer Graphics"'
authors:
- I. Ihrke
- X. Granier
- L. Jacques
- G. Guennebaud
- B. Goldlücke
url_video: ''
event: 'Eurographics 2014'
event_url: 'http://eg2014.unistra.fr/html/program/tutorials.html#TUT9'
location: 'Strasbourg, France'
---
