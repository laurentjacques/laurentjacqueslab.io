---
title: "Modern Quantization Strategies for Compressive Sensing and Acquisition Systems"
author: ["Laurent Jacques"]
date: 2013-05-27
tags: ["tutorial", "compressive sensing", "quantization"]
draft: false
links: 
- name: Webpage
  icon: link
  icon_packs: fa 
  url: http://www.boufounos.com/resources-on-quantization/
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: http://www.boufounos.com/Presentations/BJ_ICASSP13_Modern_Quantization_Tutorial.pdf
url_video: ''
event: 'International Conference on Acoustics, Speech, and Signal Processing (ICASSP13)'
event_url: 'http://www.icassp2013.com/Tutorial_09.asp'
location: 'Vancouver, Canada'
authors:
- P. Boufounos
- L. Jacques
---
