---
title: "The importance of phase in complex compressive sensing"
author: ["Laurent Jacques"]
date: 2021-09-10
tags: ["invited keynote", "complex", "phase", "compressive sensing"]
draft: false
url_video: ''
event: '"AI for signal and image processing" meeting'
event_url: 'https://indico.ijclab.in2p3.fr/event/7326/overview'
location: 'Institut Pascal, Paris-Saclay University (France)'
url_video: 'https://www.youtube.com/watch?v=woqr2sRAP34'
links:
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: PO-CS.pdf
- name: demo (1)
  url: ./demo/po-cs-oppenheim-image-exper/
- name: ipynb (1)
  url: ./demo/po-cs-oppenheim-image-exper/Oppenheim_ImageExper.ipynb
- name: demo (2)
  url: ./demo/po-cs-phase-only-cs/
- name: ipynb (2)
  url: ./demo/po-cs-phase-only-cs/PhaseOnlyCS.ipynb
---

(joint work with T. Feuillen)

**Abstract**: In this talk, we consider the estimation of a sparse (or low-complexity) signal from the phase of complex random measurements, a "phase-only compressive sensing" (PO-CS) scenario. This study is motivated by the fixed range of such measurements, which can for instance ease further data quantization methods, with potential applications in radar, computed tomography or computational imaging. With high probability and up to a global unknown amplitude, we show that perfect signal recovery is possible if the sensing matrix is a complex Gaussian random matrix and the number of measurements is large compared to the signal sparsity. Our approach consists in recasting the (non-linear) PO-CS scheme as a linear compressive sensing model. We built it from a signal normalization constraint and a phase-consistency constraint. Practically, we achieve stable and robust signal direction estimation from the basis pursuit denoising program. Numerically, robust signal direction estimation is reached at about twice the number of measurements needed for signal recovery in compressive sensing
