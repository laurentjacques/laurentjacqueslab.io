---
title: "FNRS Contact Group - \"Wavelets and applications\", December 14th, 2021, UCLouvain, Belgium"
author: ["Laurent Jacques"]
tags: ["workshop", "organization"]
draft: false
date: '2021-12-14'
#date_end: ''
links: 
- name: Event URL
  url: 'https://sites.google.com/view/cgwa21'
location: 'Virtual UCLouvain'
---

The FNRS Contact Group on "Wavelets and Applications" (CGWA) is a series of workshops sponsored by the F.R.S.-FNRS. They are organized regularly since 1996. This time, due to recent Covid-19 restrictions, the CGWA workshop goes virtual and will be held on Zoom, on Tuesday, December 14, 2021 (registration is free but mandatory -- see below -- and the zoom link will be sent only to the registered participants).

For this edition, the main themes for the CGWA workshop include (but are not restricted to):

-   convex and non-convex inverse problems with low-dimensional signal models;
-   solving inverse problems with deep learning or low-complexity generative models;
-   computational imaging and compressive sensing;
-   applications of these fields in astronomy and biomedical imaging.

**Guest speakers**: The workshop will be honored to welcome the following two guest speakers: **Yann Traonmilin** (IMB, Bordeaux, France) and **Martin Genzel** (U. Utrecht, The Netherlands).

More information about this workshop can be found [here](https://sites.google.com/view/cgwa21).

Scientific Committee

-   Laurent Jacques, UCLouvain, Louvain-La-Neuve, Belgium
-   Ignace Loris, ULB, Brussels, Belgium
-   Christine De Mol, ULB, Brussels, Belgium
-   Françoise Bastin, ULg, Liège, Belgium
-   Jean-Pierre Antoine, UCLouvain, Louvain-La-Neuve, Belgium
