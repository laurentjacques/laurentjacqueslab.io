---
title: "A quantized view on compressive sensing systems and compressive learning methods"
author: ["Laurent Jacques"]
date: 2017-12-12
tags: ["invited talk", "quantization", "compressive sensing"]
draft: false
event: 'Journée thématique conjointe IEEE-CAS et GDR/SOC2(SoC-SiP)'
event_url: GDR121217.pdf 'https://www.ieeefrance.org/index.php/250-journee-thematique-conjointe-ieee-cas-et-gdr-soc2-soc-sip-12-decembre-2017-paristech'
location: 'ParisTech, Paris, France'
---
