---
title: "Time for dithering!  Quantized random embeddings with RIP random matrices"
author: ["Laurent Jacques"]
date: 2018-05-02
tags: ["invited talk", "quantization", "compressive sensing", "dithering"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: FutRandProjIPGG_final.pdf
url_video: 'https://www.youtube.com/watch?v=qeQjTnE9-J0'
event: 'Second Mini-workshop on "The Future of Random Projections" organized by Igor Carron (LightOn, Paris), Laurent Daudet (Paris Diderot & LightOn, Paris) and Florent Krzakala (ENS and LightOn, Paris)'
event_url: 'http://nuit-blanche.blogspot.be/2018/04/mini-workshop-future-of-random.html'
location: 'Paris, France'
---
