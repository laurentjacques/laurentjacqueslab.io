---
title: "Compressed Sensing in Optics: Schlieren Deflectometry and Refractive Index Map Reconstruction"
author: ["Laurent Jacques"]
date: 2014-09-12
tags: ["tutorial", "optics", "deflectometry", "inverse problem"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: LJacques_GDR_120914.pdf
event: '"Journée thématique conjointe GDR/ISIS et GDR/SoC-SiP" entitled "Acquisition/Echantillonnage comprimé : quelles réalisations/applications pratiques ?"'
event_url: ""
location: "Télécom ParisTech, France"
---
