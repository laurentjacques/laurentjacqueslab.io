---
title: Events
subtitle: Future and past seminars, short courses, tutorials, organizations, ... (only from 2012)
cms_exclude: true

# View.
#   1 = List
#   2 = Compact
#   3 = Card
view: 1

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---
