---
title: "Quantizing compressed sensing: From high resolution to 1-bit quantization schemes"
author: ["Laurent Jacques"]
tags: ["tutorial", "compressive sensing", "quantization"]
draft: false
date: 2014-06-02
date_end: 2014-06-20
url_video: ''
event: 'Joint ICTP-TWAS School on Coherent State Transforms, Time-Frequency and Time-Scale Analysis, Applications'
event_url: 'http://cdsagenda5.ictp.it/full_display.php?ida=a13201'
location: 'ICTP, Trieste, Italy'
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: ICTP14_LJ.pdf
---
