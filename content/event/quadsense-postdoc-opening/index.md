---
title: "Project QuadSense: postdoc opening!"
author: ["Laurent Jacques"]
tags: ["postdoc", "opening", "quadsense", "project", "fnrs"]
draft: false
date: '2024-05-15'
image:
  caption: ""
  focal_point: ""
  preview_only: false
---

The research group of Prof. Laurent Jacques in the mathematical engineering department ([INMA](https://uclouvain.be/fr/node/2107)) and the [Image and Signal Processing Group](https://ispgroup.gitlab.io/) (ISPGroup) at [UCLouvain](https://uclouvain.be/en), Belgium, opens one position for a postdoctoral researcher to work on the **QuadSense** project (see project description below), a new project funded by the [Belgian Fund for Scientific Research - FNRS](https://www.frs-fnrs.be/en).

> &rarr; **Application deadline**: None (but the position could be closed earlier if a good candidate is found)<br />
> &rarr; **Starting date**: as soon as the selected candidate is available.

&rArr; For more information (application procedure, project topic, ...) &rarr; [Go here](/project/quadsense/) &larr;
