---
title: "Computational Fourier Transform Interferometry for Hyperspectral Imaging"
author: ["Laurent Jacques"]
date: 2019-09-02
tags: ["invited keynote"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: WOG19.pdf
url_video: ''
event: 'Meeting on Image reconstruction of the network on "Turning images into value through statistical parameter estimation"'
event_url: ''
location: 'UGhent, Belgium'
---

**Abstract**: Fourier Transform Interferometry (FTI) is an appealing Hyperspectral (HS) imaging modality for applications demanding high spectral resolution. In fluorescence microscopy (FM), the effective spectral resolution is, however, limited by the durability (or photobleaching) of biological elements when exposed to the illuminating light.

In this talk, we will first focus on two variants of the FTI imager leveraging the theory of compressive sensing (CS). By following a variable density sampling strategy, we provide two light modulation strategies (acting either temporally or spatiotemporally) minimizing the light exposure imposed on a biological specimen while preserving the spectral resolution. Second, we propose a single-pixel HyperSpectral (HS) imaging scheme collimating the light before interferometry and deploying an adapted space-time coding of the light illumination. This solution is of interest for miniaturized hyperspectral imagers.

This is a joint work with Amirafshar Moshtaghpour (UCLouvain, Belgium) and José Bioucas-Dias (IT, Portugal).
