---
title: "Compressed sensing et récents développements en matière de quantification extrême des mesures (à 1-bit)"
author: ["Laurent Jacques"]
date: 2014-06-23
tags: ["tutorial", "compressive sensing", "one-bit", "quantization"]
draft: false
event: 'French "Journée Scientifique de la Fédération Charles Hermite" on "Parcimonie" (sparsity).'
location: 'Institut Elie Cartan de Lorraine, IECL, Nancy, France'
---
