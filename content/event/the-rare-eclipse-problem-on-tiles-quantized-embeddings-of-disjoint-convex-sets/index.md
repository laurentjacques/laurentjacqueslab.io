---
title: "The Rare Eclipse Problem on Tiles: Quantized Embeddings of Disjoint Convex Sets"
author: ["Laurent Jacques"]
date: 2017-07-03
tags: ["invited talk", "classification", "random projections", "quantization"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: SAMPTA17_QREP_LJ.pdf
event: 'Special session "Mathematical theory of quantization" organized by Sjoerd Dirksen and Rayan Saab, at SAMPTA17 (July 3-7, 2017)'
location: 'Tallinn, Estonia'
---

(joint work with V. Cambareri and C. Xu). See also [arXiv:1702.04664](https://arxiv.org/abs/1702.04664) for the corresponding preprint.
