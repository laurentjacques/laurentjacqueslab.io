---
title: "Time for dithering!  Quantized random embeddings with RIP random matrices"
author: ["Laurent Jacques"]
date: 2018-03-05
tags: ["invited seminar", "dithering", "compressive sensing", "quantization", "rip"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: TAMU080318.pdf
url_video: ''
event: 'Seminar at the Department of Mathematics'
date_end: 2018-03-09
event_url: ''
location: 'Texas A&M University, TX, USA'
---

Invited by [Simon Foucart](https://www.math.tamu.edu/~foucart/).
