---
title: "A short introduction to compressive sensing, its applications and relatives"
author: ["Laurent Jacques"]
date: 2017-02-16
tags: ["invited talk", "compressive sensing", "introduction"]
draft: false
event: 'URSI Forum (February 16, 2017)'
event_url: 'http://www.sic.rma.ac.be/workshop/ursiforum2017.html'
location: 'RMA, Brussels, Belgium'
---
