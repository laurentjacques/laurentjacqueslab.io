---
title: "Interferometric Lensless Endoscopy: Rank-one Projections of Image Frequencies with Speckle Illuminations"
author: ["Laurent Jacques"]
tags: ["invited talk", "rank-one projection", "interferometry", "lensless imaging", "endoscopy"]
draft: false
date: '2021-10-31'
date_end: '2021-11-03'
event: 'Special session on "Computational Sampling", organized by Ayush Bhandari (Imperial College, UK), Asilomar, 2021'
event_url: 'https://cmsworkshops.com/Asilomar2021/Papers/ViewSession.asp?Sessionid=1042'
location: 'Virtual Asilomar, USA'
url_slides: 'Asilomar21-ILE-slides.pdf'
#url_video: 
links:
- name: Video (mp4, 55Mb)
  url: 'Asilomar21-ILE-talk.mp4'
---

**Abstract**: Lensless endoscopy (LE) with multicore fibers (MCF) enables fluorescent imaging of biological samples at cellular scale. In this talk, we will see that under a common far-field approximation, the corresponding imaging process is tantamount to collecting multiple rank-one projections (ROP) of an Hermitian "interferometric" matrix--a matrix encoding a subsampling of the Fourier transform of the sample image. Specifically, each ROP of this matrix is achieved with the complex vector shaping the incident wavefront (using a spatial light modulator), and, for specific MCF core arrangements, the interferometric matrix collects as many image frequencies as the square of the core number. When the SLM is configured randomly, this combined sensing viewpoint allows us to characterize the sample complexity of the system. In particular, by inspecting the separate dimensional conditions ensuring the specific restricted isometry properties of the two composing sensing models in the observation of sparse images, we show that a basis pursuit denoising (BPDN) program associated with an \\(\ell\_1\\)-fidelity cost provably provides a stable and robust image estimate. Finally, preliminary numerical experiments demonstrate the effectiveness of this imaging procedure.

This is an ongoing research made in collaboration with Olivier Leblanc (UCLouvain, Belgium), Siddharth Sivankutty (Cailabs, Rennes, Brittany, France), and Hervé Rigneault (Institut Fresnel, Marseille, France).
