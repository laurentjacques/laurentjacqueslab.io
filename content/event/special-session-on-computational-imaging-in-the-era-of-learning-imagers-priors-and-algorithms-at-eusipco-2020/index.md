---
title: "Special Session on \"Computational Imaging in the Era of Learning: Imagers, Priors and Algorithms\" at EUSIPCO 2020"
author: ["Laurent Jacques"]
tags: ["special session", "organization"]
draft: false
date: '2021-01-21'
#date_end: ''
links: 
- name: Event URL
  url: 'https://www.eurasip.org/Proceedings/Eusipco/Eusipco2020/HTML/session-index.html#1087'
location: 'Virtual Amsterdam'
---

**Abstract**: _How can we co-design data capture and image reconstruction to optimally recover visual information in challenging imaging conditions such as low-light, multiple scattering, and non-line-of-sight?  How can machine learning help us solve these complex problems that are fundamental to computational cameras, diffractive imaging, and autonomous navigation.  Can we learn  effective  prior models  from data  to represent images  while  characterizing the  imager  simultaneously?   How  can  we  leverage  multiple  data  streams  that  differ  in  spatial  and/or  temporal  resolution, field-of-view, and dynamic range to boost the accuracy of the image recovery algorithms? This special session covered the latest breakthrough research that answer these questions in 2021.  Selected works were at the interplay of signal processing and machine learning.  The special session hence provided the EUSIPCO audience and signal processing community with a very timely update on how imaging benefits from recent advances in deep neural networks and generative models_.

**Organizers**: Laurent Jacques (UCLouvain, Belgium) and Emrah Bostan (Ams AG International; formerly U. Amsterdam, Netherlands)


## Special session program: {#special-session-program}


### SS-1.1: MODEL AND LEARNING-BASED COMPUTATIONAL 3D PHASE MICROSCOPY WITH INTENSITY DIFFRACTION TOMOGRAPHY {#ss-1-dot-1-model-and-learning-based-computational-3d-phase-microscopy-with-intensity-diffraction-tomography}

Matlock, Alex, Boston University, United States Xue, Yujia, Boston University, United States Li, Yunzhe, Boston University, United States Cheng, Shiyi, Boston University, United States Tahir, Waleed, Boston University, United States Tian, Lei, Boston University, United States


### SS-1.2: MODELLING A MICROSCOPE AS LOW DIMENSIONAL SUBSPACE OF OPERATORS {#ss-1-dot-2-modelling-a-microscope-as-low-dimensional-subspace-of-operators}

Debarnot, Valentin, CNRS, France Escande, Paul, CNRS, France Mangeat, Thomas, CNRS, France Weiss, Pierre, CNRS, France


### SS-1.3: THE MODULO RADON TRANSFORM AND ITS INVERSION {#ss-1-dot-3-the-modulo-radon-transform-and-its-inversion}

Bhandari, Ayush, Imperial College London, United Kingdom Beckmann, Matthias, University of Hamburg, Germany Krahmer, Felix, University of Hamburg, Germany


### SS-1.4: THE BENEFITS OF SIDE INFORMATION FOR STRUCTURED PHASE RETRIEVAL {#ss-1-dot-4-the-benefits-of-side-information-for-structured-phase-retrieval}

Asif, Salman, UC Riverside, United States Hegde, Chinmay, NYU, United States


### SS-1.5: DESIGNING CNNS FOR MULTIMODAL IMAGE SUPER-RESOLUTION VIA THE METHOD OF MULTIPLIERS {#ss-1-dot-5-designing-cnns-for-multimodal-image-super-resolution-via-the-method-of-multipliers}

Marivani, Iman, vrije universiteit brussel-imec, Belgium Tsiligianni, Evaggelia, vrije universiteit brussel-imec, Belgium Cornelis, Bruno, vrije universiteit brussel-imec, Belgium Deligiannis, Nikos, vrije universiteit brussel-imec, Belgium


### SS-1.6: RANDOM ILLUMINATION MICROSCOPY FROM VARIANCE IMAGES {#ss-1-dot-6-random-illumination-microscopy-from-variance-images}

Labouesse, Simon, CU Boulder, France Idier, Jérôme, LS2N, France Sentenac, Anne, Institut Fresnel, France Mangeat, Thomas, CBI, France Allain, Marc, Institut Fresnel, CNRS, F-13397 Marseille, France, France
