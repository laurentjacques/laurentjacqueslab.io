---
title: "Quantized Compressive Sensing with RIP Matrices: The Benefit of Dithering"
author: ["Laurent Jacques"]
date: 2018-05-16
tags: ["invited talk", "quantization", "compressive sensing", "dithering"]
draft: false
event: 'ICCHA7 (May 14-18, 2018)'
event_url: 'https://my.vanderbilt.edu/iccha7/'
location: 'Vanderbilt U., TN, USA.'
---
