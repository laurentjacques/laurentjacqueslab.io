---
title: "Quantized Compressive Sensing with RIP Matrices: The Benefit of Dithering"
author: ["Laurent Jacques"]
date: 2018-05-29
tags: ["invited seminar", "dithering", "RIP", "compressive sensing", "embedding"]
draft: false
event: 'Research group of Sjoerd Dirksen and Holger Rauhut.'
location: 'RWTH, Aachen, Germany'
---
