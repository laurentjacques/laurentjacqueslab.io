---
title: "New preprint submitted to SampTA2025: \"Herglotz-NET: Implicit Neural Representation of Spherical Data with Harmonic Positional Encoding\""
author: ["Laurent Jacques"]
tags: ["preprint", "conference"]
draft: false
date: '2025-02-20'
---

More info [on this page](/publication/herglotz-net-implicit-neural-representation-of-spherical-data-with-harmonic-positional-encoding).
