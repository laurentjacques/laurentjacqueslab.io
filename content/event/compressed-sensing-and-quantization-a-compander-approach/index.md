---
title: "Compressed Sensing and Quantization: a Compander Approach"
author: ["Laurent Jacques"]
date: 2012-05-10
tags: ["talk", "quantization", "basis pursuit", "bpdq", "compresssive sensing"]
draft: false
links: 
- name: Link
  icon: link
  icon_packs: fa 
  url: http://sites.google.com/site/itwist1st/
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: itwist12-jacques.pdf
event: 'iTWIST2012 workshop'
event_url: http://sites.google.com/site/itwist1st/
location: 'Marseille, France'
---

**Joint work with D. Hammond (U. Oregon, USA) and J. Fadili (GREYC, Caen, France).**
