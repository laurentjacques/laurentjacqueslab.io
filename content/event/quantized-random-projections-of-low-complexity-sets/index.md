---
title: "Quantized random projections of low-complexity sets"
author: ["Laurent Jacques"]
date: 2016-02-15
tags: ["invited talk", "quantization", "random projections"]
draft: false
event: 'Workshop on Low Complexity Models in Signal Processing, February 15-19, 2016, Hausdorff Trimester Program on "Mathematics of Signal Processing"'
event_url: 'https://www.him.uni-bonn.de/en/programs/current-trimester-program/signal-processing-2016/workshop-on-low-complexity-models/'
location: 'Bonn, Germany.'
---

**2016**  Invited speaker, talk on "".
