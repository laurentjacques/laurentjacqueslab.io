---
title: "Interferometric single-pixel imaging with a multicore fiber"
author: ["Laurent Jacques"]
tags: ["interferometry", "compressive sensing", "rank-one projection", "lensless"]
draft: false
date: '2023-06-12'
date_end: '2023-06-14'
event: 'International Symposium on Computational Sensing (ISCS23)'
event_url: 'https://sites.google.com/view/iscs23'
location: 'Luxembourg'
url_slides: 'LJ_ISCS23.pdf'
url_video: 'https://www.youtube.com/watch?v=1lr-VIyWdCY'
---

**Abstract**: Lensless illumination single-pixel imaging with a multicore fiber (MCF) is a computational imaging technique that enables potential endoscopic observations of biological samples at cellular scale. In this work, we show that this technique is tantamount to collecting multiple symmetric rank-one projections (SROP) of a Hermitian _interferometric_ matrix -- a matrix encoding the spectral content of the sample image. In this model, each SROP is induced by the complex _sketching_ vector shaping the incident light wavefront with a spatial light modulator (SLM), while the projected interferometric matrix collects up to \\(O(Q^2)\\) image frequencies for a \\(Q\\)-core MCF. While this scheme subsumes previous sensing modalities, such as raster scanning (RS) imaging with beamformed illumination, we demonstrate that collecting the measurements of \\(M\\) random SLM configurations -- and thus acquiring \\(M\\) SROPs -- allows us to estimate an image of interest if \\(M\\) and \\(Q\\) scale linearly (up to log factors) with the image sparsity level, hence requiring much fewer observations than RS imaging or a complete Nyquist sampling of the \\(Q \times Q\\) interferometric matrix. This demonstration is achieved both theoretically, with a specific restricted isometry analysis of the sensing scheme, and with extensive Monte Carlo experiments. Experimental results made on an actual MCF system finally demonstrate the effectiveness of this imaging procedure on a benchmark image.
