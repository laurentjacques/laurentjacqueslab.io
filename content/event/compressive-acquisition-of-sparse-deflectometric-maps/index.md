---
title: "Compressive Acquisition of Sparse Deflectometric Maps"
author: ["Laurent Jacques"]
date: 2013-07-04
tags: ["invited talk", "compressive sensing", "quantization"]
draft: false
links: 
- name: Slides
  icon: chalkboard
  icon_packs: fa 
  url: Sampta13_qiht.pdf
event: '"Invited Session IX: Sampling for Imaging Science" of SAMPTA2013, July 1st-5th, 2013'
location: 'Bremen, Germany'
---

**Abstract**: Schlieren deflectometry aims at measuring deflections of light rays from transparent objects, which is subsequently used to characterize the objects. With each location on a smooth object surface a sparse deflection map (or spectrum) is associated. In this paper, we demonstrate the compressive acquisition and reconstruction of such maps, and the usage of deflection information for object characterization, using a schlieren deflectometer. To this end, we exploit the sparseness of deflection maps and we use the framework of spread spectrum compressed sensing. Further, at a second level, we demonstrate how to use the deflection information optimally to reconstruct the distribution of refractive index inside an object, by exploiting the sparsity of refractive index maps in gradient domain.
