+++
title = "Testing a Quasi-Isometric Quantized Embedding"
author = ["Laurent Jacques"]
date = 2013-11-19
tags = ["compressed sensing", "general"]
draft = false
+++

It took me a certain time to do it. Here is at least a first attempt to test numerically the validity of some of the results I obtained in "_[A Quantized Johnson Lindenstrauss Lemma: The Finding of Buffon’s Needle.](/publication/a-quantized-johnson-lindenstrauss-lemma-the-finding-of-buffon-s-needle/)_"

I have decided to avoid using the too conventional matlab environment. Rather, I took this exercise as an opportunity to learn the “ipython notebooks” and the wonderful tools provided by the [SciPy](http://scipy.org/) python ecosystem.

In short, for those of you who don’t know it, the ipython notebooks allow you to generate actual scientific HTML reports with (latex rendered) explanations and graphics.

The result cannot be properly presented on this blog (hosted on WordPress), so, I decided to share the report through [IPython Notebook Viewer website](http://nbviewer.ipython.org/).

Here it is:

-   “[Testing a Quasi-Isometric Embedding](/demo/a-quantized-johnson-lindenstrauss-lemma-the-finding-of-buffon-s-needle-demo/)”

(update 21/11/2013) … and a variant of it estimating a “curve of failure” (rather than playing with standard deviation analysis):

-   “[Testing a Quasi-Isometric Embedding with Percentile Analysis](/demo/a-quantized-johnson-lindenstrauss-lemma-the-finding-of-buffon-s-needle-demo-q)”

Moreover, on these two links, you have also the possibility to download the corresponding script for running it on your own IPython notebook system.

If you have any comments or corrections, don’t hesitate to add them below in the “comment” section. Enjoy!

_Image credit_: [An experiment to estimate \\(\pi\\) thanks to Buffon's theorem, from Wikipedia](https://en.wikipedia.org/wiki/Buffon%27s%5Fneedle%5Fproblem#Estimating%5F%CF%80).
