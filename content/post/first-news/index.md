+++
title = "First news"
author = ["Laurent Jacques"]
date = 2008-05-18
tags = ["general"]
draft = false
+++

So, as many researchers in the world, I have just opened my own _Science 2.0._ blog : this "Le petit chercheur illustré". An approximative English translation would be "The Illustrated (report of a) Small Researcher". As you can see, I decided to use [WordPress](http://wordpress.com) since this site supports LaTeX writing and I foresee to use it of course to display some useful notations and equations.

I hope I will introduce here some interesting elements about the scientific interests of an humble researcher in applied mathematics. As you will understand, my English is far to be perfect but I'll try to do my best to express myself correctly (not as a native English however).

To give you an idea, my fields of research are rather various. One of them is "signal representation", a subtopic of "signal processing". The term _signal_ has to be understood in its wide sense, I mean, signals living in 1-D (like the record of a piece of music), 2-D or n-D (e.g. images, videos, or multi-modal signals), or on more esoteric spaces like the sphere (imagine the measure of the temperature field all over the world). Signal can also be described as data provided on a given _manifold_, e.g. like the electric potential on a molecular surface, or this manifold itself like the high dimensional space of all the images produced by a moving camera ...

I work also on how to obtain or tune a signal "representation" using concepts, methods or algorithms like [\*-lets](http://www.laurent-duval.eu/siva-wits-where-is-the-starlet.html) basis, dictionaries, signal sparsity, signal compressibility, Basi  Pursuit, \*-Matching Pursuits, [compressed sensing](http://www.dsp.ece.rice.edu/cs/), ... I'm interested also in applications like [plenoptic](http://en.wikipedia.org/wiki/Plenoptic%5Fcamera) imaging, [light field](http://en.wikipedia.org/wiki/Light%5Ffield) rendering, [computational photography](http://en.wikipedia.org/wiki/Computational%5Fphotography), ... i.e. new ways to record visible information of the world. So, most of the news I'll publish here will concern more or less directly one of these elements. I do not plan to write here final reflection or results so be aware that I will write sometimes erroneous explanations (_C'est la vie_). But I'm sure you'll help me to improve them by inserting \_comment\_s.

_Image credit_: [La Science Illustrée, Wikipedia](https://fr.wikipedia.org/wiki/La%5FScience%5Fillustr%C3%A9e).

So, in one word : welcome ! Laurent Image
