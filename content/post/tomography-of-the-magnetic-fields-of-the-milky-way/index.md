+++
title = "Tomography of the magnetic fields of the Milky Way?"
author = ["Laurent Jacques"]
date = 2011-12-07
tags = ["General", "inverse problem", "tomography"]
draft = false
+++

I have just found this "new" (well 150 years old actually) tomographical method... for measuring the **magnetic field of our own galaxy**

> "[New all-sky map shows the magnetic fields of the Milky Way with the highest precision](http://www.mpa-garching.mpg.de/mpa/institute/news%5Farchives/news1112%5Ffara/news1112%5Ffara-en.html)"<br />
> by Niels Oppermann et al.
> (arxiv work available [here](http://arxiv.org/abs/1111.6186))
>
> "_We aim to summarize the current state of knowledge regarding Galactic Faraday rotation in an all-sky map of the Galactic Faraday depth. For this we have assembled the most extensive catalog of Faraday rotation data of compact extragalactic polarized radio sources to date. In the map making procedure we use a recently developed algorithm that reconstructs the map and the power spectrum of a statistically isotropic and homogeneous field while taking into account uncertainties in the noise statistics. This procedure is able to identify some rotation angles that are offset by an integer multiple of \\(\pi\\). The resulting map can be seen as an improved version of earlier such maps and is made publicly available, along with a map of its uncertainty. For the angular power spectrum we find a power law behavior with a power law index of \\(-2.14\\) for a Faraday sky where an overall variance profile as a function of Galactic latitude has been removed, in agreement with earlier work. We show that this is in accordance with a 3D Fourier power spectrum \\(P(k)\\) proportional to \\(k^{-2.14}\\) of the underlying field \\(n\_e \times B\_r\\) under simplifying geometrical and statistical assumptions._"

Selected excerpt:

> _... One way to measure cosmic magnetic fields, which has been known for over 150 years, makes use of an effect known as Faraday rotation.  When polarized light passes through a magnetized medium, the plane of polarization rotates. The amount of rotation depends, among other things, on the strength and direction of the magnetic field. Therefore, observing such rotation allows one to investigate the properties of the intervening magnetic fields._

Mmmm... very interesting, at least for my personal knowledge of the wonderful tomographical problem zoo (among gravitational lensing, interferometry, MRI, deflectometry).

_Note_: Wow... 16 months without any post here. I'm really bad.

_Image credit_: [Wikipedia](https://en.wikipedia.org/wiki/Milky%5FWay#Appearance)
