+++
title = "1000th visit and some Compressed Sensing \"humour\""
author = ["Laurent Jacques"]
date = 2008-11-22
tags = [2008]
draft = false
+++

As detected by [Igor Carron](http://igorcarron.googlepages.com/), this blog has reached its 1000th visit ! Well, perhaps it's 1000th robot visit ;-)

Yesterday I found some very funny (math) jokes on [Bjørn's maths blog](http://bjornsmaths.blogspot.com/) about ["How to catch a lion in the Sahara desert"](http://bjornsmaths.blogspot.com/2005/11/how-to-catch-lion-in-sahara-desert.html) with some ... mathematical tools.

Bjørn collected there many ways to realize this task from many places on the web. There are really tons of examples. To give you an idea, here is the Schrodinger's method:

> "At any given moment there is a positive probability that there is a lion in the cage. Sit down and wait."

or this one :

> "The method of inverse geometry: We place a spherical cage in the desert and enter it. We then perform an inverse operation with respect to the cage. The lion is then inside the cage and we are outside."

So, let's try something about Compressed Sensing. (Note: if you have something better than my infamous suggestion, I would be very happy to read it as a comment to this post.)

> How to catch a lion in the Sahara desert? The compressed sensing way: First you consider that only **one** lion in a big desert is definitely a very sparse situation by comparing lion's size and the desert area. No need for a cage, just project randomly the whole desert into a dune of just 5 times the lion's weight ! Since the lion obviously died in this shrinking operation, you use the **RIP** (!) .. and/ relaxed\_, you eventually reconstruct its best tame approximation.

_Image credits_: [Wikipedia](https://commons.wikimedia.org/wiki/File:Lion%5Fwaiting%5Fin%5FNamibia.jpg#filelinks).
