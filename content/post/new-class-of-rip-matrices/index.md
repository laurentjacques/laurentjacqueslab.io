+++
title = "New class of RIP matrices?"
author = ["Laurent Jacques"]
date = 2010-05-18
tags = ["general", "compressive sensing"]
draft = false
+++

Wow, almost one year and half without any post here...  Shame on me!  I'll try to be more productive with shorter posts now ;-)

I just found this interesting paper about concentration properties of submodular function (very common in "Graph Cut" methods for instance) on arxiv:

> [A note on concentration of submodular functions. (arXiv:1005.2791v1 [cs.DM])](http://arxiv.org/abs/1005.2791)
>
> Jan Vondrak, May 18, 2010
>
> _We survey a few concentration inequalities for submodular and fractionally subadditive functions of independent random variables, implied by the entropy method for self-bounding functions. The power of these concentration bounds is that they are dimension-free, in particular implying standard deviation \\(O(\sqrt{\mathbb E[f]})\\) rather than \\(O(\sqrt{n})\\) which can be obtained for any 1-Lipschitz function of n variables._

In particular, the author shows some interesting concentration results in his Corollary 3.2:

{{< figure src="/ox-hugo/2021-08-18-T21-14-29.png" width="700" >}}

Without having performed any developments, I'm wondering if this result could serve to define a new class of matrices (or non-linear operators) satisfying either the Johnson-Lindenstrauss Lemma or the Restricted Isometry Property.

For instance, by starting from Bernoulli vectors \\(X\\), _i.e._, the rows of a sensing matrix, and defining some specific submodular (or self-bounding) functions \\(f\\) (_e.g._, \\(f(X\_1,\cdots, X\_n) = g(X^T a)\\) for some sparse vector \\(a\\) and a "kind" function \\(g\\)), I'm wondering if the concentration results above are better than those coming from the classical concentration inequalities (based on the Lipschitz properties of \\(f\\) or \\(g\\). See e.g., the books of Ledoux and Talagrand)?

Ok, all this is perhaps just too hasty thoughts ....  written before a mug of black coffee ;-)
