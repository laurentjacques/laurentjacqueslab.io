+++
title = "SPGL1 and TV: Answers from SPGL1 Authors"
author = ["Laurent Jacques"]
date = 2008-09-02
tags = ["general", "sparse regularization", "total variation", "convex optimization", "spgl1"]
draft = false
+++

Following the writing of my previous post, which obtained various interesting [comments](/post/spgl1-and-tv-minimization) (many thanks to [Gabriel Peyré](http://www.ceremade.dauphine.fr/~peyre/), [Igor Carron](http://igorcarron.googlepages.com/home) and [Pierre Vandergheynst](http://ltspc89.epfl.ch/~vandergh/)), I sent a mail to [Michael P. Friedlander](http://www.cs.ubc.ca/%7Empf) and [Ewout van den Berg](http://www.cs.ubc.ca/%7Eewout78) to point them this article and possibly obtain their point of views. Nicely, they sent me interesting answers (many thanks to them). Here they are (using the notations of the [previous post](/post/spgl1-and-tv-minimization/)) : Michael's answer is about the need of a TV Lasso solver :

> "_It's an intriguing project that you describe.  I suppose in principle the theory behind [spgl1](https://friedlander.io/spgl1/) should readily extend to TV (though I haven't thought how a semi-norm might change things).  But I'm not sure how easy it'll be to solve the"TV-Lasso” subproblems.  Would be great if you can see a way to do it efficiently._"

Ewout on his side explained this :

> "_The idea you suggest may very well be feasible, as the approach taken in [SPGL1](https://friedlander.io/spgl1/) can be extended to other norms (i.e., not just the one-norm), as long as the dual norm is known and there is way to orthogonally project onto the ball induced by the primal norm. In fact, the newly released version of [SPGL1](https://friedlander.io/spgl1/) takes advantage of this and now supports two new formulations._
>
> _I heard (I haven't had time to read the paper) that Chambolle has described the dual to the TV-norm. Since the derivate of \\(\phi(\tau) =\\|y-Ax\_\tau\\|\_2\\) on the appropriate interval is given by the dual norm on \\(A^Ty\\), that part should be fine (for the one norm this gives the infinity norm)._
>
> _In SPGL1 we solve the Lasso problem using a spectrally projected gradient method, which means we need to have an orthogonal projector for the one-norm ball of radius \\(tau\\). It is not immediately obvious how to (efficiently) solve the related problem (for a given \\(x\\)):_
>
> _\\[{\rm minimize}\ \\|x - p\\|\_2\ {\rm subject\ to}\ \\|p\\|\_{\rm TV} \leq \tau.\\]_
>
> _However, the general approach taken in SPGL1 does not really care about how the Lasso subproblem is solved, so if there is any efficient way to solve_
>
> _\\[{\rm minimize}\ \\|Ax - b\\|\_2\ {\rm subject\ to}\ \\|x\\|\_{\rm TV} \leq \tau,\\]_
>
> _then that would be equally good._
>
> _Unfortunately it seems the complexification trick ([see the previous post](/post/spgl1-and-tv-minimization/)) works only from the image to the differences; when working with the differences themselves, additional constraints would be needed to ensure consistency in the image; i.e., that summing up the difference of going right first and then down, be equal to the sum of going down first and then right.”_

In a second mail, Ewout added an explanation on this last remark :

> _"I was thinking that perhaps, instead of minimizing over the signal it would be possible to minimize over the differences (expressed in complex numbers in the two-dimensional setting). The problem with that is that most complex vectors do not represent difference vectors (i.e., the differences would not add up properly). For such an approach to work, this consistency would have to be enforced by adding some constraints."_

Actually, I saw similar considerations in [A. Chambolle](http://www.cmap.polytechnique.fr/~antonin/)'s paper: ["﻿](http://www.ceremade.dauphine.fr/preprints/CMD/2002-40.ps.gz)[An Algorithm for Total Variation/ Minimization and Applications](http://www.ceremade.dauphine.fr/preprints/CMD/2002-40.ps.gz)". It is even more clear in the paper he wrote with [J.-F. Aujol](http://www.cmla.ens-cachan.fr/Membres/aujol.html), ["Dual Norms and Image Decomposition Models"](ftp://ftp.inria.fr/INRIA/publication/publi-pdf/RR/RR-5130.pdf). They develop there the notions of TV (semi) norm for different exponent \\(p\\) (i.e. in the \\(\ell\_p\\) norm used on the \\(\ell\_2\\) norm of the gradient components) and in particular they answer to the problem of finding and computing the corresponding dual norms. For the usual TV norm, this leads to the _G-norm_ :

\\[ \\|u\\|\_{\rm G} = {\rm inf}\_g\big\\{ \\|g\\|\_\infty=\max\_{kl} \\|g\_{kl}\\|\_2\ :\ {\rm div}g = u,\ g\_{kl}=(g^{1}\_{kl},g^{2}\_{kl})\big\\},\\]

where, as for the continuous setting, \\({\rm div}\\) is the discrete divergence operator defined as the adjoint of the finite difference gradient operator \\(\nabla\\) used to defined the TV norm. In other words, \\(\langle -{\rm div}g, u\rangle\_X = \langle g, \nabla u\rangle\_Y\\), where \\(X=\mathbb{R}^N\\) and \\(Y=X\times X\\).

Unfortunately, the G norm computation seems not so obvious that the one of its dual counterpart and an optimization method must be used. I don't know if this could lead to an efficient implementation of a TV spgl1.
