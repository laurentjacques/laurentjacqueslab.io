+++
title = "Matching Pursuit Before Computer Science"
author = ["Laurent Jacques"]
date = 2008-06-12
tags = ["greedy algorithm", "matching pursuit", "computer science", "sparsity", "Hooke's law"]
draft = false
+++

I have recently discovered some interesting references about techniques designed around 1938 which could, in my opinion, be considered as (a variant of) the Matching Pursuit algorithm. This is maybe something well known in the literature of the right scientific field, but here is anyway what I recently found.

From what I knew until yesterday, when [S. Mallat](http://www.cmap.polytechnique.fr/~mallat/) and Z. Zhang [1] defined their _greedy_ or iterative algorithm named _"Matching Pursuit"_ (MP) to decompose a signal \\(s \in \mathbb{R}^N\\) into a linear combination of _atoms_ picked in a dictionary \\(\mathcal{D}=\\{g\_k\in\mathbb{R}^N: 1\leq k\leq d, g\_k^Tg\_k = 1\\}\\) of \\(d\\) elements, they cited a  previous method known as "_Projection Pursuit_" of J. Friedman and W. Tukey [2] in the field of statistical regression methods.

In short, MP is quite simple. It reads (using matrix notations)

\begin{align}
&R^0 = s,\quad A^0 = 0 \tag{initialization}\\\\\\
&R^{n+1}\ =\ R^n\ -\ (g\_{\*}^T R^n) g\_{\*},\tag{reconstruction}\\\\\\
&A^{n+1}\ =\ A^n\ +\ (g\_{\*}^T R^n)g\_{\*}&\tag{reconstruction}
\end{align}

with at each step \\(n\geq 0\\)
\\[ g\_{\*} \ = \ {\rm arg max}\_{g\in\mathcal{D}}\ |g^T R^n| \tag{sensing}.\\]
The quantities \\(R^n\\) and \\(A^n\\) are the residual and the approximation of \\(s\\), respectively, at the \\(n\\)-th step (so also an approximation in \\(n\\) terms of \\(s\\)). A quite direct modification of MP is the _Orthogonal Matching Pursuit_ [8]. In this variant, one still records at each iteration the index (or parameters) of the best atom---the one maximizing its correlation with \\(R^n\\), in the "sensing" equation above---but the approximation \\(A^n\\) is then computed by a least squares minimization over the set of the \\(n\\) previously selected atoms (and not only the last one as in MP).

It is proved in [1] that MP always converges to ... well, something, since one can show that the energy of the residual \\(\\|R^n\\|\\) decreases steadily towards 0 where \\(n\\) increases.  Under certain assumptions on the dictionary \\(\mathcal{D}\\) (such as assuming it to have a small _coherence,_ or _cumulative coherence,_ that roughly measure its closeness to an orthogonal basis) it is also proved that, if \\(s\\) is described by a linear combination of few elements of that dictionary (for _sparse_ or _compressible_ signal), _i.e._, with \\(s = \mathcal{D}\alpha\_\*\\) for a coefficient vector \\(\alpha\_\*\in\mathbb{R}^d\\) with few non-zero (or large) components, then OMP recovers \\(\alpha\_\*\\) in the set of coefficients \\((g\_{\*}^T R^n)\\) computed at each iteration [9]. For instance, in the trivial case of an orthonormal basis (_i.e._, with vanishing coherence) (O)MP finds iteratively \\(\alpha = \mathcal{D}^{-1} s = \alpha\_\*\\).

Dozens (or hundreds?) of variations of these initial _greedy_ methods have been introduced since their first formulations in the signal processing community. These variations have improved for instance the initial rate of convergence of MP through the iterations, as well as our ability to solve the _sparse approximation problem_ (_i.e._, finding the vector \\(\alpha\_\*\\) expressed above). They have also adapted MP to some specific problems like the emerging field of [_Compressed Sensing_](http://nuit-blanche.blogspot.com/search/label/CS). Let's quote for instance the gradient Pursuit, stagewise OMP, CoSaMP, regularized MP,  subspace pursuit, ... (see [here](http://igorcarron.googlepages.com/cs#reconstruction) and [here](http://www.compressedsensing.com/) for more information on these algorithms).

Another variation of (O)MP explained by [K. Schnass](http://lts2www.epfl.ch/~schnass/) and [P. Vandergheynst](http://ltspc89.epfl.ch/~vandergh/) in [3], splits the _sensing part_ from the _reconstruction part_ in the initial MP algorithm above, and provides the possibility to select more than only one atom per iteration. In that work, the selection of the best atom \\(g\_\*\\) is performed by a _sensing dictionary_ \\(\mathcal{S}\\) while the reconstruction stage, which defines the residuals and the approximations, is still assigned to the first dictionary \\(\mathcal{D}\\). This variation is also proved to solve the sparse problem if the two dictionaries satisfy a small _**cross** (cumulative) coherence criterion_, which is easier to fulfill than asking for a small (cumulative) coherence of only one dictionary in the initial (O)MP. This reminds the way biorthogonal wavelet bases simplified the selection of wavelets with many moments in wavelet theory.

This last (O)MP variation is precisely the one I discovered it in the separated works of [R.V. Southwell](http://en.wikipedia.org/wiki/Richard%5FV.%5FSouthwell) [4] and G. Temple [5] (the last being more readable in my opinion) written **in 1935 and in 1938** (!!), respectively, _i.e._, before the building of the first (electronic) [computers](http://en.wikipedia.org/wiki/Computer) in the 40s.

The context of the method in the initial Southwell's work was the determination of stresses in structures. Let's summarize this problem. The structure was modeled by \\(N\\) connected springs. If \\(\alpha\_\*\in\mathbb{R}^N\\) represents any motion vector of the springs extremities, then, at the new equilibrium state reached when external forces \\(F\_{\rm external}\in\mathbb{R}^N\\) are applied to the structure, the internal forces provided by the springs follow, of course, the [Hooke law](http://en.wikipedia.org/wiki/Hooke's%5Flaw) \\(F\_{\rm internal}=D \alpha\_\*\\) for a certain symmetric matrix \\(D\in\mathbb{R}^{N\times N}\\) containing the spring constants. From Newton's first law, we thus get
\\[ F\_{\rm internal} + F\_{\rm external} = D \alpha\_\* + F\_{\rm external} = 0.\\]

In this context, the general question addressed by Southwell was the following. Given a linear system of equations \\(D\alpha\_\* = s\\), with \\(s= -F\_{\rm external}\\) and \\(D\\) positive definite, how can you practically recover \\(\alpha\_\*\\) from \\(s\\) and \\(D\\)? As explained by Temple, a solution to this problem is also _"applicable to any problem which is reducible to the solution of a system of non-homogeneous, linear, simultaneous algebraic equations in a finite number of unknown variables"_.

Nowadays, the numerical solution seems trivial: take the inverse of \\(D\\) and apply it to \\(s\\), and if \\(D\\) is really big (or even small since I'm lazy) compute \\(D^{-1}\\) for instance with Matlab and run `>> inv(D)*s` (or do something clever with the "`/`" Matlab operator).

However, the question was raised in the 30s! Even for inverting a ridiculously small 13 &times; 13 matrix, an analytical solution can be cumbersome to compute. Moreover, it can just be worthless since we are, after all, only interested in finding \\(\alpha\_\*\\), not \\(D^{-1}\\). That's why a few researchers were interested at that time in computing \\(A^{-1}s\\), or an approximation to it, without to have this painful inverse computation.

The technique found by [R.V. Southwell](http://en.wikipedia.org/wiki/Richard%5FV.%5FSouthwell) and generalized later by Temple [4,5], was dubbed of _"Successive Relaxation"_ inside a more general context named _"Successive Approximations"._ Mathematically, rewriting that work according to the notations of the Matching Pursuit, the _Successive Relaxation_ algorithm reads:

\begin{align}
&R^0 = s,\quad \alpha^0 = 0,\tag{initialization}\\\\\\
&R^{n+1}\ =\ R^n\ -\ \beta (R^{n})\_{k^\*} D\_{k^\*}\\\\\\
&\alpha^{n+1}\ =\ \alpha^n\ +\ \beta (R^{n})\_{k^\*} e\_{k^\*}\tag{reconstruction}
\end{align}

where \\(\beta\in (0,2)\\), \\(D\_j\\) is the \\(j\\)-th column of \\(D\\), \\((R^n)\_{m}\\) is the \\(m\\)-th component of \\(R^n\\), \\(e\_j\\) is the vector such that \\((e\_j)\_k=\delta\_{jk}\\) (a canonical basis vector), and with, at each step \\(n\geq 0\\), the selection (sensing)
\\[ k^{\*} = {\rm argmax}\_{k} |(R^n)\_k|, \tag{sensing}\\]
_i.e._, the component of the \\(n\\)-th residual with the highest amplitude.

In this framework, since \\(D\\) is positive definite and thus non-singular, it is proved in [5] that the vectors \\(\alpha^n\\) tend to the true answer \\(\alpha\_\*=D^{-1}s\\). The parameter \\(\beta\\) controls the importance of what you removed or add in the residual and in the approximation respectively. You can prove easily that the decreasing of the residual energy is maximum when \\(\beta=1\\).  In other words, in the concepts introduced in [3], they designed a Matching Pursuit where they selected for the reconstruction dictionary the orthonormal basis \\(D\\) and for the _sensing dictionary_ the identity (the canonical basis) of \\(\mathbb{R}^N\\). Amazing, no?

An interesting learning of the algorithm above is the presence of the factor \\(\beta\\). In the more general context of (modern) MP with non-orthonormal dictionaries, such a factor could be useful to minimize the "decoherence" effect observed experimentally in the decomposition of a signal when this one is not exactly fitted by elements of the dictionary (as in image processing, where arbitrarily oriented edges have to be described by horizontal and vertical atoms).

G. Temple in [5] also extended the method to infinite dimensional Hilbert spaces for a different updating step of \\(\alpha^n\\). This is nothing else but the foundation of the (continuous) MP studied by authors like R. DeVore and Temlyakov some years ago (on that topic you can read also [6], a paper I wrote with [C. De Vleeschouwer](http://www.uclouvain.be/christophe.devleeschouwer) for a geometric description of this continuous formalism).

By googling a bit with the keywords ["matching pursuit" and Southwell](https://www.google.com/search?q=%22matching+pursuit%22+Southwell), I found this [presentation](https://web.archive.org/web/20070102111555if%5F/http://www.dma.ens.fr:80/~stoltz/GDTs/Apprentissage/SemBuhlmann.pdf) of [Peter Buhlmann](http://stat.ethz.ch/~buhlmann/) who makes a more general connection between Southwell's work, Matching Pursuit, and greedy algorithms (around slide 15) in the context of "_Iterated Regularization for High-Dimensional Data_."  In conclusion, who is this person who explained that we do nothing but always reinventing the wheel? ;-)

If you want to complete this kind of "archeology of Matching Pursuit" please feel free to add some comments below (or to drop me an email). I'll be happy to read them and improve my general knowledge of the topic.

-- Laurent

(text updated on Feb. 15, 2023)

_References_:

-   [1] S. G. Mallat and Z. Zhang, [Matching Pursuits with Time-Frequency Dictionaries](http://www.cmap.polytechnique.fr/~mallat/papiers/MallatPursuit93.pdf), IEEE Transactions on Signal Processing, December 1993, pp. 3397-3415.
-   [2] J. H. Friedman and J. W. Tukey (Sept. 1974). "[A Projection Pursuit Algorithm for Exploratory Data Analysis](http://http//ieeexplore.ieee.org/xpls/abs%5Fall.jsp?arnumber=1672644)". IEEE Transactions on Computers C-23 (9): 881--890. <10.1109/T-C.1974.224051>. ISSN 0018-9340.
-   [3] K. Schnass and P. Vandergheynst, [Dictionary preconditioning for greedy algorithms](http://lts2www.epfl.ch/~schnass/papers/dicoprecond.pdf), IEEE Transactions on Signal Processing, Vol. 56, Nr. 5, pp. 1994-2002, 2008.
-   [4] R. V. Southwell, "[Stress-Calculation in Frameworks by the Method of ”Systematic Relaxation of Constraints](http://www.jstor.org/stable/96340)". I and II. Proc Roy. Soc. Series A, Mathematical and Physical Sciences, Vol. 151, No. 872 (Aug. 1, 1935), pp. 56-95
-   [5] G. Temple, [The General Theory of Relaxation Methods Applied to Linear Systems](http://www.jstor.org/stable/97159), Proc. Roy. Soc. Series A, Mathematical and Physical Sciences, Vol. 169, No. 939 (Mar. 7, 1939), pp. 476-500.
-   [6] L. Jacques and C. De Vleeschouwer, "[A Geometrical Study of Matching Pursuit Parametrization](http://arxiv.org/abs/0801.3372)", Signal Processing, IEEE Transactions on [see also Acoustics, Speech, and Signal Processing, IEEE Transactions on], 2008, Vol. 56(7), pp. 2835-2848
-   [7] R.A. DeVore and V.N. Temlyakov. "Some remarks on greedy algorithms." Adv. Comput. Math., 5:173--187, 1996.
-   [8] Y. C. Pati, R. Rezaiifar, and P. S. Krishnaprasad, "[Orthogonal projection pursuit: Recursive function approximation wit  applications to wavelet decomposition,](http://citeseer.ist.psu.edu/pati93orthogonal.html)" in Proceedings of Twenty-Seventh Asilomar Conference on Signals, Systems and Computers, vol. 1, (Pacific Grove, CA), pp. 40- 44, NOV. 1-3, 1993.
-   [9] Tropp, J, "[Greed is good: algorithmic results for sparse approximation](www.math.princeton.edu/tfbb/spring03/greed-ticam0304.pdf)", IEEE T. Inform. Theory., 2004, 50, 2231-2242

_Image credit_: [EDSAC](http://en.wikipedia.org/wiki/EDSAC) was one of the first computers to implement the stored program ([von Neumann](http://en.wikipedia.org/wiki/Von%5FNeumann%5Farchitecture)) architecture. Source Wikipedia.
