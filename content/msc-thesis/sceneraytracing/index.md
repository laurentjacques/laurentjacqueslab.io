---
title: "Scene reconstruction from ray tracing simulation"
author: ["Laurent Jacques"]
date: 2023-02-21
tags: ["sparsity", "ray tracing", "systems", "simulation"]
draft: false
abstract: The objective of this master project is to study and program methods to estimate a 2D scene from emission and reception of signals between two antennas.
image:
  preview_only: true
---

-   **Max. student(s)**: 1-2
-   **Advisor**: [Laurent Jacques](mailto:laurent.jacques@uclouvain.be)
-   **Teaching Assistant**: [Jérome Eertmans](mailto:jerome.eertmans@uclouvain.be)


## Context {#context}

In optics, acoustics, or telecommunications, there are many methods to estimate the power of a received signal, depending on the position of the transmitter, that of the receiver, and the geometry of the scene.  Among these methods, ray tracing has gained a particular interest in recent years, especially thanks to advances in terms of computing power.

{{< figure src="/ox-hugo/2023-03-14_12-36-22_09262f0cc6b039bbd2c00664b72071ef.png" caption="Figure 1: Image credits [2]" width="30%" >}}

Ray tracing is quite simple to understand: from an emitting source (e.g., a light source, a speaker or a radio antenna), one traces a very large number of rays going in all directions. Each time we encounter an obstacle, we apply a specular reflection[^fn:1], and we continue until we reach the receiver. If a ray has not reached the target after a fixed number of bounces, it is abandoned. The method described here is similar to ray launching, one of the many ray tracing variants.

{{< figure src="/ox-hugo/2023-03-14_12-40-50_637ac73e5457b86a8fe33b018cf93a39.png" caption="Figure 2: Image credits [2]" width="30%" >}}


## Problem {#problem}

As one may notice, the number of rays that arrive to the receiver is much lower than the number of rays sent. **Our question is the following:** _can we infer some parameters of the scene just by looking at the rays that we receive?_

Of course, the answer to this question depends on the parameters you know apriori, and those you need to infer. The next section will detail the objectives we thought about.


## Objectives {#objectives}

For this Master thesis, we propose the following steps[^fn:2]:

1.  From a known 2D scene, a fixed number of reflections, and a known antenna pattern[^fn:3], show how you can determine the emitter's location from the set of rays received at the receiver (what are the necessary conditions in this setting for this to work?);
2.  Study the sparsity / scaristy of the number of rays received, depending on the emitter's position, or the antenna pattern (i.e., very directive vs omnidirectionnal);
3.  Do the same as (1), but the unkown is now the reflection coefficient of some wall(s) in the scene;
4.  Study how uncertainty on some parameter (e.g., you know the position of the receiver with a precision of 1cm) impacts your estimation of unknown parameters;
5.  Can you infer more information about the emitter (e.g., its radiation pattern) from the knowledge of the scene's response to a set of known emitter configurations?


## Links {#links}

-   Ray Tracing Gems from Nvidia ([url](http://dx.doi.org/10.1007%2F978-1-4842-7185-8))
-   Book chapter about sound propagation ([url](http://dx.doi.org/10.1007%2F978-3-031-79214-4%5F3))
-   Fast and simple algorithm to detect object shadowing ([url](http://dx.doi.org/10.1260%2F1351-010X.18.1-2.123))


## References {#references}

-   [1] Chandak, A., Antani, L., Taylor, M., & Manocha, D. (2011). Fast and Accurate Geometric Sound Propagation Using Visibility Computations.  Building Acoustics, 18(1--2), 123--144.  [(doi)](https://doi.org/10.1260/1351-010X.18.1-2.123)
-   [2] Geok, T. K., Hossain, F., & Chiat, A. T. W. (2018). A Novel 3D Ray Launching Technique for Radio Propagation Prediction in Indoor Environments. PLOS ONE, 13(8), e0201905.  [(doi)](https://doi.org/10.1371/journal.pone.0201905)
-   [3] Liu, S., & Manocha, D. (2022). Sound Propagation. In S. Liu & D. Manocha (Eds.), Sound Synthesis, Propagation, and Rendering (pp. 29--43). Cham: Springer International Publishing.  [(doi)](https://doi.org/10.1007/978-3-031-79214-4%5F3)
-   [4] Marrs, A., Shirley, P., & Wald, I. (Eds.). (2021). Ray Tracing Gems II: Next Generation Real-Time Rendering with DXR, Vulkan, and OptiX.  Berkeley, CA: Apress. [(doi)](https://doi.org/10.1007/978-1-4842-7185-8)

[^fn:1]: We could also account for refraction, diffraction, and so on.
[^fn:2]: Note that this is not fixed, and you are welcome to propose modifications in the proposal.
[^fn:3]: That is, how the rays are emitted. The simplest one is omnidirectionnal (same power regardless of the angle of departure).
